{
    "id": "87056a95-4f09-4bcc-8a4b-9415916872c6",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel2",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "27c8601f-8562-4d9a-b334-9b6c951dfb8c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32,
            "speed": 100
        },
        {
            "id": "603d59be-9d82-4749-be44-e2a1a73cc78a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 32,
            "speed": 100
        },
        {
            "id": "99ffbf81-14c5-47dd-9d35-5ce883ef042d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 608,
            "speed": 100
        },
        {
            "id": "556d5e86-ea31-4b05-9219-0363b4e00aed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 608,
            "speed": 100
        },
        {
            "id": "27cf7e0d-fe0e-43d9-927e-33a2030c620c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 416,
            "speed": 100
        },
        {
            "id": "9c73724a-b4ad-4c7a-8bb9-b6d851067f38",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 416,
            "speed": 100
        },
        {
            "id": "eadf527c-217f-42f0-9356-23bf092753c7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 32,
            "speed": 100
        },
        {
            "id": "f7e8fa14-3684-47da-aac1-e6fdfb361f48",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1376,
            "y": 32,
            "speed": 100
        },
        {
            "id": "6ad63af9-c2ca-41b2-b308-4fb1d1cfe7f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1376,
            "y": 288,
            "speed": 100
        },
        {
            "id": "ce8197e7-f09e-4dc2-90c3-e93c948ae2e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 288,
            "speed": 100
        },
        {
            "id": "32ec1909-095e-4501-9887-342041e63a31",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 480,
            "speed": 100
        },
        {
            "id": "8f6d3276-74e0-46a2-bf71-6cf7ef0e2884",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 480,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}