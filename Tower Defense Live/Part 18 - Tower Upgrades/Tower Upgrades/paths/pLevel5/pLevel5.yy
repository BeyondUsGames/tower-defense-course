{
    "id": "ec693116-3672-46bb-99f9-4257b7ca5089",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel5",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "aaec84b7-62d7-406a-bee8-05c624d956e7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 352,
            "speed": 100
        },
        {
            "id": "8cf0cd0d-c9e9-469c-af37-dba296b23395",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 352,
            "speed": 100
        },
        {
            "id": "5f463c0b-d84c-4686-9c24-9d3e8b230f7d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 32,
            "speed": 100
        },
        {
            "id": "637c157c-825b-41f3-8240-d50fecc09c80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 32,
            "speed": 100
        },
        {
            "id": "b52b3fe1-5eea-4bd8-91fb-67ed1e230edc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 480,
            "speed": 100
        },
        {
            "id": "1a5d499f-e5d8-4faa-aaa4-da2ab10a5d99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 480,
            "speed": 100
        },
        {
            "id": "431e5157-496b-4e6a-a670-e54be62a6e90",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 608,
            "speed": 100
        },
        {
            "id": "41dea6d4-ea0c-4b25-94af-628e41d52595",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1056,
            "y": 608,
            "speed": 100
        },
        {
            "id": "745e5997-c94e-49fb-84a7-577d5b79d8cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1056,
            "y": 480,
            "speed": 100
        },
        {
            "id": "c12ee0c5-ab26-41d1-ac05-2d27c231ab53",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 480,
            "speed": 100
        },
        {
            "id": "3f7fc01a-7d56-4940-a38e-3cb63c1cd33c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 224,
            "speed": 100
        },
        {
            "id": "7d512a9f-31ae-4d10-98c6-2237fb303edd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1184,
            "y": 224,
            "speed": 100
        },
        {
            "id": "cc470324-1b7f-4c42-b8f6-b3648ec55ec1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1184,
            "y": 352,
            "speed": 100
        },
        {
            "id": "1e93c44f-0f2a-4048-9d39-3e1a3f999228",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 352,
            "speed": 100
        },
        {
            "id": "049f68b4-d150-43df-b922-1078a52e2b6c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 416,
            "speed": 100
        },
        {
            "id": "ac6a04b0-3fee-4fe1-8dfa-40da53822b72",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1312,
            "y": 416,
            "speed": 100
        },
        {
            "id": "6572c185-55ef-44ce-b38d-868345e95a87",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1312,
            "y": 480,
            "speed": 100
        },
        {
            "id": "a2a1d9bb-ad13-4250-8e5a-f1232143f587",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1376,
            "y": 480,
            "speed": 100
        },
        {
            "id": "ca312b28-d915-4c5f-9dab-5923605edcfa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1376,
            "y": 544,
            "speed": 100
        },
        {
            "id": "3a74b216-8fd2-4784-a2a4-bb25d610955d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1440,
            "y": 544,
            "speed": 100
        },
        {
            "id": "3c03ee8f-056d-4f11-b8c1-9d6ad5328ef5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1440,
            "y": 608,
            "speed": 100
        },
        {
            "id": "4761d214-ff51-4c5d-b186-5c691a4c2652",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 608,
            "speed": 100
        },
        {
            "id": "8f62a253-d443-4c19-b305-e43db42b758e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 672,
            "speed": 100
        },
        {
            "id": "26c15925-0ae3-491f-a487-7d027ce17c9c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 672,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}