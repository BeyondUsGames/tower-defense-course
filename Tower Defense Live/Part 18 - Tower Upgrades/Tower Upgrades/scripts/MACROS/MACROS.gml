///Set Up Macros

//Tower Info
#macro TowerSprite 0
#macro TowerObject 1
#macro TowerCost 2

#macro ArcherTowerCost 25
#macro FireballTowerCost 50
#macro SludgeTowerCost 40

//Saving & Loading
#macro FileName "Tower_Defense_Data.ini"
#macro Level "Level"
#macro StarRating "Star_Rating"
#macro KillCount "Kill_Count"

//Upgrade Macros
#macro TowerLevel1 0
#macro TowerLevel2 1
#macro TowerLevel3 2
#macro TowerLevel4 3

#macro FireRate 0
#macro Radius 1
#macro Damage 2
#macro UpgradeCost 3

#macro MaxedOut "Maxed Out"