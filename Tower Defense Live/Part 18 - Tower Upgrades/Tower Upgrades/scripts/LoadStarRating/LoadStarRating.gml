///Load and return star rating
ini_open(FileName);

var rating = ini_read_real(Level + string(myLevelNumber), StarRating, 0);

ini_close();

return rating;