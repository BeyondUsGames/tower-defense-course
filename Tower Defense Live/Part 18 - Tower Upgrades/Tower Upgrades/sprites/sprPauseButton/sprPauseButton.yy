{
    "id": "0537d472-80d8-41f0-b472-ec65589e88a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPauseButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6de47ec8-25cd-4caa-a0c4-52c049ef00dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0537d472-80d8-41f0-b472-ec65589e88a7",
            "compositeImage": {
                "id": "00b0c65b-e1f2-4619-878a-11572d13d65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de47ec8-25cd-4caa-a0c4-52c049ef00dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d44a122-b1eb-49ed-b870-27426190e853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de47ec8-25cd-4caa-a0c4-52c049ef00dd",
                    "LayerId": "8116d7b3-0ee1-4f24-adcd-71f80813057b"
                }
            ]
        },
        {
            "id": "1a2b06f2-51d5-4f1f-b415-b5b9bd60049d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0537d472-80d8-41f0-b472-ec65589e88a7",
            "compositeImage": {
                "id": "57feb925-3db5-42c0-a272-751ad2e85a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2b06f2-51d5-4f1f-b415-b5b9bd60049d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa36b2f-3d7e-4221-84f3-2adc212be646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2b06f2-51d5-4f1f-b415-b5b9bd60049d",
                    "LayerId": "8116d7b3-0ee1-4f24-adcd-71f80813057b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8116d7b3-0ee1-4f24-adcd-71f80813057b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0537d472-80d8-41f0-b472-ec65589e88a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}