{
    "id": "e915a069-ee49-48ef-a39a-b4e1db482c55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8e6f350-6513-413a-8a64-39b09a7111fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e915a069-ee49-48ef-a39a-b4e1db482c55",
            "compositeImage": {
                "id": "13e9ed64-76db-4c90-b1ed-f5bf4abfca53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e6f350-6513-413a-8a64-39b09a7111fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e95c598-adea-427d-a5eb-2a98dd26d144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e6f350-6513-413a-8a64-39b09a7111fc",
                    "LayerId": "43399d36-d3b4-4d00-b6a1-8eb8e020ff5d"
                }
            ]
        },
        {
            "id": "78dd0fbf-4bd5-4c4b-9483-4e213aab78d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e915a069-ee49-48ef-a39a-b4e1db482c55",
            "compositeImage": {
                "id": "84d13320-a4a0-4063-83ca-d9140ddbeb26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78dd0fbf-4bd5-4c4b-9483-4e213aab78d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dfb2f68-af3c-4eda-b755-3196f4b224af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78dd0fbf-4bd5-4c4b-9483-4e213aab78d3",
                    "LayerId": "43399d36-d3b4-4d00-b6a1-8eb8e020ff5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "43399d36-d3b4-4d00-b6a1-8eb8e020ff5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e915a069-ee49-48ef-a39a-b4e1db482c55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}