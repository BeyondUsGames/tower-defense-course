{
    "id": "1b8ed226-cff1-4a84-a51f-0e9a1f000747",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSludgeTower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 212,
    "bbox_left": 46,
    "bbox_right": 213,
    "bbox_top": 45,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43278569-7cdf-4cb4-869a-06ddc4701c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b8ed226-cff1-4a84-a51f-0e9a1f000747",
            "compositeImage": {
                "id": "d30a8718-b5e4-45bd-8694-376194501476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43278569-7cdf-4cb4-869a-06ddc4701c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99bf5400-192f-46d7-a967-1720d23903e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43278569-7cdf-4cb4-869a-06ddc4701c8c",
                    "LayerId": "0a19839c-1c65-40c0-a3b7-a399a531f2c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0a19839c-1c65-40c0-a3b7-a399a531f2c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b8ed226-cff1-4a84-a51f-0e9a1f000747",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}