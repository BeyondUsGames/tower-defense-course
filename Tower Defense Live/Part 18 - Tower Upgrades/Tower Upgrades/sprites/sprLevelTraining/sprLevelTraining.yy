{
    "id": "6922a0fc-c70b-4889-b9b1-9270f5beb8dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevelTraining",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d579ed5f-b6c6-47d6-9f02-c8b2d0551622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6922a0fc-c70b-4889-b9b1-9270f5beb8dc",
            "compositeImage": {
                "id": "5a385440-3eb3-49d5-90a5-c8b9320dcc9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d579ed5f-b6c6-47d6-9f02-c8b2d0551622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef59b845-5e0b-4df1-8f94-d9fb9b7d18fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d579ed5f-b6c6-47d6-9f02-c8b2d0551622",
                    "LayerId": "49de172f-9f29-4228-93f2-c3c04c33989e"
                }
            ]
        },
        {
            "id": "165f9749-5573-4485-bc7b-97d9b6c8784f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6922a0fc-c70b-4889-b9b1-9270f5beb8dc",
            "compositeImage": {
                "id": "537ae628-b43b-45ea-a9f5-e739e54e68ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165f9749-5573-4485-bc7b-97d9b6c8784f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1765e2b7-a14e-4856-a601-aee801fbb203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165f9749-5573-4485-bc7b-97d9b6c8784f",
                    "LayerId": "49de172f-9f29-4228-93f2-c3c04c33989e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "49de172f-9f29-4228-93f2-c3c04c33989e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6922a0fc-c70b-4889-b9b1-9270f5beb8dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}