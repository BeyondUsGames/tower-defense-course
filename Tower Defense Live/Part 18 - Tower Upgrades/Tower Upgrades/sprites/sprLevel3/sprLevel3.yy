{
    "id": "5c1d2527-baba-4a2b-b6a0-9ba4701ce0ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e5f1b41-8f0e-46ad-8e6f-e169d050f6c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1d2527-baba-4a2b-b6a0-9ba4701ce0ff",
            "compositeImage": {
                "id": "b61639ff-d3f8-43e4-8c4a-6a1ee1ad30cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5f1b41-8f0e-46ad-8e6f-e169d050f6c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b309d384-3189-4c5d-93b8-6cc7828e2950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5f1b41-8f0e-46ad-8e6f-e169d050f6c0",
                    "LayerId": "6fbb8c5d-b3a6-482f-bb4f-dc786e83c351"
                }
            ]
        },
        {
            "id": "79283d61-69b7-474c-ab4f-faa0be7a1d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1d2527-baba-4a2b-b6a0-9ba4701ce0ff",
            "compositeImage": {
                "id": "d204dd91-6008-4915-9926-dd8c351dc99d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79283d61-69b7-474c-ab4f-faa0be7a1d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dadc2095-a179-4a1f-a8c5-aeeb71df57e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79283d61-69b7-474c-ab4f-faa0be7a1d7a",
                    "LayerId": "6fbb8c5d-b3a6-482f-bb4f-dc786e83c351"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6fbb8c5d-b3a6-482f-bb4f-dc786e83c351",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c1d2527-baba-4a2b-b6a0-9ba4701ce0ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}