{
    "id": "6fd75811-e2ac-4730-9ce8-048bd5ccd69e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntUpgradeText",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fcc70725-8dd1-4d44-8f04-d71f174d61e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3c53f4ab-141a-40e8-b479-10242c3a89ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e1717741-78f5-4388-9c62-b52676c186cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f700c17f-3849-4845-b8ff-9770646e2d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8964f852-1aec-4223-926c-60983b00c8ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bfd02cfb-6176-46bc-ae72-1b0627370f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 18,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6f933170-3240-4934-9810-b39908d216a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "df07c72e-8b81-4307-8041-480e22af80bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0f28ad41-5541-4117-b87e-dbc234da2a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 237,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5db7317e-c77a-446c-8acb-ae24dd9604b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "181dd570-895c-4a6e-a313-d20f11059b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b176a633-b78d-4de6-aa81-742ecd19d51b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 216,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "33e33093-41f5-4a53-b0dc-7a0790bc34c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 198,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a25427e9-51b6-4e0e-b1a4-56605d8d4a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9a4212c1-0771-42d4-a5ab-3261a012a706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "27dab4f0-5eb6-40c5-9696-81321e5e1d98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f70c64f9-e4e8-456b-aeea-d4c1996eeb81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ebc5f539-1872-41cb-aeb1-b16684b7816e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bfbc1452-a7cd-43ef-b4fc-a556a9e1cd6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "20a9cb4b-1ef5-41cd-8ee7-58aebd2bf235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 128,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e32e2a31-e51a-45fb-87fd-40f7fa5af179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3f51432a-e009-4353-8724-04ed765488ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "03f1e0eb-437e-4da2-b8b1-f92264b9f3f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 89,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7a46d706-09e1-4871-9f7c-fe28da4e0227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9f011fce-0ce0-4816-8f6e-2ec369d95647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b38912cd-5d64-46ae-a85c-f5539ac55e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 150,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d3dfc8f1-34e3-4dfb-afce-69488b474158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 145,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f1439667-ef34-4e32-a47c-02ec4859a79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "cb570310-05ea-492a-b698-9709e7fd97a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "77dc58f6-f05b-4f26-93f4-93596c0e7597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8020f60f-3744-4b59-a900-867305d01cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 101,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5744bc2e-93e0-4acc-9ea9-f49e065d9a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 88,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "41f6f6d1-53da-4c1a-a516-c34097a6af25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cf026a26-5b7b-4ac6-a4ce-37852523201b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 48,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0a4d4bf6-106d-4ae1-b179-97413298b4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1a529a0b-5860-42eb-b35b-388e78c11291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "497734d5-1d9c-46b7-bf36-1572bfb79cf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "79c68d73-3571-4b74-a45d-6f3a5a2f6ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f365b16a-e7f8-4889-af40-db96a8b30a54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "77bf162a-59dc-46d4-8435-5d72cc2a6a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 202,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fcb21fe8-59e6-41eb-a5cf-6ab58293a0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "81c8d75b-4f6e-487b-98a1-cc9a31ef9ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1c3155f8-a02e-4ef9-aead-2c200561644e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "171c5197-b5ff-46e5-8fbb-d5110785cf90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9e00f67d-c1c7-439a-a8be-0143745e3898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 144,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "35cd52b9-115a-4de1-968e-4049d3d281d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3e207d58-ec98-4518-a221-79e92b7b2d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "eae69c1d-bac7-43d6-9d98-d2db8eb52af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 83,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d67b93b8-b001-46e0-bd6c-583c197d0afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "99426902-1fba-450f-a986-533526067b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ea58f753-6a25-4906-b137-bdcea0e6234e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2de0d38d-c19c-4d46-ac42-e1176020b6f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "21b76a91-2852-4ca1-9f0a-ba3b1f2cd820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ada94cb7-5112-4dec-98ed-9171c47e05b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6d6f340b-cfb5-41b9-8f2c-ae3dcd5774fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7e35dd03-4849-4344-af33-aace22947c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1412f589-5ed2-4c06-be75-e4cd17354c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b688ff32-c4f3-4c4d-8f7c-63dc005a87b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3966ba4f-bbdf-4015-a33f-57e08eda320f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6bddbfbd-b8bd-4699-9e21-93b2bebd9166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 33,
                "y": 29
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "39a5e1fb-85a2-47b8-a1fb-d388bdc3aed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a9d6d2df-8236-408c-a48f-197396004e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "699beae4-63bc-47d5-972d-1f721335587b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d5cd1c35-513a-4504-9018-90d1814de79f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "923ba5bb-452c-45dc-ae2c-21bfd9b29791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "bfe6b38e-1e40-45e9-94f3-cb19eb0fe355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5cebd670-ad9e-4e8a-9a7b-64b30ee26337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "11b5ddd6-e01d-4cf2-85e3-ce6795e1d14c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6ff92bbf-34bd-45ac-b9ad-232401930fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "80b5a5c7-2f76-4d5a-999e-47edee1080ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a0c2e098-68f3-4d56-973b-d3c43f8e6c74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3050794c-8aee-4e7b-a0a9-c799931812d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c96278b5-fc55-455f-b4be-749944b71e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 180,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0e3545d0-1231-4c12-8241-9a564a14f437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 71,
                "y": 29
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1052834c-bde4-4ba0-973a-15de2db5043b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1c6cbb38-1d0e-45f5-9d3c-929484b1b2d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8f24691e-bc96-4358-9bc3-89169ba6d362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 32,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b28917f8-ed2d-49a9-90e6-426914a1d7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5ac455ee-bb2f-44dc-9979-05579b341218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3a24fb3c-5c69-4fa7-a85b-b27a5cf264b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a652e3bd-19e2-4269-99f3-0c8f348c7810",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e86d3ac7-fae4-452d-96b7-57dc0f06d681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 209,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "af9a695c-5b62-4e91-9f55-f01341a22234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f3ba92b8-ffbc-4044-a1b1-66871055842f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e612e00e-450c-4d7d-b8cf-8eecbeace2d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b3fb8371-0359-4c3f-8f31-5ab6d4cedd0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "eb7bf9b2-dc0f-4ab1-808d-edf37fb5941c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 155,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8e2dde15-4573-4e4f-8392-c77814558156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "38979faa-9ce2-49f6-92e6-19280ce705b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "900796e4-1bf8-4bf1-9890-cbfacf4bc5d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ddc4aabe-f94d-4f77-985a-0eb81d85c1a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f6ff67e2-0ccf-4d21-8ab4-84d8945e44d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3b87cf5f-df18-430e-9f78-f93a23a28cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b86a53cf-36c3-4183-bc15-13f346b61063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 29
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a967913d-7891-4cf6-a91f-9ce6ad9abfeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 163,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "79035bde-06e7-46e1-adaf-343211ef1388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 177,
                "y": 110
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1e877ee6-b800-4058-9586-9f9f6c7ee97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "623a5b6a-f32d-4352-a5be-6583916ac2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "fa64b168-1615-4301-a945-34dd7970658a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "1c2759d5-983e-4e3f-8220-c54f3bd687e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "d0824e5c-2ae5-4771-9383-ec0bc76d62f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "7fe142c2-6757-4671-8992-b00347bb3b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "e67a49f8-40d2-4264-ba7e-b67ebec249d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "f55a74cb-3b8a-424b-81d4-60ee22811876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "886dc955-1820-4a95-907b-f36b73d1b874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "73dcb089-55e9-46ff-ae6c-a0f1358befdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "315bab20-4732-44f5-8435-8c55c37bf556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "a2599667-c78d-47c5-86f5-9c77c72c7e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "78c9f240-2294-4595-8795-7812553c6d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "0d8865df-4b9e-427c-b4db-906a4abe9a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "a4461aa8-0c5a-4c75-ba04-25753ba1ba6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "717535bf-8762-4610-b0cb-17b322b742c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "4d174aad-03ed-4979-8c35-fd4c0005b986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "fdd8f556-e5c6-4968-9261-e5f371ff58a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "44ae6679-3658-4c8d-bafd-1a10774b130c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "73e800c8-831a-4111-856b-48366646b6e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "d9cdace3-b017-4aad-b81c-ee94de7bc5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "d9d7476b-c2f9-49a4-ac95-90d4ee55f608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "88183500-fdff-4872-bd3d-903f161c0c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "0d469875-6e8a-4a07-85e5-d0d11ad48124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "7c3394c1-5e34-4e48-a673-9ad9e0096280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "53de18fd-8b59-477f-8711-67433d338459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "a7d1c2fc-b64b-462c-b480-615de0e39555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "e3ecd2d3-d269-4242-b824-6a837f2a3a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "2f206311-4e7b-40f5-ab37-60d795f81309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "4c8f547e-fb8d-4a0e-819a-b9387346fb97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "017acd4f-d38e-4eb3-aca2-c302ede388c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "2115d513-bdeb-4c88-bff0-b26b43b6a6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "52087750-842b-4532-95b3-ddd5bde21670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "94486ec9-d4c5-4fd6-b2c3-a89d3ec8cc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "9312eef1-393d-4a9d-ac05-50f3223e7373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "ea69796e-615c-4617-9da9-2049d40c7f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "36b08bfe-5ed6-4ca7-a9fe-ccf988f8b127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "7c7d27da-4d3a-4dde-96bf-7d23467f65fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "c7b3e63f-e0f9-409c-af84-d1745b9fb4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "655b058d-dd6a-4f37-a476-df5dd6f157b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "3578c923-15f6-4035-8db8-2808777c28cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "fe033afd-5b2d-45b9-81f2-f4c21c987eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "8dcc097c-0c7a-4ef8-a103-cee3fc176dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "702374a8-cc2f-4191-b1f4-330dcb161fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "15e0e5e0-e0b7-4835-b5cf-824e3b20403b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "13b44d9e-7485-4193-aeea-f4dbc532a6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "f5ad63df-88ba-4e5d-8ad0-038d180c245d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "d222b0ae-3094-4cd2-b893-70eeaa43ff32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "a7fa66ed-41e8-47d9-80cd-cb535c8fa8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "8657f846-bc5b-4033-94cd-46110e268053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "d05d5790-8a21-4ede-9429-dd50881ea8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "510aaf04-dd0d-4d89-9ae8-4b74ade4ef7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "b848d8e8-05c5-4b55-ae58-99712dd23690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "dedaaec8-5357-4fd7-a296-da7da2ef8025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "da0c97d3-fe5e-4ce3-ba33-bbd27839daa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "8c217203-8213-47a6-90fe-efa92d531cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "15248f07-f773-4790-9dff-40d4a9eb2946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "7c53c1e3-6142-4de5-8269-5735d4a255f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "0b247eef-eff6-48c7-b8f2-6a11b1442bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "ddab8a16-7f9e-46c9-91bd-247ae35a0e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a7a49eec-92fb-4a21-8170-f3265a773d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "9a805571-5e48-4ab8-a3ff-dbafc84597a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "6141693b-de87-4e3a-8630-2c2235b2a70b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "fed076e5-2ce6-4abb-86b0-e90f9507c704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "dd6cb5bf-5c87-4f1e-9108-da659fce25d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "3928be02-9d46-4e4e-8d70-6058ca0fabc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "fd050080-9aaf-463f-a42c-de27b4d1e45d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "6ad3d4c2-167e-4135-ac86-4e00c60c81c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "5c992d37-7aa5-41e5-8a21-c360af902800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "825f59e8-e987-4481-ac61-f18db4dd3f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "9e328a5b-d1ba-4753-80c5-dbeba7f376ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "b017d270-3904-4514-a6fb-284b22e33f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "984a48c3-97cd-4109-bc3e-2695685ee037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "86239160-215a-4600-a48e-d00991dc317b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ce77d55a-4f27-474b-9d3f-bb960a9ac03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "9179d329-4ca7-4a82-9139-f15ccb59702e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "016f6efc-bdee-497e-82e3-8cfd700ff7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "0496ab0a-2d24-492f-a56b-84cfff5a7ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "b6d205b1-1611-4026-a760-8dfb294b42a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "54e659ca-e32d-4f89-8dd0-4c78422c6bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "4c583804-7d74-40c6-b80c-aca22641d88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "64c3bdb6-45e1-455a-afba-166e8fa5a07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "5438d780-0ea2-4bf9-ac5e-7c01eeb278ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "70e0a5da-a99c-435f-96de-237492e443ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "8f7b7d72-5602-44c8-b774-a5445afc8f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "8833214e-2aea-431b-9711-59edd03ed9f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "ccb8b2c7-7449-4641-a26b-5184f7f65069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "fb2bce14-20e5-43b9-a4a9-d316f8647a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}