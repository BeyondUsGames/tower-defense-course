{
    "id": "ce706134-780a-42e6-b0b5-02ee918c7537",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFlyingSkeletonOW",
    "eventList": [
        {
            "id": "a90f51f3-a174-475f-8d96-4fe71916f5da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce706134-780a-42e6-b0b5-02ee918c7537"
        },
        {
            "id": "a6c445e9-b422-4bb7-9ccf-5a9652822004",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "ce706134-780a-42e6-b0b5-02ee918c7537"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
    "visible": true
}