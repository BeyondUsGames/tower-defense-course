/// @description Check health
if(currentHealth <= 0) {
	instance_destroy();
	objPlayer.gold += myValue;
	objLevelParent.killCount += 1;
}
if(path_position == 1) {
	instance_destroy();
	objPlayer.currentHealth -= 1;
	ShakeScreen(1, .5);
}