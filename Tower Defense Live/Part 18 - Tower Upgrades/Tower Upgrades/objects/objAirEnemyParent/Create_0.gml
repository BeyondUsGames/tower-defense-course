/// @description Set Unique Path
myPath = path_add();
path_add_point(myPath, 0, 32, 100);
path_add_point(myPath, path_get_x(objLevelParent.myPath, 1), path_get_y(objLevelParent.myPath, 1), 100);
path_set_closed(myPath, false);

depth -= 200;