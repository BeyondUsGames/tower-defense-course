{
    "id": "92b5bf50-6327-4af8-a268-ec731e95dc6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDeleteButton",
    "eventList": [
        {
            "id": "1c6e6769-fdbc-4da1-b042-4832fd3989c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 6,
            "m_owner": "92b5bf50-6327-4af8-a268-ec731e95dc6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ad1ecdc5-4519-4a83-8c7f-84a4ae297146",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7872139-bb51-44fd-a260-e4c1a949701e",
    "visible": true
}