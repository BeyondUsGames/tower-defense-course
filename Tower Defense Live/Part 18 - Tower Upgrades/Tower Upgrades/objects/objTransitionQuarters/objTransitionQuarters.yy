{
    "id": "8fd80acc-5ba5-4f02-9496-38cc94a5d558",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTransitionQuarters",
    "eventList": [
        {
            "id": "80e54637-05c2-4fec-a5a3-719f1f18632f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8fd80acc-5ba5-4f02-9496-38cc94a5d558"
        },
        {
            "id": "0100556d-bd33-4ef3-8791-3c002ed3f30b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8fd80acc-5ba5-4f02-9496-38cc94a5d558"
        },
        {
            "id": "9325a26c-a3fb-4b19-9b15-b8df1a1af4ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8fd80acc-5ba5-4f02-9496-38cc94a5d558"
        },
        {
            "id": "babe6124-ca19-4eea-b9b1-713138de1894",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8fd80acc-5ba5-4f02-9496-38cc94a5d558"
        },
        {
            "id": "500b030a-cf98-425b-934f-4b83996ff75d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8fd80acc-5ba5-4f02-9496-38cc94a5d558"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}