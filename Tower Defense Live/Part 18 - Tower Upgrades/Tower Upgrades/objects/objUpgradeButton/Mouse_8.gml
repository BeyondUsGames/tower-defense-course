/// @description Upgrade The Tower
if(myTower.levelStats[myTower.currentLevel + 1, Damage] == MaxedOut)
	show_message("That tower is maxed out.");
else if(objPlayer.gold < myTower.levelStats[myTower.currentLevel, UpgradeCost])
	show_message("not enough gold.");
else {
	myTower.damage = myTower.levelStats[myTower.currentLevel + 1, Damage];
	myTower.radius = myTower.levelStats[myTower.currentLevel + 1, Radius];
	myTower.fireRate = myTower.levelStats[myTower.currentLevel + 1, FireRate];
	objPlayer.gold -= myTower.levelStats[myTower.currentLevel, UpgradeCost];
	++myTower.currentLevel;
}