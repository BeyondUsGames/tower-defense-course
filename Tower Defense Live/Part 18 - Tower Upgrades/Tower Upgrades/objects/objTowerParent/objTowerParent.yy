{
    "id": "952ef05d-7ad2-446b-80ea-67522250a0f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTowerParent",
    "eventList": [
        {
            "id": "6de30b99-dd53-41ab-a44b-f4aa12ff6a85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "952ef05d-7ad2-446b-80ea-67522250a0f1"
        },
        {
            "id": "2ea37866-d476-491e-8375-2ccf3364453f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "952ef05d-7ad2-446b-80ea-67522250a0f1"
        },
        {
            "id": "a9239ec2-f9c7-41a9-b45c-7dbf2dc6c0a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "952ef05d-7ad2-446b-80ea-67522250a0f1"
        },
        {
            "id": "1d56e315-f0b2-41a0-b8df-2c4a029fd1d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "952ef05d-7ad2-446b-80ea-67522250a0f1"
        },
        {
            "id": "b5408b7b-a86c-4113-a146-915ef171b5b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "952ef05d-7ad2-446b-80ea-67522250a0f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}