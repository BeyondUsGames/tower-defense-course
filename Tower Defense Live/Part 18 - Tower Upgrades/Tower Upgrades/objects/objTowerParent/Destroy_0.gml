/// @description Refund some gold
var refundAmount = 0;

//Find our tower, and add that cost
for(var i = 0; i < array_height_2d(global.AvailableTowers); ++ i) {
	if(object_get_name(id.object_index) == object_get_name(global.AvailableTowers[i, TowerObject])) {
		refundAmount = global.AvailableTowers[i, TowerCost];
		break;
	}
}

//Add upgrade cost
for(var i = 0; i < currentLevel; ++i) {
	refundAmount += levelStats[i, UpgradeCost];
}

//Refund The gold
objPlayer.gold += round(refundAmount / 2);