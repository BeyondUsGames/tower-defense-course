{
    "id": "96df2bde-ff36-49a6-bad5-117302443f4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSpeedUpButton",
    "eventList": [
        {
            "id": "4fa9f153-9ce0-41e2-b13b-dd5e0259e3a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "96df2bde-ff36-49a6-bad5-117302443f4b"
        },
        {
            "id": "4b0574a0-b543-45a5-935c-0854ffcccdc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "96df2bde-ff36-49a6-bad5-117302443f4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56b0dcfe-cc2b-4794-88da-82f0378017b2",
    "visible": true
}