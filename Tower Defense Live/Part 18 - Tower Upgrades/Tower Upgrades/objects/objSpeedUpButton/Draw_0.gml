/// @description Draw Speed
draw_self();

draw_set_colour(c_black);
draw_set_font(fntBigText);
draw_set_halign(fa_center);
draw_text(x - 32, y + 16, "Current Speed: " + string(game_get_speed(gamespeed_fps)));