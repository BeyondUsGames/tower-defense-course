{
    "id": "21ff8c20-3af3-4a80-97ac-79504859cbbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGrassBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db970341-8482-438e-9f3d-d55be1365726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21ff8c20-3af3-4a80-97ac-79504859cbbf",
            "compositeImage": {
                "id": "7d2ad8d9-ad74-4a98-99bb-838aca94dfec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db970341-8482-438e-9f3d-d55be1365726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "179d04bb-f74b-4143-96fd-703374ab67ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db970341-8482-438e-9f3d-d55be1365726",
                    "LayerId": "6884ab78-53af-47fe-825f-20ec374ce65d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "6884ab78-53af-47fe-825f-20ec374ce65d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21ff8c20-3af3-4a80-97ac-79504859cbbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}