///Shoot at where enemy will be
type = argument0;

if(type == "x")
	return path_get_x(other.nearestEnemy.path_index, other.nearestEnemy.path_position + other.nearestEnemy.mySpeed / 150);
else if(type == "y")
	return path_get_y(other.nearestEnemy.path_index, other.nearestEnemy.path_position + other.nearestEnemy.mySpeed / 150);
else
	return undefined;