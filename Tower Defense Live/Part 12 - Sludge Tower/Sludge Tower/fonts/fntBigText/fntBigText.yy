{
    "id": "52921fa9-3363-4ace-b5a2-b27f32b23540",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntBigText",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c63c8c8e-a3d7-457c-8f25-a81b4e07b794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 47,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f3d3477e-9fd0-4fca-b808-feaaf1d97792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 47,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 118,
                "y": 100
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ba74d0c3-c80a-467d-86e5-3605f50be4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 104,
                "y": 100
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b7bda452-972d-4bd4-a305-4b4a30e19e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 83,
                "y": 100
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8cd31ab8-a6a1-4c63-af00-6c55820a381f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 69,
                "y": 100
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6ea161ba-2fb3-435f-9a7e-328620d4f3b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 41,
                "y": 100
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1de2a2d8-dd71-4b08-a0d6-2e24e1743c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 21,
                "y": 100
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0feee145-c5cb-47e5-bc3a-ed76a2ebe521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 47,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 13,
                "y": 100
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7bc3ccb6-f05c-4447-9239-54356345d6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 2,
                "y": 100
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f1e7bb34-1f85-407e-859a-d5b86bf35dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 495,
                "y": 51
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1c1fc356-d287-4893-b2b9-ee7c8c72b1b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 128,
                "y": 100
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4ac3375b-2ef6-4658-bb76-86d1d56e5920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 476,
                "y": 51
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f56f06a6-8b3c-4932-a4b7-3b2f12d79803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 450,
                "y": 51
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e8f899f4-9ac3-44d0-bed1-6cb09442701b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 440,
                "y": 51
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b8ca5e42-137a-4b21-a86b-f17bbcc96940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 430,
                "y": 51
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "af7bf43f-e9de-413a-976e-ccab3bd41d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 418,
                "y": 51
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9ba3538d-4dc9-46a2-a883-6f379af0fd8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 51
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a87529b3-0d0c-4c8a-9d92-6dbd231c7f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 47,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 391,
                "y": 51
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0e97b21b-e8c4-42a6-a99c-907027d7d078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 373,
                "y": 51
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8a68d195-c01e-4ee4-a4a1-560a3b1dacc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 359,
                "y": 51
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5fc001f4-bfad-4aac-8666-9b17d013bf34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 341,
                "y": 51
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3d7e11a2-c5cb-4f2a-83d7-2b35d684a454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 460,
                "y": 51
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "715f2064-bd0b-424c-8c31-9d62f7b57f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 142,
                "y": 100
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "94a7192a-1298-43fb-be1e-42f3f1c272c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 162,
                "y": 100
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fa69e517-8809-4d70-92f0-099d36085df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 180,
                "y": 100
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d134b678-6653-4ae3-84c9-a319d27c37c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 57,
                "y": 149
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ad2d2b6f-f746-4d6a-bb7b-39e342a26faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 47,
                "y": 149
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ab3bd1b1-654f-4ea5-b2ea-75e6dc3b7f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 37,
                "y": 149
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "28c1a563-701f-4af0-b504-aa120959b944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 21,
                "y": 149
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d45610fc-d897-4233-b225-ceb2c20ec017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8e2d7d3e-6c20-4186-a728-dd102149b68b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 478,
                "y": 100
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e779956b-f622-4a96-b80b-0a9181ea9b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 461,
                "y": 100
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5bb2739b-98be-4c91-b559-f02c5aabff2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 47,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 437,
                "y": 100
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ae07e5dd-619f-47b1-96b1-3dfe3a1574c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 418,
                "y": 100
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ab813214-2698-4686-b22d-f702f8f7e4c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 100
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5d00cc9d-f431-416f-b90e-d75223973707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 374,
                "y": 100
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "33f30422-ff45-466b-a6ff-b5ecf71d0df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 351,
                "y": 100
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "92dd3a60-7066-4d57-8306-c86a68fcddb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 336,
                "y": 100
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "99935984-1de8-499b-bc8a-df8abcdb4cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 318,
                "y": 100
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ab66e9e6-10af-4fc2-a8fa-8cbb65254065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 47,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 296,
                "y": 100
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3810ea1e-9159-438e-ba35-b74af903d7c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 47,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 278,
                "y": 100
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c56581a4-a79f-4df4-9b27-4a29aedb2169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 47,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 269,
                "y": 100
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2d0f2fd7-aebd-483b-9a1b-1e0543168254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 47,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 257,
                "y": 100
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7f610334-99b0-4fc8-9118-b59cccc2aab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 236,
                "y": 100
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f624cc34-123e-4f8c-a501-609693571c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 224,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "02c48e84-3894-414b-9fcb-388832b4872f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 198,
                "y": 100
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8c87cc3e-441a-44cf-99d9-d4d990bf0258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 322,
                "y": 51
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "99febfe5-543b-43de-8a29-abef315b4f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 296,
                "y": 51
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "86b63979-304a-4311-b34a-ac9ca0c742f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 277,
                "y": 51
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "64682a52-9e5f-42cd-8807-2784591d812d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "00f99565-9e29-463e-bde0-a6ef0956c07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 356,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "044f25b4-fe73-44f8-a41b-51f3f2a7d27f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 47,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6cff130a-116d-4338-bb8b-8854975d740c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "81a6674f-b0c1-4659-abe7-577d2a1a552c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b5f9705c-7ba9-401e-86d8-903ba97c9d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 47,
                "offset": -2,
                "shift": 17,
                "w": 20,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7d831b90-a312-4ce7-b4b2-7b508ffff131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "158549e9-fb0b-428f-b895-9b1328b90a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3a21cddf-9f00-4dcc-b265-2bae0779002f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5f3cfd0a-b7f5-4184-acd0-2a545b8f2d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 47,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "11784cc2-6c02-4a62-886e-dc7d79de2102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0a27d088-85c3-463e-becc-112fc9727e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "008707ae-a651-4aa4-9178-14b84cb3a9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e7241978-ccc0-4c1a-a27c-6fa332ddf5cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d72e7d1f-fbbf-4bfe-9463-c27bca44f348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b377ed88-d6d9-4419-be21-f5a4f3787204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 47,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "76cb8f53-ee5a-41d1-bd14-407cb0e09f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8debfb5e-3536-4c69-a8f1-8974d097b984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5352a7d0-4c78-4175-90e2-0b847dd48973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0785bbf9-23b5-4a13-8c35-7051faa0fc8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6c5ce304-860c-4694-aa98-1d7688d47c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e2be154f-3dfa-4bca-94b1-a971be3709f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3988fb15-888b-46aa-bde0-92afdca07e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c3228d0c-6203-4fd4-b28e-c2a9c8492bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 79,
                "y": 51
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "94d92f75-e78e-4d52-8557-5fad2605e81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b5e4e46d-fa48-4090-bf34-6a20145c4fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 47,
                "offset": -3,
                "shift": 11,
                "w": 13,
                "x": 248,
                "y": 51
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a27cb343-c269-4765-bea0-fb81c7e40f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 229,
                "y": 51
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7d998f94-602d-4df8-8629-354c5b144988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 220,
                "y": 51
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2a42e583-98a0-4652-bcca-ccb9b109359b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 195,
                "y": 51
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "25bcde62-7386-43b3-84d8-f3c9804543d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 178,
                "y": 51
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ffb04875-26ea-4e06-8da0-38a0f1f13aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 158,
                "y": 51
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "02df609c-3d33-46dd-91bc-62ba3dbc0ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 139,
                "y": 51
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b99b5a79-0f63-4535-83bd-e0a3f3aabbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 119,
                "y": 51
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e61ab232-9bd5-4904-9379-6fcaa5d73ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 51
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b4a588f5-e8c0-4f46-85a0-9fbe0ffe8050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 47,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 263,
                "y": 51
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3b528406-4251-4ce1-9c7e-a2c61ebbd6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 96,
                "y": 51
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "54589203-be49-4ffe-80d2-3eb3013bc7c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 62,
                "y": 51
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1decf6f7-69db-4c01-9a3f-78482ca43e78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 47,
                "offset": -2,
                "shift": 12,
                "w": 16,
                "x": 44,
                "y": 51
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c14b9fcd-621c-4790-91bd-52b098bea1b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 19,
                "y": 51
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6be389b0-bf7e-4c8a-9449-a3ce493475c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 51
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "95e768d5-3e67-4461-83d4-63868828b72d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5eaaac6e-ab1c-4a38-87b8-00c112a8f6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6b45993e-ea0d-4c5e-84bd-51ffa18a78cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "62ea23a3-4e52-4bac-92f0-824f18f4429a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 47,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "543d1744-2229-4f55-b042-9a2b53097286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "aca366b9-873e-40ce-975a-2bb7a27872b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 77,
                "y": 149
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2feec53d-f9b8-4b18-805e-26f08a9eb0af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 47,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 96,
                "y": 149
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}