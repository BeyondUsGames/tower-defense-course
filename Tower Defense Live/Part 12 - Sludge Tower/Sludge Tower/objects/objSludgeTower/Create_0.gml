/// @description Initialize
//Set Scale
image_xscale = .25;
image_yscale = .25;

fireRate = 120;
radius = 96;
damage = 0;

myEnemyType = objGroundEnemyParent;
myProjectile = objSludge;