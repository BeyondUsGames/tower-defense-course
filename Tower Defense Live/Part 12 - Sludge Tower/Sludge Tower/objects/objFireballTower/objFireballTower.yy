{
    "id": "fb571a3b-54a5-42fb-8b63-b79bd888e441",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFireballTower",
    "eventList": [
        {
            "id": "0326bbfb-2e7d-44b1-ac14-f8dd1f58e88e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb571a3b-54a5-42fb-8b63-b79bd888e441"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "952ef05d-7ad2-446b-80ea-67522250a0f1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5d4a7046-f118-4f75-96df-4f5b711a32cc",
    "visible": true
}