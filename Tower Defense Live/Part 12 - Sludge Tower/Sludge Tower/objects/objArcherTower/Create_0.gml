/// @description Initialize
//Set Scale
image_xscale = .25;
image_yscale = .25;

fireRate = 30;
radius = 128;
damage = 5;

myEnemyType = objGroundEnemyParent;
myProjectile = objArrow;