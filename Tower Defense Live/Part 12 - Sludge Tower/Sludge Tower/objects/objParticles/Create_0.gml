/// @description Create Particles

//Fireball System
global.PartFireballSystem = part_system_create_layer("Instances", true);

//Fireball Particle
global.FireballPart = part_type_create();
part_type_shape(global.FireballPart, pt_shape_explosion);
part_type_size(global.FireballPart, .2, .4, 0, 0);
part_type_speed(global.FireballPart, 1, 2, 0, 0);
part_type_life(global.FireballPart, 25, 35);
part_type_color_mix(global.FireballPart, c_red, make_color_rgb(145, 64, 37));
part_type_blend(global.FireballPart, true);