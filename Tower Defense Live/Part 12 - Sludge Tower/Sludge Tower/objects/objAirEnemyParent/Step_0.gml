/// @description Set Custom Path
// Inherit the parent event
event_inherited();

if(path_index != myPath)
	path_start(myPath, 1, path_action_stop, true);