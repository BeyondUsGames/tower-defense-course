/// @description Fire at the enemy
if(nearestEnemy != -4) {
	audio_play_sound(sndArrowShot, 10, false);
	arrow = instance_create_layer(x, y, layer, myProjectile);
	with(arrow) {
		speed = 6;
		direction = point_direction(x, y, ShootAt("x"), ShootAt("y"));
		image_angle = direction;
		myDamage = other.damage;
	}
	alarm[0] = fireRate;
}