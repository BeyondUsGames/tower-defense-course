/// @description Initialize
//Setting enemy array
enemies[0, 0] = objFlyingSkeleton; //Enemy to spawn
enemies[0, 1] = 5; //Amount to spawn
enemies[1, 0] = objCat;
enemies[1, 1] = 5;
enemies[2, 0] = objGoblin;
enemies[2, 1] = 5;

myEnemy = enemies[0, 0];
currentEnemies = 0;
maxEnemies = 0;
for(var i = 0; i < array_height_2d(enemies); ++i) {
	maxEnemies += enemies[i, 1];
}
myPath = pTraining;
arraySpot = 0;

decreaser = 4;
spawnRate = 120;
alarm[0] = spawnRate;

//Create tower spawner
instance_create_layer(x, y, layer, objTowerMaker);
//Create Player
with(instance_create_layer(x, y, layer, objPlayer)) {
	maxHealth = 5;
	currentHealth = maxHealth;
	gold = 50;
}