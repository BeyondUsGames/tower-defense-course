/// @description Spawn an enemy
if(enemies[arraySpot, 1] <= 0)
	arraySpot = clamp(++arraySpot, 0, array_height_2d(enemies) - 1);
var enemy = instance_create_layer(x, y, layer, enemies[arraySpot, 0]);
with(enemy) {
	path_start(other.myPath, 2, path_action_stop, true);
}
++currentEnemies;
enemies[arraySpot, 1] -= 1;


if(currentEnemies < maxEnemies) {
	spawnRate -= decreaser;
	if(spawnRate < 1)
		spawnRate = 1;
	alarm[0] = spawnRate;
}