/// @description Draw Tower Base
draw_sprite_ext(sprTowerBase, 0, x, y, .25, .25, 0, c_white, 1);
draw_self();
//Draw Radius
draw_set_alpha(.25);
draw_set_colour(c_red);
draw_circle(x, y, radius, true);

//Draw arrow
draw_sprite_ext(sprArrow, 0, x, y, .2, .2, image_angle, c_white, 1);