{
    "id": "448e0a62-4e5f-4a16-81e1-90995c6c92dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objScreenShake",
    "eventList": [
        {
            "id": "049191bd-3c91-4d97-b79a-6476314aa7c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "448e0a62-4e5f-4a16-81e1-90995c6c92dd"
        },
        {
            "id": "832d8494-88ac-4d82-9dd9-b2ebab3493d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "448e0a62-4e5f-4a16-81e1-90995c6c92dd"
        },
        {
            "id": "07b9857d-1295-410b-9799-6613bbbec6ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "448e0a62-4e5f-4a16-81e1-90995c6c92dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}