{
    "id": "ee9df7c5-2a35-49e0-8fa1-c52e9ecc6186",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite37",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9c1b634-d2e9-425e-aa29-3a62694b8b7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9df7c5-2a35-49e0-8fa1-c52e9ecc6186",
            "compositeImage": {
                "id": "da08d0b9-dd4b-4919-a7ea-8e193567d955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c1b634-d2e9-425e-aa29-3a62694b8b7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c1354c-813e-489d-acc2-72c57fa772f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c1b634-d2e9-425e-aa29-3a62694b8b7f",
                    "LayerId": "9f8ed974-e5b3-4aa2-9d39-1adedcc25b16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9f8ed974-e5b3-4aa2-9d39-1adedcc25b16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee9df7c5-2a35-49e0-8fa1-c52e9ecc6186",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}