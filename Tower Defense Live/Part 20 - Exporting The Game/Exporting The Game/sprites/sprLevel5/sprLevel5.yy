{
    "id": "9a80175a-8b81-4faa-940a-ecb9a720a2e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ca11fba-f549-43d6-9f2b-167fdc8d871e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a80175a-8b81-4faa-940a-ecb9a720a2e8",
            "compositeImage": {
                "id": "c127757a-efa2-4ddd-9fbb-b8690eb26c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca11fba-f549-43d6-9f2b-167fdc8d871e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "124f77b6-ea05-41c0-b7e8-c23084402ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca11fba-f549-43d6-9f2b-167fdc8d871e",
                    "LayerId": "0e7e4f8d-9825-43c5-8819-9576d5a0fdae"
                }
            ]
        },
        {
            "id": "2c01dcbb-f85d-4ede-b305-1a82375278e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a80175a-8b81-4faa-940a-ecb9a720a2e8",
            "compositeImage": {
                "id": "024fbcdb-26d9-43fb-b6a3-21bc1032d38d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c01dcbb-f85d-4ede-b305-1a82375278e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0325f4a-ba6a-43ab-8737-d9e39fad507f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c01dcbb-f85d-4ede-b305-1a82375278e3",
                    "LayerId": "0e7e4f8d-9825-43c5-8819-9576d5a0fdae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0e7e4f8d-9825-43c5-8819-9576d5a0fdae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a80175a-8b81-4faa-940a-ecb9a720a2e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}