{
    "id": "8eb0da1a-7425-4f79-bdd6-c61736e4fb95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTextBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 146,
    "bbox_left": 0,
    "bbox_right": 598,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "813bf5b7-2d24-49d7-898f-97f465d0fada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eb0da1a-7425-4f79-bdd6-c61736e4fb95",
            "compositeImage": {
                "id": "e6b2104d-5e64-4e27-b51d-52f1c71cd50d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "813bf5b7-2d24-49d7-898f-97f465d0fada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2202df65-aa85-4d87-ba74-b596ed0c2482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "813bf5b7-2d24-49d7-898f-97f465d0fada",
                    "LayerId": "dd72d8d1-96f2-4f1f-9341-a28363eda4e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 147,
    "layers": [
        {
            "id": "dd72d8d1-96f2-4f1f-9341-a28363eda4e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8eb0da1a-7425-4f79-bdd6-c61736e4fb95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 599,
    "xorig": 299,
    "yorig": 73
}