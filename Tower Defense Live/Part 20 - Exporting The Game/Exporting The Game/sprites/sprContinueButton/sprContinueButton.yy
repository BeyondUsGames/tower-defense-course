{
    "id": "a9c0c605-3f39-449f-b127-31f38204b9c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprContinueButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 227,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4eb215c1-a993-4000-9480-875ffd4ea547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9c0c605-3f39-449f-b127-31f38204b9c1",
            "compositeImage": {
                "id": "95726d04-64fd-4abc-b3bf-d853633cf9a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb215c1-a993-4000-9480-875ffd4ea547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3502985-79e6-4c79-9708-8ddadbe836c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb215c1-a993-4000-9480-875ffd4ea547",
                    "LayerId": "6c0a4ebf-97df-4110-befc-a03991ec76c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "6c0a4ebf-97df-4110-befc-a03991ec76c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9c0c605-3f39-449f-b127-31f38204b9c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 228,
    "xorig": 114,
    "yorig": 17
}