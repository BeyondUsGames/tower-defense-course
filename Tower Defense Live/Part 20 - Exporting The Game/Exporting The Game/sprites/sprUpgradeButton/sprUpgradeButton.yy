{
    "id": "e2cbc8f3-04f3-474b-8fe2-4271878ef090",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprUpgradeButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cfae49d-bc3e-4611-aab4-d3710d06fcf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2cbc8f3-04f3-474b-8fe2-4271878ef090",
            "compositeImage": {
                "id": "5077fd80-e5a9-4d0d-9e69-86ff57c71dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cfae49d-bc3e-4611-aab4-d3710d06fcf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf6db74-0cb2-4a55-a2e4-cce869f62150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cfae49d-bc3e-4611-aab4-d3710d06fcf1",
                    "LayerId": "fc93aa44-9581-4c74-b032-69581cf32746"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "fc93aa44-9581-4c74-b032-69581cf32746",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2cbc8f3-04f3-474b-8fe2-4271878ef090",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}