{
    "id": "cfc82476-5592-4272-878a-c72acfc3d088",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1c126e8-6731-4cd7-a4ad-39f7ae9c695a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfc82476-5592-4272-878a-c72acfc3d088",
            "compositeImage": {
                "id": "03298c06-89c5-4d04-97b2-794acad4da65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c126e8-6731-4cd7-a4ad-39f7ae9c695a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "592b9ce8-51af-47f2-a8b8-046cb6f6c283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c126e8-6731-4cd7-a4ad-39f7ae9c695a",
                    "LayerId": "2a95d7d5-9a88-4ba1-8cf5-a7a7d32d7e75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "2a95d7d5-9a88-4ba1-8cf5-a7a7d32d7e75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfc82476-5592-4272-878a-c72acfc3d088",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}