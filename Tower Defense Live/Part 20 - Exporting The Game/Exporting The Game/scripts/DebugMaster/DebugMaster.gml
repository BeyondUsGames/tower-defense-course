///For Debugging Purposes
if(room == rmOverworld) {
	for(var i = 0; i < instance_number(objOverworldLevel); ++i) {
		with(instance_find(objOverworldLevel, i))
			starRating = 3;
	}
}

if(room != rmOverworld) {
	objPlayer.gold = 9999;
	objPlayer.currentHealth = 9999;
}