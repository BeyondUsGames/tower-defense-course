///Show message in a text box
message = argument0;

var box = instance_create_depth(room_width / 2, room_height - sprite_get_height(sprTextBox), depth - 3000, objTextBox);
with(box)
	message = other.message;