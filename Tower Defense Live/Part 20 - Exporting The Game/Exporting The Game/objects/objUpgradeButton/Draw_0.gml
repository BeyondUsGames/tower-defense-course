/// @description Draw Info
draw_self();

draw_set_font(fntUpgradeText);
draw_set_colour(c_black);
draw_set_halign(fa_center);

if(drawInfo) {
	draw_text(x, y - 64, "Current Level: " + string(myTower.currentLevel + 1));
	draw_text(x, y - 96, "Cost: " + string(myTower.levelStats[myTower.currentLevel, UpgradeCost]));
	draw_text(x, y - 128, "Damage: " + string(myTower.damage) + " -----> " + string(myTower.levelStats[myTower.currentLevel + 1, Damage]));
	draw_text(x, y - 160, "Sight: " + string(myTower.radius) + " -----> " + string(myTower.levelStats[myTower.currentLevel + 1, Radius]));
	draw_text(x, y - 192, "Fire Rate: " + string(myTower.fireRate) + " -----> " + string(myTower.levelStats[myTower.currentLevel + 1, FireRate]));
}