{
    "id": "36167bcf-6962-44cb-a2a0-d72e83d16e30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUpgradeButton",
    "eventList": [
        {
            "id": "31cefae8-6990-4e38-b2ce-cbeb542282a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36167bcf-6962-44cb-a2a0-d72e83d16e30"
        },
        {
            "id": "97667abb-5bf2-4ad9-a0ae-fa6beab443e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "36167bcf-6962-44cb-a2a0-d72e83d16e30"
        },
        {
            "id": "a3793a4c-90d0-4ec3-9287-e13c04eefba1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "36167bcf-6962-44cb-a2a0-d72e83d16e30"
        },
        {
            "id": "4fe0534d-a77c-4711-9161-98877df8ae5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "36167bcf-6962-44cb-a2a0-d72e83d16e30"
        },
        {
            "id": "16d71139-8f13-44c2-b46f-7dab54e62863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 6,
            "m_owner": "36167bcf-6962-44cb-a2a0-d72e83d16e30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ad1ecdc5-4519-4a83-8c7f-84a4ae297146",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2cbc8f3-04f3-474b-8fe2-4271878ef090",
    "visible": true
}