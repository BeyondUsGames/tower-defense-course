{
    "id": "3fba11e0-60a8-4c31-9407-4dd01f446651",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGroundEnemyParent",
    "eventList": [
        {
            "id": "70c58a76-956b-442c-8db2-704a2e381326",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "25df52b8-a898-4507-a8cc-d8e69c370e03",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3fba11e0-60a8-4c31-9407-4dd01f446651"
        },
        {
            "id": "7b4d4ec6-ec87-46e1-a087-6313d3a77180",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3fba11e0-60a8-4c31-9407-4dd01f446651"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e890d949-2c7f-4c73-90e5-6267e9ddf00a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}