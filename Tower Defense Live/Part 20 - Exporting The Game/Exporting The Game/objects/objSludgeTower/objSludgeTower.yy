{
    "id": "f6d16409-ca79-4d4e-a48e-9fd61690dbdc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSludgeTower",
    "eventList": [
        {
            "id": "07336eb3-5672-4972-8f77-6c41731a23b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6d16409-ca79-4d4e-a48e-9fd61690dbdc"
        },
        {
            "id": "b2cdef9a-1fae-4f2f-9336-4cffd755d3be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f6d16409-ca79-4d4e-a48e-9fd61690dbdc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "952ef05d-7ad2-446b-80ea-67522250a0f1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b8ed226-cff1-4a84-a51f-0e9a1f000747",
    "visible": true
}