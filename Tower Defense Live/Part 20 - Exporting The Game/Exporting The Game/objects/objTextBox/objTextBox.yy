{
    "id": "a2dd4b5d-1f24-4560-8116-9525a03ce24f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTextBox",
    "eventList": [
        {
            "id": "c0405f35-f982-4de2-ae67-c27b414e06e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2dd4b5d-1f24-4560-8116-9525a03ce24f"
        },
        {
            "id": "2e0d165b-bf3d-4324-872b-c5adfef6f578",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2dd4b5d-1f24-4560-8116-9525a03ce24f"
        },
        {
            "id": "128f4404-15fe-4fc4-896e-f5945847c410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a2dd4b5d-1f24-4560-8116-9525a03ce24f"
        },
        {
            "id": "33860e96-2025-40bf-87cc-5f1c1fb76cc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a2dd4b5d-1f24-4560-8116-9525a03ce24f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8eb0da1a-7425-4f79-bdd6-c61736e4fb95",
    "visible": true
}