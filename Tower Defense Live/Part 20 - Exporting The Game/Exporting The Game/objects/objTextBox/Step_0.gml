/// @description Add to the current text
count = clamp(count + .75, 0, string_length(message));
currentText = string_copy(message, 0, count);

//Destroy Self
if(keyboard_check(vk_anykey) || mouse_check_button(mb_any))
	instance_destroy();