/// @description Initialize Tower
//Set Scale
image_xscale = .25;
image_yscale = .25;

//Upgrade Stats
levelStats[TowerLevel1, FireRate] = 60;
levelStats[TowerLevel1, Radius] = 256;
levelStats[TowerLevel1, Damage] = 10;
levelStats[TowerLevel1, UpgradeCost] = 40;

levelStats[TowerLevel2, FireRate] = 50;
levelStats[TowerLevel2, Radius] = 312;
levelStats[TowerLevel2, Damage] = 15;
levelStats[TowerLevel2, UpgradeCost] = 80;

levelStats[TowerLevel3, FireRate] = 40;
levelStats[TowerLevel3, Radius] = 364;
levelStats[TowerLevel3, Damage] = 20;
levelStats[TowerLevel3, UpgradeCost] = MaxedOut;

levelStats[TowerLevel4, FireRate] = MaxedOut;
levelStats[TowerLevel4, Radius] = MaxedOut;
levelStats[TowerLevel4, Damage] = MaxedOut;

fireRate = levelStats[TowerLevel1, FireRate];
radius = levelStats[TowerLevel1, Radius];
damage = levelStats[TowerLevel1, Damage];
currentLevel = 0;

myEnemyType = objEnemyParent;
myProjectile = objFireball;
mySound = sndFireball;