{
    "id": "e0d72850-6acd-411c-b74f-fb4d9252030e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFlyingSkeleton",
    "eventList": [
        {
            "id": "f0fb59a7-361b-4155-ac6e-f03beab4364c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0d72850-6acd-411c-b74f-fb4d9252030e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "657e8d2e-c2d6-4a45-b5d6-0771103ef212",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
    "visible": true
}