/// @description Initialize
randomize();
global.AvailableTowers = 0;

/*
//Archer Tower
global.AvailableTowers[0, TowerSprite] = sprArcherTower;
global.AvailableTowers[0, TowerObject] = objArcherTower;
global.AvailableTowers[0, TowerCost] = ArcherTowerCost;
//Fireball Tower
global.AvailableTowers[1, TowerSprite] = sprFireballTower;
global.AvailableTowers[1, TowerObject] = objFireballTower;
global.AvailableTowers[1, TowerCost] = FireballTowerCost;
//Sludge Tower
global.AvailableTowers[2, TowerSprite] = sprSludgeTower;
global.AvailableTowers[2, TowerObject] = objSludgeTower;
global.AvailableTowers[2, TowerCost] = SludgeTowerCost;
*/

backgroundMusic[0] = sndBackgroundMusic1;
backgroundMusic[1] = sndBackgroundMusic2;
backgroundMusic[2] = sndBackgroundMusic3;

currentSong = irandom(2);
audio_play_sound(backgroundMusic[currentSong], 100, false);

totalKills = 0;