{
    "id": "fc3c9657-7ed3-4d6a-9893-d35edf098e71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPausedCursor",
    "eventList": [
        {
            "id": "2ccf0426-d64e-4703-b8ce-7a553dafb564",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc3c9657-7ed3-4d6a-9893-d35edf098e71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cfc82476-5592-4272-878a-c72acfc3d088",
    "visible": true
}