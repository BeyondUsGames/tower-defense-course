///Draw Level Info
draw_set_alpha(1);
draw_set_colour(c_dkgray);
draw_set_halign(fa_center);
draw_set_font(fntBigText);

//Current Level
draw_text(room_width - sprite_get_width(sprLevelInfoFrame) / 2 - 32, room_height - 180, "Level " + string(objLevelParent.myLevelNumber));
draw_line(room_width - sprite_get_width(sprLevelInfoFrame) / 2 - 120, room_height - 140,
		room_width - sprite_get_width(sprLevelInfoFrame) / 2 + 50, room_height - 140);
		
//Monsters Spawned
draw_text(room_width - sprite_get_width(sprLevelInfoFrame) + 100, room_height - 125, "Spawned: " + 
		string(currentEnemies) + "\\" + string(maxEnemies));

//Draw Time To Start
draw_set_halign(fa_left);
if(currentEnemies == 0) {
	draw_text(room_width / 2 - 128, room_height / 2 - 32, "Level Will Begin In: " + string(alarm[0] / 60));
}