///Set Up Macros

//Tower Info
#macro TowerSprite 0
#macro TowerObject 1
#macro TowerCost 2

#macro ArcherTowerCost 25
#macro FireballTowerCost 50
#macro SludgeTowerCost 40