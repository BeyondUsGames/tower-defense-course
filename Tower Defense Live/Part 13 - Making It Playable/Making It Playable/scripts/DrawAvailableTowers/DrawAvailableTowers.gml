//Draw Tower Options
draw_set_alpha(1);
draw_set_font(fntBigText);
draw_set_halign(fa_center);
//Draw UI Frame
draw_sprite(sprTowerFrame, 0, 32, 780);
for(var i = 0; i < array_height_2d(global.AvailableTowers); ++i) {
	//Draw Tower & Base
	draw_sprite_ext(sprTowerBase, 0, (i * 112) + 128, room_height - 80, .3, .3, 0, c_white, 1);
	draw_sprite_ext(global.AvailableTowers[i, TowerSprite], 0, (i * 112) + 128, room_height - 80, .3, .3, 90, c_white, 1);
	//Draw Their Cost
	if(objPlayer.gold < global.AvailableTowers[i, TowerCost])
		draw_set_color(c_red);
	else
		draw_set_colour(make_color_rgb(255, 212, 0));
	draw_text((i * 112) + 128, room_height - 170, "$" + string(global.AvailableTowers[i, TowerCost]));
	//Draw Frame Around Towers
	if(i == currentTower)
		draw_sprite_ext(sprCurrentTowerFrame, 0, (i * 112) + 128, room_height - 80, .3, .3, 0, c_white, 1);
	else
		draw_sprite_ext(sprDefaultTowerFrame, 0, (i * 112) + 128, room_height - 80, .3, .3, 0, c_white, 1);
}