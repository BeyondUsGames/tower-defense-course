/// @description Fire at the enemy
if(nearestEnemy != -4) {
	audio_play_sound(mySound, 10, false);
	projectile = instance_create_layer(x, y, layer, myProjectile);
	with(projectile) {
		speed = 6;
		direction = point_direction(x, y, other.nearestEnemy.x, other.nearestEnemy.y);
		image_angle = direction;
		myDamage = other.damage;
	}
	alarm[0] = fireRate;
}