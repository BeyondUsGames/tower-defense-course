/// @description Initialize Tower
//Set Scale
image_xscale = .25;
image_yscale = .25;

fireRate = 60;
radius = 256;
damage = 10;

myEnemyType = objEnemyParent;
myProjectile = objFireball;
mySound = sndFireball;