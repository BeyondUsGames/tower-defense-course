{
    "id": "8ed2b3d1-c5b8-4bcf-bd00-3ba9f907bd6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprArcherTower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 53,
    "bbox_right": 250,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b473977-f057-4b5f-b0f2-76841cf88207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ed2b3d1-c5b8-4bcf-bd00-3ba9f907bd6d",
            "compositeImage": {
                "id": "684f0900-6548-492b-bc73-17d9f7bfe726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b473977-f057-4b5f-b0f2-76841cf88207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6fbd38-07c7-43d3-9305-850daf4626bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b473977-f057-4b5f-b0f2-76841cf88207",
                    "LayerId": "6de4055e-62ae-4c01-a228-9131e71abff0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6de4055e-62ae-4c01-a228-9131e71abff0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ed2b3d1-c5b8-4bcf-bd00-3ba9f907bd6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}