/// @description Draw Health & info
draw_set_alpha(1);
//Draw Frame
draw_sprite(sprLevelInfoFrame, 0, room_width - 32, room_height - 32);

//Draw Hearts
for(var i = 0; i < currentHealth; ++i) {
	draw_sprite(sprHeart, 0, room_width - (sprite_get_width(sprHeart) * maxHealth) + 
	(i * 32) - 64, room_height - 78);
}

//Draw gold
draw_set_font(fntBigText);
draw_set_color(c_white);
draw_text(room_width - 500, room_height - 108, "Gold " + "$" + string(gold));