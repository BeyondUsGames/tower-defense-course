{
    "id": "ce83b506-5869-4d48-98b2-1c8d6aa94eb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCat",
    "eventList": [
        {
            "id": "54496070-54cc-4d30-96ee-c65e560db974",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce83b506-5869-4d48-98b2-1c8d6aa94eb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fba11e0-60a8-4c31-9407-4dd01f446651",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
    "visible": true
}