{
    "id": "8409fadb-b0ea-45a4-b578-272f25d483ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objScorpion",
    "eventList": [
        {
            "id": "667ca97a-4c33-48ac-b107-fdb2d2c632b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8409fadb-b0ea-45a4-b578-272f25d483ca"
        },
        {
            "id": "68b47a89-6579-47bd-8207-1cae648fdfbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8409fadb-b0ea-45a4-b578-272f25d483ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3fba11e0-60a8-4c31-9407-4dd01f446651",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
    "visible": true
}