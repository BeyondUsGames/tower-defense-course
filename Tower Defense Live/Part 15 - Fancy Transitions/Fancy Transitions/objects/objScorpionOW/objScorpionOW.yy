{
    "id": "720e5e1f-3b66-46a7-8eb5-9cd1fdc33f0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objScorpionOW",
    "eventList": [
        {
            "id": "56161f30-5d8c-4271-9d55-9d5edaa1f195",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "720e5e1f-3b66-46a7-8eb5-9cd1fdc33f0b"
        },
        {
            "id": "91d285d5-2e30-4d63-9762-e294d2d7a071",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "720e5e1f-3b66-46a7-8eb5-9cd1fdc33f0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
    "visible": true
}