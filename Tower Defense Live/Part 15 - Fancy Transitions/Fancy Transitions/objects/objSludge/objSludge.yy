{
    "id": "25df52b8-a898-4507-a8cc-d8e69c370e03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSludge",
    "eventList": [
        {
            "id": "48c54b44-a6e4-4305-bdaa-f0b1d95c1374",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25df52b8-a898-4507-a8cc-d8e69c370e03"
        },
        {
            "id": "1432894a-d254-4e3e-84be-917f25a445e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "25df52b8-a898-4507-a8cc-d8e69c370e03"
        },
        {
            "id": "79215f3c-e3b8-461e-9183-a7c6c0fd2769",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "25df52b8-a898-4507-a8cc-d8e69c370e03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c2d4e493-0261-4d83-aca2-f42f4ff4daee",
    "visible": true
}