/// @description Inherit and Draw More

// Inherit the parent event
event_inherited();

//Draw arrow
draw_sprite_ext(sprArrow, 0, x, y, .2, .2, image_angle, c_white, 1);