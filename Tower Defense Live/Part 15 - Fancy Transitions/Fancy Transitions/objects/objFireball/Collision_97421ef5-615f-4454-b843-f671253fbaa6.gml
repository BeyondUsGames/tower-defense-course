/// @description Damage and Die
--waitToDie;
if(waitToDie <= 0) {
	instance_destroy();
	with(other)
		currentHealth -= other.myDamage;
}