/// @description Initialize
currentFrame = 0;
maxFrames = 160;
persistent = true;

var halfHeight, halfWidth;
halfHeight = surface_get_height(application_surface) / 2;
halfWidth = surface_get_width(application_surface) / 2;

leftX = GetLeftX();
rightX = GetRightX();
topY = GetTopY();
bottomY = GetBottomY();

sprRoomTopLeft = sprite_create_from_surface(application_surface, 0, 0, halfWidth, halfHeight, false, false, 0, 0);
sprRoomTopRight = sprite_create_from_surface(application_surface, halfWidth, 0, halfWidth, halfHeight, false, false, halfWidth, 0);
sprRoomBottomLeft = sprite_create_from_surface(application_surface, 0, halfHeight, halfWidth, halfHeight, false, false, 0, halfHeight);
sprRoomBottomRight = sprite_create_from_surface(application_surface, halfWidth, halfHeight, halfWidth, halfHeight, false, false, halfWidth, halfHeight);

///Define Room and Set To Change
newRoom = undefined;
alarm[0] = 1;