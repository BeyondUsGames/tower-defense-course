/// @description Draw Transitions
if(currentFrame > 0) {
	//Draw Fade First
	draw_set_colour(c_black);
	draw_set_alpha(1 - EaseInQuad(currentFrame, 0, 1, maxFrames));
	draw_rectangle(GetLeftX(), GetTopY(), GetRightX(), GetBottomY(), false);
	
	draw_set_alpha(1);
	
	//Convert Frames to a number between 0 and 90 for our angle
	var rotation = EaseOutCubic(currentFrame, 0, 90, maxFrames);
	
	draw_sprite_ext(sprRoomTopLeft, 0, GetLeftX(), GetTopY(), 1, 1, rotation, c_white, 1);
	draw_sprite_ext(sprRoomTopRight, 0, GetRightX(), GetTopY(), 1, 1, rotation, c_white, 1);
	draw_sprite_ext(sprRoomBottomLeft, 0, GetLeftX(), GetBottomY(), 1, 1, rotation, c_white, 1);
	draw_sprite_ext(sprRoomBottomRight, 0, GetRightX(), GetBottomY(), 1, 1, rotation, c_white, 1);
}