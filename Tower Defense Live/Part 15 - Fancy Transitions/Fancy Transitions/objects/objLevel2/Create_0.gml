/// @description Initialize
//Inherit
event_inherited();

//Setting enemy array
enemies[0, 0] = objFlyingSkeleton; //Enemy to spawn
enemies[0, 1] = 1; //Amount to spawn
enemies[1, 0] = objGoblin;
enemies[1, 1] = 7;
enemies[2, 0] = objCat;
enemies[2, 1] = 12;

myEnemy = enemies[0, 0];

for(var i = 0; i < array_height_2d(enemies); ++i) {
	maxEnemies += enemies[i, 1];
}
myPath = pLevel2;

myLevelNumber = 2;

decreaser = 4;
spawnRate = 120;
alarm[0] = spawnRate + 180;


//Create Player
with(instance_create_layer(x, y, layer, objPlayer)) {
	maxHealth = 5;
	currentHealth = maxHealth;
	gold = 75;
}

global.AvailableTowers = 0;

//Archer Tower
global.AvailableTowers[0, TowerSprite] = sprArcherTower;
global.AvailableTowers[0, TowerObject] = objArcherTower;
global.AvailableTowers[0, TowerCost] = ArcherTowerCost;
//Fireball Tower
global.AvailableTowers[1, TowerSprite] = sprFireballTower;
global.AvailableTowers[1, TowerObject] = objFireballTower;
global.AvailableTowers[1, TowerCost] = FireballTowerCost;