///Set Up Macros

//Tower Info
#macro TowerSprite 0
#macro TowerObject 1
#macro TowerCost 2

#macro ArcherTowerCost 25
#macro FireballTowerCost 50
#macro SludgeTowerCost 40

//Saving & Loading
#macro FileName "Tower_Defense_Data.ini"
#macro Level "Level"
#macro StarRating "Star_Rating"
#macro KillCount "Kill_Count"