///EaseInQuad(input value, output minimum, output max, input max)

argument0 /= argument3;
return argument2 * argument0 * argument0 + argument1;