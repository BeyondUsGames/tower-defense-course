/// @description Draw The Grid
if(mouse_check_button(mb_left)) {
	//Draw Grid on map
	draw_set_alpha(1);
	draw_set_colour(c_black);
	for(var i = 0; i < gridWidth; ++i) {
		//Vertical Lines
		draw_line(i * 64, 0, i * 64, room_height - 192);
		//Horizontal Lines
		if(i < 13)
			draw_line(0, i * 64, room_width, i * 64);
	}
	sprite_index = sprTowerBase;
	image_xscale = .25;
	image_yscale = .25;
	image_alpha = .5;
}
else {
	sprite_index = sprCursor;
	image_alpha = 1;
	image_yscale = 1;
	image_xscale = 1;
}

draw_self();