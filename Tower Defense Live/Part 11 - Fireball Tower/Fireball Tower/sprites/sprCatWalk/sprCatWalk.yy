{
    "id": "ef7ecab5-8906-4937-b1db-60a601181dbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCatWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 230,
    "bbox_left": 0,
    "bbox_right": 225,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62b2983b-f6d8-487d-ad49-5a02604f58ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "1cad4c17-953c-4ba6-a843-b6eccc2e7e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b2983b-f6d8-487d-ad49-5a02604f58ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8886739c-48df-41f0-b5f1-6b753140dbc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b2983b-f6d8-487d-ad49-5a02604f58ef",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "c8d9b5e7-173a-4325-bd09-1739c9128419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "31222a2b-c9c2-4195-878b-f81b678f3d57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8d9b5e7-173a-4325-bd09-1739c9128419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06e80707-4d1d-4ff2-bc28-0cb3147a3225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8d9b5e7-173a-4325-bd09-1739c9128419",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "d218ea63-33e7-48f9-be8f-adbb41067fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "7e7a5df6-1c72-49e0-88c0-c574651cfdbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d218ea63-33e7-48f9-be8f-adbb41067fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17baff19-dcdf-4fcc-b011-d8ebc1de0b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d218ea63-33e7-48f9-be8f-adbb41067fdd",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "47ddc419-8f9c-4d46-abf0-d60627d6eb52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "ec8b6ce7-85d0-48c0-b141-a02ffdc31cb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47ddc419-8f9c-4d46-abf0-d60627d6eb52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82437c06-ed86-4c97-8db0-4868a8a12dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47ddc419-8f9c-4d46-abf0-d60627d6eb52",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "9bd831f9-69a9-4033-9b6c-9a677b74fc5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "0aa9171e-5241-4aa4-b023-3ea2f9515c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd831f9-69a9-4033-9b6c-9a677b74fc5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4928f3-d1cc-4baf-983e-b9a784a726ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd831f9-69a9-4033-9b6c-9a677b74fc5a",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "76ea209d-7cee-44f8-a889-f4ed02080738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "070a4408-1c15-4e2b-9dc3-abfd59298f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ea209d-7cee-44f8-a889-f4ed02080738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d506f7fe-2d2c-4ee0-a05e-4e1a1e423a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ea209d-7cee-44f8-a889-f4ed02080738",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "2d42b07c-004c-4c3f-883c-5085cded2889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "a66a254a-8edf-4da2-a48c-fa7ca211e5e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d42b07c-004c-4c3f-883c-5085cded2889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8487ca-734c-4d04-8d26-c4083cdda09f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d42b07c-004c-4c3f-883c-5085cded2889",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "608b3fbe-2c0b-4358-9cee-c51182ffaf9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "2d20f950-e29f-486b-82df-44cacb3d1499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "608b3fbe-2c0b-4358-9cee-c51182ffaf9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0226c3-3242-425b-b5c0-0a515f705879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "608b3fbe-2c0b-4358-9cee-c51182ffaf9a",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "df11e118-ca95-436f-b6dc-989265620f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "88b6b618-24a0-43b2-b593-b7805c326f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df11e118-ca95-436f-b6dc-989265620f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6e2377-2413-47c0-b073-5d3dff7ee306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df11e118-ca95-436f-b6dc-989265620f38",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "c6bb47bf-bdec-43c9-bd8e-50866d13157d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "94b0cb2d-71c9-4a01-85bf-07cc76792219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6bb47bf-bdec-43c9-bd8e-50866d13157d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc08d77-e363-430b-ac7e-d2a82a925d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6bb47bf-bdec-43c9-bd8e-50866d13157d",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "2002061f-3172-461c-8b18-ea37b90ed968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "bdbe1141-17e2-4f55-bc43-5edbe85924aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2002061f-3172-461c-8b18-ea37b90ed968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "877606c7-242e-4314-98f1-59bd8690f5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2002061f-3172-461c-8b18-ea37b90ed968",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "9766ec62-2f0c-4f7f-b815-a0bc59ae77bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "c36c47ab-3349-49a4-ba99-b8b5d9295878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9766ec62-2f0c-4f7f-b815-a0bc59ae77bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2511bf92-51d3-46ba-aa96-5ad00abdda1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9766ec62-2f0c-4f7f-b815-a0bc59ae77bc",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "9647e102-e09b-49b0-9379-b772d3440680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "277d20dc-ce6b-42d8-84ec-ffc489daa4de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9647e102-e09b-49b0-9379-b772d3440680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb469b79-aecf-42e0-bc68-03bf109afd39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9647e102-e09b-49b0-9379-b772d3440680",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "cd1612cb-a386-48ce-8e18-51cb252c1d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "83100831-a4e2-4a5b-a7bc-52b018d3f073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1612cb-a386-48ce-8e18-51cb252c1d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65bb5a7e-09d8-44e5-8d7a-77d43a817c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1612cb-a386-48ce-8e18-51cb252c1d90",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "538146d0-62cc-4465-86ad-b17eceb5dd7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "064c143b-6437-4434-96d9-96beea57d74d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "538146d0-62cc-4465-86ad-b17eceb5dd7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c91facb-5b56-4e11-835e-5dda45ba2f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538146d0-62cc-4465-86ad-b17eceb5dd7d",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "e7d78e25-be8f-409e-88e0-b58ae4bce585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "6d44f72c-1903-48cb-8808-320ff15e2520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d78e25-be8f-409e-88e0-b58ae4bce585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b136b332-359b-4aee-aa99-09f223f018c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d78e25-be8f-409e-88e0-b58ae4bce585",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "7ad3160d-8de8-4d38-b670-6cb32773c825",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "8c25823a-91a9-49ea-9fc1-1b3ee99caf6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad3160d-8de8-4d38-b670-6cb32773c825",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caffff73-baa5-4d06-b7a4-d9068f28a354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad3160d-8de8-4d38-b670-6cb32773c825",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "4832e4d4-0d1f-4a27-808e-0fb853329751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "90bf04ef-a0a2-421a-ba3e-026265b9e5a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4832e4d4-0d1f-4a27-808e-0fb853329751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd1d77f9-ebe8-41fa-8610-21504c89a048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4832e4d4-0d1f-4a27-808e-0fb853329751",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "357af5c0-339c-44df-9509-700105a022d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "deb512fe-4171-45e4-baae-ac2e7ceb899e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "357af5c0-339c-44df-9509-700105a022d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a37e01-1c26-444d-b6aa-4a6aa9fe1a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "357af5c0-339c-44df-9509-700105a022d7",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        },
        {
            "id": "4feb4ff1-b760-4af6-826e-aa49f7962312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "compositeImage": {
                "id": "653b70d4-ddc0-42ab-a4ec-59cf047056b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4feb4ff1-b760-4af6-826e-aa49f7962312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd817d58-7f2c-4873-8129-a64052ff9fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4feb4ff1-b760-4af6-826e-aa49f7962312",
                    "LayerId": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 231,
    "layers": [
        {
            "id": "0dbdabc9-6e69-4172-bf70-1d0433c8a8a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 226,
    "xorig": 113,
    "yorig": 115
}