{
    "id": "ce7fd142-7d6c-4d0f-844b-c222aabcf7e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevelInfoFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 162,
    "bbox_left": 0,
    "bbox_right": 522,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69d80a79-6e79-407f-877e-153bee415699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce7fd142-7d6c-4d0f-844b-c222aabcf7e5",
            "compositeImage": {
                "id": "0845eb65-f91f-47ba-9d34-334cbb9e4787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d80a79-6e79-407f-877e-153bee415699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8318908-dcdd-4aad-99b4-bac7e199c3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d80a79-6e79-407f-877e-153bee415699",
                    "LayerId": "793e700a-0179-4cd0-bbf1-867275fc45c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 163,
    "layers": [
        {
            "id": "793e700a-0179-4cd0-bbf1-867275fc45c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce7fd142-7d6c-4d0f-844b-c222aabcf7e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 523,
    "xorig": 522,
    "yorig": 162
}