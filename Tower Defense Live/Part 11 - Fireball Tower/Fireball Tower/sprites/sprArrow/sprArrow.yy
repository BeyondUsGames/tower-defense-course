{
    "id": "ad88cd9a-91de-4fbf-ac59-cba73f8e3a8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 366,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9464e912-22a9-4c0d-8203-85330b8662ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad88cd9a-91de-4fbf-ac59-cba73f8e3a8d",
            "compositeImage": {
                "id": "c1b7cac9-851c-4e78-b087-aed81a752f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9464e912-22a9-4c0d-8203-85330b8662ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48edf301-f79f-4adf-9add-98d6a86e63ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9464e912-22a9-4c0d-8203-85330b8662ba",
                    "LayerId": "669ae7e4-1003-4bf5-9154-47a34c253371"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "669ae7e4-1003-4bf5-9154-47a34c253371",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad88cd9a-91de-4fbf-ac59-cba73f8e3a8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 367,
    "xorig": 183,
    "yorig": 20
}