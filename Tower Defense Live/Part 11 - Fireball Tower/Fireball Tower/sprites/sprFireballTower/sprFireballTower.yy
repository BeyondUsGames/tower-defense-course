{
    "id": "5d4a7046-f118-4f75-96df-4f5b711a32cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFireballTower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 196,
    "bbox_left": 80,
    "bbox_right": 183,
    "bbox_top": 88,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dd4575d-ef42-4aea-b7d0-882b73f25e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d4a7046-f118-4f75-96df-4f5b711a32cc",
            "compositeImage": {
                "id": "e5f66599-db76-4c08-a9eb-e50e02edd879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd4575d-ef42-4aea-b7d0-882b73f25e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e877d877-cf43-483d-b551-e888e8a7bb84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd4575d-ef42-4aea-b7d0-882b73f25e0d",
                    "LayerId": "a54ea26a-9afd-450d-b521-5c154fa2d59b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a54ea26a-9afd-450d-b521-5c154fa2d59b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d4a7046-f118-4f75-96df-4f5b711a32cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}