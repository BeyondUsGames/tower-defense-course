{
    "id": "469442c4-f590-4967-a89e-1d8841ca0e4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTowerMaker",
    "eventList": [
        {
            "id": "d0fe0fbf-26b2-46b7-b7ba-b79a9d35566c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "469442c4-f590-4967-a89e-1d8841ca0e4d"
        },
        {
            "id": "e9369616-f393-401b-910e-1b01695050ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "469442c4-f590-4967-a89e-1d8841ca0e4d"
        },
        {
            "id": "cfceab53-4da1-436c-a1ef-9433f6c9f619",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "469442c4-f590-4967-a89e-1d8841ca0e4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cfc82476-5592-4272-878a-c72acfc3d088",
    "visible": true
}