/// @description Follow Mouse
x = mouse_x;
y = mouse_y;

myGridSpotX = round(mouse_x / gridSize);
myGridSpotY = round(mouse_y / gridSize);

if(mouse_check_button_released(mb_left)) {
	sprite_index = sprCursor;
	image_blend = c_white;
	//Align tower to grid
	if(myGridSpotX mod 2 == 0) {
		++myGridSpotX;
		x = myGridSpotX * gridSize;
	}
	if(myGridSpotY mod 2 == 0) {
		++myGridSpotY;
		y = myGridSpotY * gridSize;
	}
	if(GoodToBuild())
		instance_create_layer(myGridSpotX * gridSize, myGridSpotY * gridSize, "Instances", objFireballTower);
}