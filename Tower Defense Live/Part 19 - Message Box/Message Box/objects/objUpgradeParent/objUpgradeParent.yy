{
    "id": "ad1ecdc5-4519-4a83-8c7f-84a4ae297146",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUpgradeParent",
    "eventList": [
        {
            "id": "480e7863-51cf-4556-8ddb-221f1decc08a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad1ecdc5-4519-4a83-8c7f-84a4ae297146"
        },
        {
            "id": "d69a43dc-72e5-49c9-b741-7806d9c46a5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 57,
            "eventtype": 6,
            "m_owner": "ad1ecdc5-4519-4a83-8c7f-84a4ae297146"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}