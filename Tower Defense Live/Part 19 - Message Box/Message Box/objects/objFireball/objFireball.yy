{
    "id": "1a880a9f-2e1d-4476-a2f1-27e99a7612d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFireball",
    "eventList": [
        {
            "id": "6365aa92-57e0-4363-95ec-a9e33ee7b86d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1a880a9f-2e1d-4476-a2f1-27e99a7612d3"
        },
        {
            "id": "3381a49c-730a-432c-9be9-6c641c486fc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a880a9f-2e1d-4476-a2f1-27e99a7612d3"
        },
        {
            "id": "c2308fbb-3172-4bce-b06b-0df08cd14400",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1a880a9f-2e1d-4476-a2f1-27e99a7612d3"
        },
        {
            "id": "97421ef5-615f-4454-b843-f671253fbaa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e890d949-2c7f-4c73-90e5-6267e9ddf00a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a880a9f-2e1d-4476-a2f1-27e99a7612d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db16d4fa-0632-4e7f-bc90-d743374bf371",
    "visible": true
}