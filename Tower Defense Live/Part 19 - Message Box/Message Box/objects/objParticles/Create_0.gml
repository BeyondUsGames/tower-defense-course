/// @description Create Particles

//Fireball System
global.PartFireballSystem = part_system_create_layer("Instances", true);

//Fireball Particle
global.FireballPart = part_type_create();
part_type_shape(global.FireballPart, pt_shape_explosion);
part_type_size(global.FireballPart, .2, .4, 0, 0);
part_type_speed(global.FireballPart, 1, 2, 0, 0);
part_type_life(global.FireballPart, 25, 35);
part_type_color_mix(global.FireballPart, c_red, make_color_rgb(145, 64, 37));
part_type_blend(global.FireballPart, true);

////Fireworks Particles
//System
global.PartFireworkSystem = part_system_create();

//Rocket Particle
global.PartRocket = part_type_create();
part_type_direction(global.PartRocket, 80, 100, 0, 0);
part_type_speed(global.PartRocket, 15, 18, 0, 0);
part_type_life(global.PartRocket, 60, 80);
part_type_gravity(global.PartRocket, .15, 270);
part_type_alpha1(global.PartRocket, 0);

//Trail for the rocket
global.PartTrail = part_type_create();
part_type_colour2(global.PartTrail, c_dkgray, c_ltgray);
part_type_gravity(global.PartTrail, 0.1, 270);
part_type_shape(global.PartTrail, pt_shape_cloud);
part_type_life(global.PartTrail, 30, 40);
part_type_alpha2(global.PartTrail, 0.8, 0);
part_type_size(global.PartTrail, .3, .4, -.01, 0);

//Explosion
global.PartExplode = part_type_create();
part_type_shape(global.PartExplode, pt_shape_flare);
part_type_size(global.PartExplode, .5, .7, -.001, 0)
part_type_alpha2(global.PartExplode, 1, 0);
part_type_direction(global.PartExplode, 0, 359, 0, 0);
part_type_speed(global.PartExplode, 2, 8, 0, 0);
part_type_life(global.PartExplode, 30, 60);
part_type_blend(global.PartExplode, true);
part_type_gravity(global.PartExplode, .1, 270);
part_type_colour_rgb(global.PartExplode, 0, 255, 0, 255, 0, 255);

//Sequence for the fireworks
part_type_step(global.PartRocket, 1, global.PartTrail);
part_type_death(global.PartRocket, 150, global.PartExplode);

//Emitter
global.PartFireworkEmitter = part_emitter_create(global.PartFireworkSystem);
part_emitter_region(global.PartFireworkSystem, global.PartFireworkEmitter, 0,
			room_width, room_height + 300, room_height + 300, ps_shape_line, ps_distr_linear);