/// @description Initialize
//Inherit
event_inherited();

//Setting enemy array
enemies[0, 0] = objScorpion; //Enemy to spawn
enemies[0, 1] = 20; //Amount to spawn

myEnemy = enemies[0, 0];

for(var i = 0; i < array_height_2d(enemies); ++i) {
	maxEnemies += enemies[i, 1];
}
myPath = pLevel3;

myLevelNumber = 3;

decreaser = 5;
spawnRate = 140;
alarm[0] = spawnRate + 180;


//Create Player
with(instance_create_layer(x, y, layer, objPlayer)) {
	maxHealth = 5;
	currentHealth = maxHealth;
	gold = 90;
}

global.AvailableTowers = 0;

//Archer Tower
global.AvailableTowers[0, TowerSprite] = sprArcherTower;
global.AvailableTowers[0, TowerObject] = objArcherTower;
global.AvailableTowers[0, TowerCost] = ArcherTowerCost;
//Sludge Tower
global.AvailableTowers[1, TowerSprite] = sprSludgeTower;
global.AvailableTowers[1, TowerObject] = objSludgeTower;
global.AvailableTowers[1, TowerCost] = SludgeTowerCost;