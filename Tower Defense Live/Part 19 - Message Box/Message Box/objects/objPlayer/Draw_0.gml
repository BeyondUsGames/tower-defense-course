/// @description Draw Health & info
draw_set_alpha(1);
//Draw Frame
draw_sprite(sprLevelInfoFrame, 0, room_width - 32, room_height - 16);

//Draw Hearts
for(var i = 0; i < currentHealth; ++i) {
	draw_sprite(sprHeart, 0, room_width - (sprite_get_width(sprHeart) * maxHealth) + 
	(i * 32) - 64, room_height - 78);
}

//Draw gold
draw_set_font(fntBigText);
draw_set_colour(make_color_rgb(255, 212, 0));
draw_text(room_width - 468, room_height - 80, "Gold " + "$" + string(gold));