/// @description Check Health
if(currentHealth <= 0) {
	ShowMessage("You've failed. Try again next time, I'm sure you can beat it.");
	deathTimer -= 1;
}

if(deathTimer <= 0)
	room_goto(rmOverworld);