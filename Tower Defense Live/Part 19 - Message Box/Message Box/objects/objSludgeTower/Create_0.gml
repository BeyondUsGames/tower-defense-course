/// @description Initialize
//Set Scale
image_xscale = .25;
image_yscale = .25;

//Upgrade Stats
levelStats[TowerLevel1, FireRate] = 120;
levelStats[TowerLevel1, Radius] = 96;
levelStats[TowerLevel1, Damage] = 0;
levelStats[TowerLevel1, UpgradeCost] = 25;

levelStats[TowerLevel2, FireRate] = 100;
levelStats[TowerLevel2, Radius] = 164;
levelStats[TowerLevel2, Damage] = 0;
levelStats[TowerLevel2, UpgradeCost] = 50;

levelStats[TowerLevel3, FireRate] = 80;
levelStats[TowerLevel3, Radius] = 196;
levelStats[TowerLevel3, Damage] = 0.1;
levelStats[TowerLevel3, UpgradeCost] = MaxedOut;

levelStats[TowerLevel4, FireRate] = MaxedOut;
levelStats[TowerLevel4, Radius] = MaxedOut;
levelStats[TowerLevel4, Damage] = MaxedOut;


fireRate = levelStats[TowerLevel1, FireRate];
radius = levelStats[TowerLevel1, Radius];
damage = levelStats[TowerLevel1, Damage];
currentLevel = 0;

myEnemyType = objGroundEnemyParent;
myProjectile = objSludge;
mySound = sndSludge;