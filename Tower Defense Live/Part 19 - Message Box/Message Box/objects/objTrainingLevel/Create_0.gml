/// @description Initialize
//Inherit
event_inherited();
//Setting enemy array
enemies[0, 0] = objScorpion; //Enemy to spawn
enemies[0, 1] = 5; //Amount to spawn
/*enemies[1, 0] = objCat;
enemies[1, 1] = 5;
enemies[2, 0] = objGoblin;
enemies[2, 1] = 5;*/

myEnemy = enemies[0, 0];

for(var i = 0; i < array_height_2d(enemies); ++i) {
	maxEnemies += enemies[i, 1];
}
myPath = pTraining;

myLevelNumber = 0;

decreaser = 4;
spawnRate = 120;
alarm[0] = spawnRate + 180;


//Create Player
with(instance_create_layer(x, y, layer, objPlayer)) {
	maxHealth = 5;
	currentHealth = maxHealth;
	gold = 50;
}


global.AvailableTowers = 0;
//Archer Tower
global.AvailableTowers[0, TowerSprite] = sprArcherTower;
global.AvailableTowers[0, TowerObject] = objArcherTower;
global.AvailableTowers[0, TowerCost] = ArcherTowerCost;
//Fireball Tower
global.AvailableTowers[1, TowerSprite] = sprFireballTower;
global.AvailableTowers[1, TowerObject] = objFireballTower;
global.AvailableTowers[1, TowerCost] = FireballTowerCost;
//Sludge Tower
global.AvailableTowers[2, TowerSprite] = sprSludgeTower;
global.AvailableTowers[2, TowerObject] = objSludgeTower;
global.AvailableTowers[2, TowerCost] = SludgeTowerCost;