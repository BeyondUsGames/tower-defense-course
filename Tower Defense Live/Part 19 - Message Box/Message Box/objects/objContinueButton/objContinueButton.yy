{
    "id": "3a90759b-2de6-436b-90a7-ab77f9abfdfd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objContinueButton",
    "eventList": [
        {
            "id": "790de05c-e0ba-4247-b015-bb44e892e1e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3a90759b-2de6-436b-90a7-ab77f9abfdfd"
        },
        {
            "id": "827cc163-4f2c-4dad-a6d2-64d60399b449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a90759b-2de6-436b-90a7-ab77f9abfdfd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9c0c605-3f39-449f-b127-31f38204b9c1",
    "visible": true
}