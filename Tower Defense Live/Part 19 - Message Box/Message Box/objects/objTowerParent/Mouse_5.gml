/// @description Create Options
with(instance_create_depth(x - 64, y, depth - 10, objDeleteButton))
	myTower = other.id;
with(instance_create_depth(x + 64, y, depth - 10, objUpgradeButton))
	myTower = other.id;