{
    "id": "84383fc5-1a06-4b36-8964-2f179842cc0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objArcherTower",
    "eventList": [
        {
            "id": "1898148d-3faf-4571-a2ac-bd33e61177af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        },
        {
            "id": "7917e687-aac1-4293-a0e6-e64ec2fe69c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "952ef05d-7ad2-446b-80ea-67522250a0f1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ed2b3d1-c5b8-4bcf-bd00-3ba9f907bd6d",
    "visible": true
}