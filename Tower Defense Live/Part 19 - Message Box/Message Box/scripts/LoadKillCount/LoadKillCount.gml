///Load Total Kills

ini_open(FileName);

var kills = 0;
for(var i = 0; i < 6; /*Change if adding new levels */ ++i) {
	kills += ini_read_real(Level + string(i), KillCount, 0);
}

ini_close();

return kills;