{
    "id": "f835e074-2037-47b6-b8bc-85946e2a4c43",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel3",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "9cc5824d-c14e-482f-879d-cc601ef2b7e7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 576,
            "speed": 100
        },
        {
            "id": "f6b70482-2c17-49df-b833-cd7ce73a8f47",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 576,
            "speed": 100
        },
        {
            "id": "2a8a6953-6f70-4e40-9f36-7fcf2aeff535",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 64,
            "speed": 100
        },
        {
            "id": "8e9d88e8-34fa-41f7-aac3-2b195d08684e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 64,
            "speed": 100
        },
        {
            "id": "9b20eb0f-6b4f-4448-9638-eb71904d7e69",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 576,
            "speed": 100
        },
        {
            "id": "668e9628-53f6-4496-8db9-892e4f45b8ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 576,
            "speed": 100
        },
        {
            "id": "3277fe31-1b7b-44e9-a353-ff7c3ff5903c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 928,
            "y": 64,
            "speed": 100
        },
        {
            "id": "227f7b21-702d-4386-8a2d-206bca30d441",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 64,
            "speed": 100
        },
        {
            "id": "66035ac6-4085-49ce-b231-663d97153a4a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 576,
            "speed": 100
        },
        {
            "id": "1cdf1170-42f0-45e5-bc83-efb67b8ab1d1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 576,
            "speed": 100
        },
        {
            "id": "0d0ae53d-bb0f-41d0-89b7-a0c3d4d92997",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1504,
            "y": 64,
            "speed": 100
        },
        {
            "id": "40792436-bd13-4012-9da8-21110e93de9e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 64,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}