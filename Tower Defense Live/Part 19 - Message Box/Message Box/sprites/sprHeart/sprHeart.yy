{
    "id": "f6166bbd-61cf-481c-b11f-22fbdb007a3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77cf3ee7-513f-446f-a472-50bf7d37eb3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6166bbd-61cf-481c-b11f-22fbdb007a3c",
            "compositeImage": {
                "id": "75fc150d-cb15-45ff-b885-7916748a2ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77cf3ee7-513f-446f-a472-50bf7d37eb3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28f1ba0c-40be-4b9a-97f1-42fb2ffc495d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77cf3ee7-513f-446f-a472-50bf7d37eb3b",
                    "LayerId": "ffad716a-9216-4e3a-8bdf-576d2ae52014"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ffad716a-9216-4e3a-8bdf-576d2ae52014",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6166bbd-61cf-481c-b11f-22fbdb007a3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}