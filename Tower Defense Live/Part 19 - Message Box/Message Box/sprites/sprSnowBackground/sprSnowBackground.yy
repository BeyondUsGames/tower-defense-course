{
    "id": "3ce025cd-b3d0-4e4c-adff-4e01c953bb67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSnowBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6b61bba-327c-4d23-a720-c00c20a94e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ce025cd-b3d0-4e4c-adff-4e01c953bb67",
            "compositeImage": {
                "id": "58c6f519-dfdd-43f7-a3d6-f64fb732daaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b61bba-327c-4d23-a720-c00c20a94e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcea66e-02ae-497e-97a9-c52459d89187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b61bba-327c-4d23-a720-c00c20a94e7d",
                    "LayerId": "56dfe5ea-9a9f-40b5-bf91-79a3fdd25897"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "56dfe5ea-9a9f-40b5-bf91-79a3fdd25897",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ce025cd-b3d0-4e4c-adff-4e01c953bb67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}