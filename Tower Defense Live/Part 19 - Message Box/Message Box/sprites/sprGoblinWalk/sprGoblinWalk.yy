{
    "id": "be650552-5de2-433e-bf36-05bd09f87c88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c703cc85-4e78-44cc-ba93-f0e78c2ca3cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "493c07c3-5479-4304-842e-8896d79187f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c703cc85-4e78-44cc-ba93-f0e78c2ca3cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e4f8d0a-ec23-4ef5-8a4e-7a5d44fe5773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c703cc85-4e78-44cc-ba93-f0e78c2ca3cb",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "edebc829-a04a-4775-bca5-9df809ebcf53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "ff1d9f35-f203-4f70-aa86-e9b2ce4368c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edebc829-a04a-4775-bca5-9df809ebcf53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b6701da-746d-45b0-a296-6c4c3f6105cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edebc829-a04a-4775-bca5-9df809ebcf53",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "08ebb177-5d57-4389-837c-fd64b47a4904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "109ef35d-d7ba-4de6-9d7d-9043564fa7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ebb177-5d57-4389-837c-fd64b47a4904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15af05af-fbef-43ef-8d57-3deb19985b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ebb177-5d57-4389-837c-fd64b47a4904",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "148be5d8-d19e-4a9b-9056-781f456c43d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "4c379e11-7373-456a-9dc7-ab72a94d91c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148be5d8-d19e-4a9b-9056-781f456c43d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f3e8e5-9b29-40d2-bd8f-57c9203ef0b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148be5d8-d19e-4a9b-9056-781f456c43d2",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "28f115a9-c977-4faa-9f78-93453960c0d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "4897a344-fec4-4f7d-b261-d76c4f03e0d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f115a9-c977-4faa-9f78-93453960c0d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdb4b588-3897-439d-85b9-2f67f99170fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f115a9-c977-4faa-9f78-93453960c0d8",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "3f575b17-0526-48c5-a6bf-38881151d014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "60225ff5-f8e3-4e9b-a0ff-b5067c56453a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f575b17-0526-48c5-a6bf-38881151d014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e01075-5cdc-4518-a01d-9fe07d8adb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f575b17-0526-48c5-a6bf-38881151d014",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "3da05547-4059-4f0c-b3d8-54b0e30dda8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "d49e3fa1-3f17-4dfb-bc88-cf540c58413a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da05547-4059-4f0c-b3d8-54b0e30dda8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7deaaa55-1907-4347-826a-dbe3393b7234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da05547-4059-4f0c-b3d8-54b0e30dda8d",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "2227eb88-8cc6-42ad-90a0-16f92305462a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "a345b088-6f56-4eb6-8f5b-9cba056ef894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2227eb88-8cc6-42ad-90a0-16f92305462a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd8de9a-25e3-458b-a6e0-e587685da0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2227eb88-8cc6-42ad-90a0-16f92305462a",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "dced7304-d272-4de3-b354-8309486f7529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "cdb15f03-d7e0-4657-aee9-b777dfe2ebcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dced7304-d272-4de3-b354-8309486f7529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a2dd413-0288-44c6-ad72-724d4eee6c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dced7304-d272-4de3-b354-8309486f7529",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "af249bec-9a7c-46b9-9c5c-f0fb9f24eee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "cb92c11f-f342-45af-b18b-7a229bea09c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af249bec-9a7c-46b9-9c5c-f0fb9f24eee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e6e0eff-65c8-488b-ab6f-4b4349ad222d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af249bec-9a7c-46b9-9c5c-f0fb9f24eee0",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "01633466-7031-4b30-8963-e2b28766c558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "21d73543-2068-43a0-ab3f-f935d39ea6ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01633466-7031-4b30-8963-e2b28766c558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ba486a-8e93-4557-9f73-1a734cf46307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01633466-7031-4b30-8963-e2b28766c558",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "06b9cb26-f0b5-4593-9c27-aeb9e310b1bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "e1de7908-e8db-4867-bdda-c4f94851166e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b9cb26-f0b5-4593-9c27-aeb9e310b1bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be226c7-41de-4656-ba29-0abadbb620ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b9cb26-f0b5-4593-9c27-aeb9e310b1bc",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "1238a4e5-79b6-4ac5-b5ae-5831519a7dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "d22e09fb-4c75-432c-be91-d347d4e955cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1238a4e5-79b6-4ac5-b5ae-5831519a7dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30de9b57-237c-4889-9b3c-703de29b0d77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1238a4e5-79b6-4ac5-b5ae-5831519a7dcd",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "df005be8-1c4f-434d-9798-88eb0c041950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "7a83584a-cbc6-46a3-9e6d-8e19b9735578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df005be8-1c4f-434d-9798-88eb0c041950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e762efa6-1be0-4d44-a921-06cb505c39b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df005be8-1c4f-434d-9798-88eb0c041950",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "16310a6a-1ed0-4213-8abe-f58d02dc5452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "8c6881e5-8985-45ae-8eb9-1bbc85d51991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16310a6a-1ed0-4213-8abe-f58d02dc5452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61c6c2f8-c520-4c6d-9b06-4c09bb952dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16310a6a-1ed0-4213-8abe-f58d02dc5452",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "d71f2be2-9382-416e-bc79-c7e1889f6e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "505b3366-4a86-4c0f-a885-4c452607bcd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71f2be2-9382-416e-bc79-c7e1889f6e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40e7c763-63f9-4127-934a-f32cb9f54902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71f2be2-9382-416e-bc79-c7e1889f6e00",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "ff3bb074-d204-4a33-903a-cf3f0066c21f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "248fe4e1-2b61-494d-b51a-a68886f34960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3bb074-d204-4a33-903a-cf3f0066c21f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5c0deb-65bf-4c89-a7c7-51c6b668c6c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3bb074-d204-4a33-903a-cf3f0066c21f",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "ef83c86e-adb5-4084-9950-df2a4f7b0555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "a4397477-3c32-41c3-8906-c5393d19c823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef83c86e-adb5-4084-9950-df2a4f7b0555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8687ac49-71ae-497c-8236-33dbc1801806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef83c86e-adb5-4084-9950-df2a4f7b0555",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "3bf49ae2-5ac1-40e7-b9f4-2860e9041316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "5b38cf03-27e3-43b5-b029-bfdd8cf4423e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf49ae2-5ac1-40e7-b9f4-2860e9041316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fef475f-0e5c-472b-8177-4fe8c83b8f15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf49ae2-5ac1-40e7-b9f4-2860e9041316",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        },
        {
            "id": "f78370de-94a9-4141-9318-18b3edcc2f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "compositeImage": {
                "id": "823a1b9d-d07b-4e4d-8f5b-0d827d7bc577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78370de-94a9-4141-9318-18b3edcc2f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdac7a9b-4fad-43b5-a23d-562bbb3b4424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78370de-94a9-4141-9318-18b3edcc2f01",
                    "LayerId": "f653be15-bba7-4c31-b298-55c8a2c46af9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 228,
    "layers": [
        {
            "id": "f653be15-bba7-4c31-b298-55c8a2c46af9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 35,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 91,
    "yorig": 114
}