{
    "id": "db16d4fa-0632-4e7f-bc90-d743374bf371",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d94c78cc-f5bd-4b4b-a6ec-fc2e626a513b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db16d4fa-0632-4e7f-bc90-d743374bf371",
            "compositeImage": {
                "id": "5bd097e4-187a-4a67-97ff-06328ee37697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d94c78cc-f5bd-4b4b-a6ec-fc2e626a513b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d7b63b6-563a-465b-995b-6a553b8f2d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d94c78cc-f5bd-4b4b-a6ec-fc2e626a513b",
                    "LayerId": "678ffd58-e236-4031-9570-0657886febfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "678ffd58-e236-4031-9570-0657886febfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db16d4fa-0632-4e7f-bc90-d743374bf371",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}