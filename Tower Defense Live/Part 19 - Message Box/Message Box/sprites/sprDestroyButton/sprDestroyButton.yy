{
    "id": "d7872139-bb51-44fd-a260-e4c1a949701e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDestroyButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf0df108-44c1-45df-82b4-97e8cdfb28b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7872139-bb51-44fd-a260-e4c1a949701e",
            "compositeImage": {
                "id": "7102ce2b-e312-43ad-bbc1-7011a4bbde89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf0df108-44c1-45df-82b4-97e8cdfb28b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23959535-f71b-4b01-af6c-e0a7ccfa2f63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf0df108-44c1-45df-82b4-97e8cdfb28b2",
                    "LayerId": "df8b844d-9c31-4345-9062-d60aecc0ac54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "df8b844d-9c31-4345-9062-d60aecc0ac54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7872139-bb51-44fd-a260-e4c1a949701e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}