{
    "id": "06f53249-e00b-40f1-b0dd-38e02d38c5e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoblin",
    "eventList": [
        {
            "id": "66d6b308-3e5b-4525-80df-4a97bff4c51d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "06f53249-e00b-40f1-b0dd-38e02d38c5e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
    "visible": true
}