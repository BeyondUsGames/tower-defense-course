/// @description Spawn an enemy
var enemy = instance_create_layer(x, y, layer, myEnemy);
with(enemy) {
	path_start(other.myPath, 2, path_action_stop, true);
}
++currentEnemies;

if(currentEnemies < enemiesToSpawn) {
	spawnRate -= decreaser;
	if(spawnRate < 1)
		spawnRate = 1;
	alarm[0] = spawnRate;
}