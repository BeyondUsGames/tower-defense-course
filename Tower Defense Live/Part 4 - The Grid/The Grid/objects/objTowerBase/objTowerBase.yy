{
    "id": "84383fc5-1a06-4b36-8964-2f179842cc0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTowerBase",
    "eventList": [
        {
            "id": "1898148d-3faf-4571-a2ac-bd33e61177af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54cc4b8a-cf7c-4185-a285-4e373a4e55c1",
    "visible": true
}