{
    "id": "4f2e591c-4b7a-4078-8e44-23e1d5a40cc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objArrow",
    "eventList": [
        {
            "id": "9855ab42-3550-4444-b24f-044a01824ebb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f2e591c-4b7a-4078-8e44-23e1d5a40cc8"
        },
        {
            "id": "11802aef-59a5-46a7-9360-11720c746d62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "06f53249-e00b-40f1-b0dd-38e02d38c5e4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4f2e591c-4b7a-4078-8e44-23e1d5a40cc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad88cd9a-91de-4fbf-ac59-cba73f8e3a8d",
    "visible": true
}