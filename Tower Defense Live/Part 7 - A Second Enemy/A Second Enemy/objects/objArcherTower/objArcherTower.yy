{
    "id": "84383fc5-1a06-4b36-8964-2f179842cc0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objArcherTower",
    "eventList": [
        {
            "id": "1898148d-3faf-4571-a2ac-bd33e61177af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        },
        {
            "id": "fbdc3850-cb7b-4edb-a0a6-ba459a12fe9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        },
        {
            "id": "e307463a-4e84-4954-b14d-2330e6a05a87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        },
        {
            "id": "befd0a02-e493-4961-ad77-e438df3f6312",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "84383fc5-1a06-4b36-8964-2f179842cc0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ed2b3d1-c5b8-4bcf-bd00-3ba9f907bd6d",
    "visible": true
}