/// @description Looking For Enemy
//Check audio levels
audio_sound_gain(sndArrowShot, .15 / instance_number(object_index), 0);

nearestEnemy = collision_circle(x, y, radius, objCat, false, true);

if(nearestEnemy != -4) {
	var tower = point_direction(x, y, nearestEnemy.x, nearestEnemy.y);
	var angle = angle_difference(image_angle, tower);
	image_angle -= min(abs(angle), 10) * sign(angle) / 2;
	
	//Fire at them
	if(alarm[0] == -1) {
		event_perform(ev_alarm, 0);
	}
}
else
	image_angle += .75;