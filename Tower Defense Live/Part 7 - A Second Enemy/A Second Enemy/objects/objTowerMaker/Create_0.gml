/// @description Initialize
window_set_cursor(cr_none);

gridSize = 32;
gridWidth = floor(room_width / gridSize);
gridHeight = floor(room_height / gridSize);

depth -= 500;