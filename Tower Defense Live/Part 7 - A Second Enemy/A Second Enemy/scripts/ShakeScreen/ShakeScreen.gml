////Shake the screen an amount
/// @description Shake The Screen
/// @arg amount The intensity of the shake
/// @param length How long to shake

screenShakeAmount = argument0;
screenShakeLength = argument1;

var shaker = instance_create_layer(x, y, layer, objScreenShake);
shaker.shakeAmount = screenShakeAmount;
shaker.alarm[0] = screenShakeLength * 60;