///Save Level Info
var level = Level + string(myLevelNumber);

ini_open(FileName);

ini_write_real(level, StarRating, starRating);

var prevKills = ini_read_real(level, KillCount, 0);
ini_write_real(level, KillCount, killCount + prevKills);

ini_close();