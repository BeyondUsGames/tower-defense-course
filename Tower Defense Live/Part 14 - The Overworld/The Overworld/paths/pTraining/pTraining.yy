{
    "id": "912c3b35-0490-4b5c-8703-55458416465b",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pTraining",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "e73c0e49-5799-40b8-916f-d50133f2ee22",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32,
            "speed": 100
        },
        {
            "id": "9daf4167-f1b3-40fb-8956-23d0b5fd599f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 32,
            "speed": 100
        },
        {
            "id": "83862de8-e51e-40f9-ac72-e70e0be03b8f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 224,
            "speed": 100
        },
        {
            "id": "bb1976e2-04d5-4320-bc27-b6ff17a2ce88",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 224,
            "speed": 100
        },
        {
            "id": "f2732153-0b0a-4fbc-a576-0b6dd1922000",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 416,
            "speed": 100
        },
        {
            "id": "283a9d77-cd4d-4b19-becb-3df28e7f952e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 416,
            "speed": 100
        },
        {
            "id": "367ab1ae-c7e4-4bd0-a94a-7f770e7ff524",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 608,
            "speed": 100
        },
        {
            "id": "f392133e-f6a3-4a3a-9f06-9dc42580e738",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 608,
            "speed": 100
        },
        {
            "id": "c6ec2aaf-15df-4b28-9f1f-e1c42f6c75f4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 224,
            "speed": 100
        },
        {
            "id": "05bb6fa7-f28c-4efa-9256-077673afc736",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 224,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}