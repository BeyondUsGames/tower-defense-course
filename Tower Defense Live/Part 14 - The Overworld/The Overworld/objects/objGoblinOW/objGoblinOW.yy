{
    "id": "88fb6c11-3536-4d12-bbcd-af316d2e05cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoblinOW",
    "eventList": [
        {
            "id": "d8d0c37a-ad0b-4be5-9501-f037cc0f9eb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "88fb6c11-3536-4d12-bbcd-af316d2e05cd"
        },
        {
            "id": "6969b5b4-7223-4a46-9dc1-c6e5c6fb4115",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "88fb6c11-3536-4d12-bbcd-af316d2e05cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be650552-5de2-433e-bf36-05bd09f87c88",
    "visible": true
}