/// @description Change Rooms
if(myLevelNumber == 0 || prevLevelObject.starRating != 0)
	room_goto(myRoom);
else
	show_message("You must complete the levels in order.");