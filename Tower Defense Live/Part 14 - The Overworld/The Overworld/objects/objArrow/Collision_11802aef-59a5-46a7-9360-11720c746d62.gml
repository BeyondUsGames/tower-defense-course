/// @description Destroy self, damage enemy
instance_destroy();
with(other)
	currentHealth -= other.myDamage;