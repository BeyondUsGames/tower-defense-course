
{
    "name": "rmOverworld",
    "id": "22190a59-5453-4c1b-bea6-414ded228d38",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "3aaa253f-94eb-4ba4-931d-28bac8a161a1",
        "3f834f00-d95f-4080-b694-b814e8af7bbe",
        "fcc3c2e9-049c-435f-bbe7-975b3379a03e",
        "8fe87543-f675-4529-ae0d-afc66b4bee97",
        "b703b654-001b-4ba7-a9fe-2a141015690d",
        "8011c58d-ea57-442d-b122-f2995bdb6bfe",
        "f185c6f5-4b26-4277-a115-f4762c502811",
        "b6f3c46a-ce09-4b2d-8959-f82ec121f00c"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "5cc7cd47-d60b-4cf4-b6db-a93060f0c3f8",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_64767155","id": "3aaa253f-94eb-4ba4-931d-28bac8a161a1","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_64767155.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_64767155","objId": "49eff524-4f42-43b4-85da-07808232e68a","properties": [{"id": "bae33a6b-6f4b-491f-8be0-240303fadef2","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "dcc52a31-0fc0-4614-8289-857ce05e636a","mvc": "1.0","value": "rmTraining"},{"id": "fc9ed282-51e9-41ab-ac94-a8268ab48dab","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "29652437-4499-4d52-b5ad-87ac971b5184","mvc": "1.0","value": "sprLevelTraining"},{"id": "f67e7225-8a04-4f6b-833a-0dd069abba09","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "a9d89448-105c-4c84-a587-79e66a2a9bdf","mvc": "1.0","value": "0"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 64},
{"name": "inst_7AE0A945","id": "3f834f00-d95f-4080-b694-b814e8af7bbe","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7AE0A945","objId": "49eff524-4f42-43b4-85da-07808232e68a","properties": [{"id": "4ba91bea-6b56-452f-bcf8-d8ef9a348909","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "dcc52a31-0fc0-4614-8289-857ce05e636a","mvc": "1.0","value": "rmLevel1"},{"id": "a8b2df42-e382-4e85-a7c5-c681f430bfc0","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "29652437-4499-4d52-b5ad-87ac971b5184","mvc": "1.0","value": "sprLevel1"},{"id": "11a0faf2-e342-4b63-837f-896ca278e6d8","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "a9d89448-105c-4c84-a587-79e66a2a9bdf","mvc": "1.0","value": "1"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 64},
{"name": "inst_43378AFA","id": "fcc3c2e9-049c-435f-bbe7-975b3379a03e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_43378AFA","objId": "49eff524-4f42-43b4-85da-07808232e68a","properties": [{"id": "1aa9124b-c392-453b-a437-88d651108869","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "dcc52a31-0fc0-4614-8289-857ce05e636a","mvc": "1.0","value": "rmLevel2"},{"id": "abe889ed-3adc-47e7-9945-058f4e59b99e","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "29652437-4499-4d52-b5ad-87ac971b5184","mvc": "1.0","value": "sprLevel2"},{"id": "d3b7db1d-f471-420c-9203-8f6b7ef44e33","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "a9d89448-105c-4c84-a587-79e66a2a9bdf","mvc": "1.0","value": "2"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 320,"y": 64},
{"name": "inst_2134644","id": "8fe87543-f675-4529-ae0d-afc66b4bee97","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2134644","objId": "49eff524-4f42-43b4-85da-07808232e68a","properties": [{"id": "a05f59a8-7cdd-4aac-985d-5be42e29f73b","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "dcc52a31-0fc0-4614-8289-857ce05e636a","mvc": "1.0","value": "rmLevel3"},{"id": "e5f22918-af7b-42ec-a1d1-b6c13fd7104c","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "29652437-4499-4d52-b5ad-87ac971b5184","mvc": "1.0","value": "sprLevel3"},{"id": "134a1d45-6642-4b6a-9870-ed5424684014","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "a9d89448-105c-4c84-a587-79e66a2a9bdf","mvc": "1.0","value": "3"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 448,"y": 64},
{"name": "inst_63242B68","id": "b703b654-001b-4ba7-a9fe-2a141015690d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_63242B68","objId": "49eff524-4f42-43b4-85da-07808232e68a","properties": [{"id": "4413c153-0baa-4523-abb8-cf982d74354e","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "dcc52a31-0fc0-4614-8289-857ce05e636a","mvc": "1.0","value": "rmLevel4"},{"id": "03360dd5-da60-47f4-936b-b57cb86eec6d","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "29652437-4499-4d52-b5ad-87ac971b5184","mvc": "1.0","value": "sprLevel4"},{"id": "63ee91fc-8cd6-4a31-b080-a241495036b1","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "a9d89448-105c-4c84-a587-79e66a2a9bdf","mvc": "1.0","value": "4"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 576,"y": 64},
{"name": "inst_2711F4A3","id": "8011c58d-ea57-442d-b122-f2995bdb6bfe","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2711F4A3","objId": "49eff524-4f42-43b4-85da-07808232e68a","properties": [{"id": "370857ef-8914-40f2-8da3-3077a3f472d9","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "dcc52a31-0fc0-4614-8289-857ce05e636a","mvc": "1.0","value": "rmLevel5"},{"id": "37ea8f17-2f15-4f95-93b1-14dfd0caf957","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "29652437-4499-4d52-b5ad-87ac971b5184","mvc": "1.0","value": "sprLevel5"},{"id": "64e0cd4c-b9df-4f3b-a010-f469c64a83f5","modelName": "GMOverriddenProperty","objectId": "49eff524-4f42-43b4-85da-07808232e68a","propertyId": "a9d89448-105c-4c84-a587-79e66a2a9bdf","mvc": "1.0","value": "5"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 704,"y": 64},
{"name": "inst_383CF034","id": "f185c6f5-4b26-4277-a115-f4762c502811","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_383CF034","objId": "961b36d7-1204-4f35-91f8-a40259c2aeba","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 672,"y": 448},
{"name": "inst_4D0DA872","id": "b6f3c46a-ce09-4b2d-8959-f82ec121f00c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4D0DA872","objId": "fa2632bc-9e3a-42e4-b93e-0afe00979050","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "4fdf20dc-482f-47e5-b7d8-71f39ec8f63a",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": -1,
            "htiled": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "17f3f40b-4071-4402-ad61-5b32b2e44df2",
            "stretch": true,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "60f13b16-cd66-485b-952b-fa964832413a",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "b017c07c-cea8-47f1-af64-3dec7b417606",
        "Height": 480,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 800
    },
    "mvc": "1.0",
    "views": [
{"id": "3d708cb6-a5e4-48a6-ab30-173173cd4caa","hborder": 32,"hport": 960,"hspeed": -1,"hview": 480,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1600,"wview": 800,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "364f079e-3e07-4953-8cba-3566c8877b59","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "48e5282b-fc89-4bb2-8b7d-2e8199334515","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "66936a81-2205-467d-8afa-f9d326a9d62f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "c312134f-9c26-4464-bce0-fe3fb296f9c6","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a625a848-05a8-4e42-b9e4-a3ac7e5e2549","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "aa95d8ad-d3c7-41e8-b292-e5e883e7f633","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "88b54074-6e9f-4804-a05e-5c308fcc4f58","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "31728974-c09b-4645-8a93-bbf8d1855930",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}