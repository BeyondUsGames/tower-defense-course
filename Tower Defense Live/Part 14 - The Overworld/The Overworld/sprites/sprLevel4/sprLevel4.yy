{
    "id": "8e4d4ec8-0fcc-4b33-93f4-80788686d141",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "444f9be9-8e08-4938-89c8-ebf708759d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e4d4ec8-0fcc-4b33-93f4-80788686d141",
            "compositeImage": {
                "id": "6be22f81-962f-47ed-b28d-f34209b67bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444f9be9-8e08-4938-89c8-ebf708759d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90df6645-e06e-4d92-8d93-ad3c4193384e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444f9be9-8e08-4938-89c8-ebf708759d19",
                    "LayerId": "d70cdb76-4bf2-4198-969e-6f61d8e5ddaa"
                }
            ]
        },
        {
            "id": "bf01545d-172a-4260-aa17-41722ca60fe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e4d4ec8-0fcc-4b33-93f4-80788686d141",
            "compositeImage": {
                "id": "bf144456-48d3-451d-a17c-e1f93e1c13f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf01545d-172a-4260-aa17-41722ca60fe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6486b2d7-886c-4cc8-b5b7-541d166123e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf01545d-172a-4260-aa17-41722ca60fe2",
                    "LayerId": "d70cdb76-4bf2-4198-969e-6f61d8e5ddaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d70cdb76-4bf2-4198-969e-6f61d8e5ddaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e4d4ec8-0fcc-4b33-93f4-80788686d141",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}