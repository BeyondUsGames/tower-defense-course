{
    "id": "5748ec39-66dd-40b4-ac50-e560474a20aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFlyingSkeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 379,
    "bbox_left": 0,
    "bbox_right": 454,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "062b6327-0607-442e-8bef-ad3fb643edf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "ddf81bd6-fc9e-424f-b451-b0ae30d8cd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062b6327-0607-442e-8bef-ad3fb643edf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de9744a-d4e8-47c5-8092-daa6692d0461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062b6327-0607-442e-8bef-ad3fb643edf9",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "b645f159-2a1f-4e7d-a09d-5a6d12bc05c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "c35d2cce-f7ad-426a-8a9b-fb0b344669f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b645f159-2a1f-4e7d-a09d-5a6d12bc05c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "326a795a-4201-4eaa-a69f-fad2cf540b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b645f159-2a1f-4e7d-a09d-5a6d12bc05c7",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "a7389ce6-c8e4-4eb5-928b-b594897c9823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "cdc260cb-4ba5-4d3c-83c3-db8e98b2c179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7389ce6-c8e4-4eb5-928b-b594897c9823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f481211-85cd-49f2-be05-305245a8bc32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7389ce6-c8e4-4eb5-928b-b594897c9823",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "3b0e6cac-72f3-4710-81e0-23260c6f5b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "33b14680-1dd1-47e0-8a23-3a718718a5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b0e6cac-72f3-4710-81e0-23260c6f5b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83098113-50f7-4bf9-84ef-19cdc5921f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b0e6cac-72f3-4710-81e0-23260c6f5b0e",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "ce4c03de-0043-4d61-a2cd-ebb5ef397ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "fa1af59f-0ecf-4a83-84b9-9900546fd378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4c03de-0043-4d61-a2cd-ebb5ef397ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815f11b2-f97e-4145-8305-80a91b0e3bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4c03de-0043-4d61-a2cd-ebb5ef397ff7",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "25645c48-ca64-4278-9ba3-a08fad40ed7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "b56e7909-4d6d-471a-b7d1-c3230ad9a5a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25645c48-ca64-4278-9ba3-a08fad40ed7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52221836-2bb5-41ac-b22a-cb54763837ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25645c48-ca64-4278-9ba3-a08fad40ed7a",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "5a9b204b-d7e9-4d49-ab02-e08ec188a503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "f111b47c-3ee0-4cd4-b20c-0ddc0c0e1a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a9b204b-d7e9-4d49-ab02-e08ec188a503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e7576a8-36d7-43fb-a481-94f10bda3081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a9b204b-d7e9-4d49-ab02-e08ec188a503",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "2e76c529-e8cc-49f6-a7dd-32b7b742d7ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "97fd520b-07de-40dd-8e97-d371410df35a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e76c529-e8cc-49f6-a7dd-32b7b742d7ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b60a4bad-cbc0-44ba-8c17-6d21c36bca81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e76c529-e8cc-49f6-a7dd-32b7b742d7ce",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "6d3673b3-bb50-40c9-87dd-649dad533723",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "f888cb3b-cf8d-4ba6-a9e8-bfe25a895fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d3673b3-bb50-40c9-87dd-649dad533723",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceffaec0-b4e0-4d48-a10a-cff13f85739d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d3673b3-bb50-40c9-87dd-649dad533723",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        },
        {
            "id": "58f3cb86-937b-4449-ae4b-e7d7dadba73f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "compositeImage": {
                "id": "928cb06d-7981-428e-88b4-716ebb1c4281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f3cb86-937b-4449-ae4b-e7d7dadba73f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5ed7c6-a554-4885-8bdc-46e78abebe1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f3cb86-937b-4449-ae4b-e7d7dadba73f",
                    "LayerId": "174a3b89-ee7d-48e7-b63d-89d9a850af94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 380,
    "layers": [
        {
            "id": "174a3b89-ee7d-48e7-b63d-89d9a850af94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5748ec39-66dd-40b4-ac50-e560474a20aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 455,
    "xorig": 227,
    "yorig": 190
}