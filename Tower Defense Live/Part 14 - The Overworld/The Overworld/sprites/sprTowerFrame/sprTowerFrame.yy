{
    "id": "b9af13c1-a29b-45b9-8476-dad3a49451c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTowerFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 162,
    "bbox_left": 0,
    "bbox_right": 713,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fab9ef70-3945-4429-8236-bd525f612c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9af13c1-a29b-45b9-8476-dad3a49451c5",
            "compositeImage": {
                "id": "fee66a8c-9c3f-46a8-beb1-18bb3841401f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab9ef70-3945-4429-8236-bd525f612c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "023bddc9-e6a4-4269-9d6d-eac7d3964324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab9ef70-3945-4429-8236-bd525f612c84",
                    "LayerId": "62d43b62-e840-45e6-b18d-e0b56814c7ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 163,
    "layers": [
        {
            "id": "62d43b62-e840-45e6-b18d-e0b56814c7ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9af13c1-a29b-45b9-8476-dad3a49451c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 714,
    "xorig": 0,
    "yorig": 0
}