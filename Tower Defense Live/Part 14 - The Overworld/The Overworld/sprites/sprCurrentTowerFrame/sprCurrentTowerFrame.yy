{
    "id": "622cb491-7e03-4a11-94bc-1a80babcfe91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCurrentTowerFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecd0e8ac-716f-439f-a1bd-a536a162bc75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622cb491-7e03-4a11-94bc-1a80babcfe91",
            "compositeImage": {
                "id": "e0b90541-7d91-4d9b-a290-26183f9a8072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd0e8ac-716f-439f-a1bd-a536a162bc75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ebee7e-6b07-4337-bbb6-a7d1f0d3dcda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd0e8ac-716f-439f-a1bd-a536a162bc75",
                    "LayerId": "65f1d83b-8cc6-4f5d-86a5-b1a854e19d41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "65f1d83b-8cc6-4f5d-86a5-b1a854e19d41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622cb491-7e03-4a11-94bc-1a80babcfe91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}