{
    "id": "b6e9ac36-99cd-415d-86b8-2ad3cfba2fc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDarkSandBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "800f9b51-adb4-4da6-af2c-4b947e37d015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e9ac36-99cd-415d-86b8-2ad3cfba2fc5",
            "compositeImage": {
                "id": "df236228-c92d-4b25-a6de-ad23a21f5e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "800f9b51-adb4-4da6-af2c-4b947e37d015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "039d9cdd-90a7-47eb-8236-bf5d5598ec01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "800f9b51-adb4-4da6-af2c-4b947e37d015",
                    "LayerId": "42687639-7921-40fb-af91-ac95b7676ef3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "42687639-7921-40fb-af91-ac95b7676ef3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6e9ac36-99cd-415d-86b8-2ad3cfba2fc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}