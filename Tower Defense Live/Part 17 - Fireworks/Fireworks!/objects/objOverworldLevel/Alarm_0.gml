/// @description Find Level Objects
for(var i = 0; i < instance_number(objOverworldLevel); ++i) {
	if(instance_find(objOverworldLevel, i).myLevelNumber == myLevelNumber - 1)
		prevLevelObject = instance_find(objOverworldLevel, i);
	if(instance_find(objOverworldLevel, i).myLevelNumber == myLevelNumber + 1)
		nextLevelObject = instance_find(objOverworldLevel, i);
}

if(prevLevelObject == noone)
	prevLevelObject = id;