/// @description Check for songs
if(!audio_is_playing(backgroundMusic[currentSong])) {
	var temp = currentSong;
	while(temp == currentSong)
		currentSong = irandom(2);
	audio_play_sound(backgroundMusic[currentSong], 100, false);
}