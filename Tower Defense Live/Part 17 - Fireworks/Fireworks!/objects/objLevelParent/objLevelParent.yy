{
    "id": "1dd9fabc-202d-44ad-a580-3ff860255a83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLevelParent",
    "eventList": [
        {
            "id": "6d13a7e5-7f7c-45c3-ac27-bae9db1aaf83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1dd9fabc-202d-44ad-a580-3ff860255a83"
        },
        {
            "id": "330700e5-7218-4dfe-81ee-6f08f2706033",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1dd9fabc-202d-44ad-a580-3ff860255a83"
        },
        {
            "id": "07c63cf8-b1c0-48b7-94a4-42052a17cf99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1dd9fabc-202d-44ad-a580-3ff860255a83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}