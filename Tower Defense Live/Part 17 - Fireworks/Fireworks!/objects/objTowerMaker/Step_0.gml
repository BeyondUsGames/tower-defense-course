/// @description Follow Mouse
x = mouse_x;
y = mouse_y;

myGridSpotX = round(mouse_x / gridSize);
myGridSpotY = round(mouse_y / gridSize);

if(mouse_check_button_released(mb_left)) {
	sprite_index = sprCursor;
	image_blend = c_white;
	//Align tower to grid
	if(myGridSpotX mod 2 == 0) {
		++myGridSpotX;
		x = myGridSpotX * gridSize;
	}
	if(myGridSpotY mod 2 == 0) {
		++myGridSpotY;
		y = myGridSpotY * gridSize;
	}
	if(GoodToBuild()) {
		objPlayer.gold -= global.AvailableTowers[currentTower, TowerCost];
		instance_create_layer(myGridSpotX * gridSize, myGridSpotY * gridSize, "Instances", global.AvailableTowers[currentTower, TowerObject]);
	}
}

//Choose Available Towers
if(array_height_2d(global.AvailableTowers) > 0 && keyboard_check_pressed(ord("1")))
	currentTower = 0;
if(array_height_2d(global.AvailableTowers) > 1 && keyboard_check_pressed(ord("2")))
	currentTower = 1;
if(array_height_2d(global.AvailableTowers) > 2 && keyboard_check_pressed(ord("3")))
	currentTower = 2;