{
    "id": "6d19c5e8-08c4-45b0-aa3d-a01dfed9bfe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c75aca6-dd4c-4094-92fe-6c68f0318cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d19c5e8-08c4-45b0-aa3d-a01dfed9bfe7",
            "compositeImage": {
                "id": "9e69014e-ba46-4397-9fef-3340ca71451c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c75aca6-dd4c-4094-92fe-6c68f0318cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186e6a2d-da8f-4243-832b-9707be212150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c75aca6-dd4c-4094-92fe-6c68f0318cf1",
                    "LayerId": "a715c2b5-9a4e-4184-b3e5-e5efb628a0a0"
                }
            ]
        },
        {
            "id": "e62f8170-556b-4a1e-b6b8-3a8750bc4781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d19c5e8-08c4-45b0-aa3d-a01dfed9bfe7",
            "compositeImage": {
                "id": "c9547056-968d-4a38-9f4a-cb5f1b2191dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62f8170-556b-4a1e-b6b8-3a8750bc4781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c451e8-4f1c-4039-bb1b-d0ea192ffef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62f8170-556b-4a1e-b6b8-3a8750bc4781",
                    "LayerId": "a715c2b5-9a4e-4184-b3e5-e5efb628a0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a715c2b5-9a4e-4184-b3e5-e5efb628a0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d19c5e8-08c4-45b0-aa3d-a01dfed9bfe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}