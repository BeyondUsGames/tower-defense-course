{
    "id": "210e13f3-29c9-4e5a-92cd-eb44ff964bc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSlowDownButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b760b68f-3d1f-4543-a9ef-c534e06afe0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "210e13f3-29c9-4e5a-92cd-eb44ff964bc6",
            "compositeImage": {
                "id": "7fd1fe58-e91a-45e4-998a-1afe5ebf530a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b760b68f-3d1f-4543-a9ef-c534e06afe0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "798df769-098f-4268-994f-8f63f63ae0b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b760b68f-3d1f-4543-a9ef-c534e06afe0c",
                    "LayerId": "29836fd4-f578-4899-9d64-2cc36b58d1af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "29836fd4-f578-4899-9d64-2cc36b58d1af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "210e13f3-29c9-4e5a-92cd-eb44ff964bc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}