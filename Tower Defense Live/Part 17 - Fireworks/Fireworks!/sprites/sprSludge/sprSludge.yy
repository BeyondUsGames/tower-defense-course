{
    "id": "c2d4e493-0261-4d83-aca2-f42f4ff4daee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSludge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a08731e1-2d7b-4a77-820d-cc3e6bd45a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2d4e493-0261-4d83-aca2-f42f4ff4daee",
            "compositeImage": {
                "id": "815ecd2e-5831-4e02-a973-c5cba06760d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a08731e1-2d7b-4a77-820d-cc3e6bd45a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12eb056a-e7c4-43a7-8edf-9560dc4dc858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a08731e1-2d7b-4a77-820d-cc3e6bd45a18",
                    "LayerId": "81eeb107-5bb8-4ad4-8410-84d56a4cddee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "81eeb107-5bb8-4ad4-8410-84d56a4cddee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2d4e493-0261-4d83-aca2-f42f4ff4daee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 23
}