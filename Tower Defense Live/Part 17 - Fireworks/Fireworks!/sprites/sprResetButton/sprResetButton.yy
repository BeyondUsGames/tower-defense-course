{
    "id": "6f4ddeed-e753-4d76-8018-a40fd11303a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprResetButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 227,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "955c6e78-e19a-43c0-8fc6-36eef7426e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f4ddeed-e753-4d76-8018-a40fd11303a1",
            "compositeImage": {
                "id": "4c4e5167-e7c0-42db-acb2-b7931556cab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955c6e78-e19a-43c0-8fc6-36eef7426e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb4c0ee2-8444-471f-a069-467f3a1c280b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955c6e78-e19a-43c0-8fc6-36eef7426e56",
                    "LayerId": "2a63b954-cf9e-47ce-912d-622851e82ca7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "2a63b954-cf9e-47ce-912d-622851e82ca7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f4ddeed-e753-4d76-8018-a40fd11303a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 228,
    "xorig": 114,
    "yorig": 17
}