{
    "id": "7a638f06-e024-4d8e-bb03-d5a2ebca860f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDefaultTowerFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "580278d4-f8f1-4335-8731-48b812cc68e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a638f06-e024-4d8e-bb03-d5a2ebca860f",
            "compositeImage": {
                "id": "b67d77cb-ee8d-44e6-b99e-1a758462e29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "580278d4-f8f1-4335-8731-48b812cc68e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f495e389-64e5-44a5-9a7e-cec22650258b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "580278d4-f8f1-4335-8731-48b812cc68e9",
                    "LayerId": "002eb11e-7136-4d01-9a7e-d94ebd61c560"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "002eb11e-7136-4d01-9a7e-d94ebd61c560",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a638f06-e024-4d8e-bb03-d5a2ebca860f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}