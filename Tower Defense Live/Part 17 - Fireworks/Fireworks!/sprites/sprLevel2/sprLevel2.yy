{
    "id": "0b95f31d-fa28-4424-b735-ea19f2cedd05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "396c72e0-92f1-4f85-82fa-9d386da42684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b95f31d-fa28-4424-b735-ea19f2cedd05",
            "compositeImage": {
                "id": "44e12b10-3659-4973-bb1f-81a93fc26bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "396c72e0-92f1-4f85-82fa-9d386da42684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f00b98-bf92-49f1-b5b4-525666076798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396c72e0-92f1-4f85-82fa-9d386da42684",
                    "LayerId": "05a969ec-de43-4e97-a804-f1f2f0f40788"
                }
            ]
        },
        {
            "id": "b6cbc0d1-f960-46ab-b358-752be75fc813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b95f31d-fa28-4424-b735-ea19f2cedd05",
            "compositeImage": {
                "id": "c24d266c-bcef-4433-aa88-07cf1b91faef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6cbc0d1-f960-46ab-b358-752be75fc813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c7fa68-47ba-47da-914d-9c52184703b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6cbc0d1-f960-46ab-b358-752be75fc813",
                    "LayerId": "05a969ec-de43-4e97-a804-f1f2f0f40788"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "05a969ec-de43-4e97-a804-f1f2f0f40788",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b95f31d-fa28-4424-b735-ea19f2cedd05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}