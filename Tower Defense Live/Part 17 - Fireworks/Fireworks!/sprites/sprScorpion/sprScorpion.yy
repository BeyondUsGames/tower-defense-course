{
    "id": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprScorpion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 25,
    "bbox_right": 185,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3808e83-60f3-486e-a692-e663035d4ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "a30aa5e4-c720-45e3-981f-102b8681a410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3808e83-60f3-486e-a692-e663035d4ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e2ad658-5f97-41e6-a06f-67150b1caad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3808e83-60f3-486e-a692-e663035d4ec3",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "a094e42f-4c59-4cab-9bd7-cae00c514a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "3025d4a3-4ff6-4a24-b5ed-8ec344efd003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a094e42f-4c59-4cab-9bd7-cae00c514a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da4e41d4-034e-472e-ba99-bb69c025d98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a094e42f-4c59-4cab-9bd7-cae00c514a6d",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "b341d00c-cbd3-40f8-b3e7-84822cafc109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "223b1753-a655-4138-a0df-a1cb061ba0c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b341d00c-cbd3-40f8-b3e7-84822cafc109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49756912-5ff4-4cda-b54d-b5c913c41162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b341d00c-cbd3-40f8-b3e7-84822cafc109",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "5a9bc97d-e569-495d-ae66-24a0bbb19fa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "8be32873-4e77-4a03-a64c-f1c1fd1cb97d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a9bc97d-e569-495d-ae66-24a0bbb19fa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05396452-20fc-4fd6-8a8f-d2aecb8668fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a9bc97d-e569-495d-ae66-24a0bbb19fa9",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "98d76b53-a04f-414f-98d3-a2b0268ac641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "b6e06379-ff53-4115-b20f-3f0de3c1b24d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d76b53-a04f-414f-98d3-a2b0268ac641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c77093-69b6-4425-a66b-e92b02e087cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d76b53-a04f-414f-98d3-a2b0268ac641",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "1c090592-768a-4451-88cb-2753cf6c21cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "91ddfd55-1c33-416e-bee6-86e74d63fbe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c090592-768a-4451-88cb-2753cf6c21cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888ea3ba-996b-4d49-b815-ba4a957104cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c090592-768a-4451-88cb-2753cf6c21cb",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "b63929ef-8232-4962-b4a2-8e9231b17b02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "008c76ee-f0b3-48fc-8351-03e09025687e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63929ef-8232-4962-b4a2-8e9231b17b02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9809a784-130f-45a5-8595-1aa453bcf44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63929ef-8232-4962-b4a2-8e9231b17b02",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "8cafb673-20bb-492a-a35b-81b6c2c163ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "fc7e9b01-5238-4986-988b-c32dc759de94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cafb673-20bb-492a-a35b-81b6c2c163ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb22a9a4-5756-4b95-806a-b5304da71e7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cafb673-20bb-492a-a35b-81b6c2c163ad",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "e92df22e-99c9-4fee-9b2b-61acbfd7c3e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "536d0fb4-10e8-4184-b8e4-7463bd6e398e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e92df22e-99c9-4fee-9b2b-61acbfd7c3e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "245a1950-cbeb-4c35-ace8-fc87e0abe56d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e92df22e-99c9-4fee-9b2b-61acbfd7c3e8",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "5301715d-27b0-4fde-8fa4-cc6cbd801f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "abd4cd55-2e0b-4e46-a699-0cf89a567f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5301715d-27b0-4fde-8fa4-cc6cbd801f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d40325e9-ffb1-4547-a650-4c5f319afd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5301715d-27b0-4fde-8fa4-cc6cbd801f20",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "765c2844-fe98-41db-9264-393d8d75a0a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "12c79e57-ce03-4d78-a313-5ca2118df646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "765c2844-fe98-41db-9264-393d8d75a0a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5967293-25ca-4916-bf42-66e5bb168453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "765c2844-fe98-41db-9264-393d8d75a0a5",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "e31283df-97e2-4965-862a-3079d394e96c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "1fa21bbe-5f04-4bde-9d1c-19178b15b7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e31283df-97e2-4965-862a-3079d394e96c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96f2770-2636-4659-a401-8bd58cc46778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e31283df-97e2-4965-862a-3079d394e96c",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "8b4f661e-aae7-4a9a-96f9-433cb291e37b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "0611ad4f-7065-4578-83e6-cc81a008f27d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b4f661e-aae7-4a9a-96f9-433cb291e37b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7771f3-346b-403c-81b6-fc624c7e1ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b4f661e-aae7-4a9a-96f9-433cb291e37b",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "f23a456c-9a6f-464e-8bcf-3b9ceaae29eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "b2f56361-06ca-4cb0-a72d-dc8c8a6d3921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f23a456c-9a6f-464e-8bcf-3b9ceaae29eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71219cd4-a37c-427c-8a9e-d73f9ff4d0d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f23a456c-9a6f-464e-8bcf-3b9ceaae29eb",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "ffbc69ac-596d-4c5b-9c35-c6c1f79de3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "51000c46-a7ac-4583-8f76-dad1a162dad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffbc69ac-596d-4c5b-9c35-c6c1f79de3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c5b6d0-0019-4e04-a815-30dd19c9b602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffbc69ac-596d-4c5b-9c35-c6c1f79de3e3",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "69ea367d-3b9a-4af7-9beb-d53e78409e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "b468be11-e5f9-4247-a5ed-35a0459f4fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ea367d-3b9a-4af7-9beb-d53e78409e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ddec738-32f1-4efc-b97b-6a9c23bfa2fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ea367d-3b9a-4af7-9beb-d53e78409e1f",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "e47c65f3-de18-4c76-9c8d-e426547a3084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "c1269c2a-388c-45ae-8b75-ae1b795791c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e47c65f3-de18-4c76-9c8d-e426547a3084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61470a7b-1684-4b64-b770-f2170e84b892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e47c65f3-de18-4c76-9c8d-e426547a3084",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "22169108-a734-4c1d-ac04-c4d4d0215671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "0d1402f8-c1ad-4e89-97f0-9770bbb9259c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22169108-a734-4c1d-ac04-c4d4d0215671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c24fd2c-17fa-42e3-aef6-615e91b9a738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22169108-a734-4c1d-ac04-c4d4d0215671",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        },
        {
            "id": "8e7f7774-2d9a-41ee-9eca-c1665276d5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "compositeImage": {
                "id": "dcf3b1b0-d0b2-4b83-9ddc-8500683637bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e7f7774-2d9a-41ee-9eca-c1665276d5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0b08db-af98-481b-b5b2-71cc49fcfc3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e7f7774-2d9a-41ee-9eca-c1665276d5ec",
                    "LayerId": "3986cf21-6689-4cd1-a9d6-0f455350a85d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 226,
    "layers": [
        {
            "id": "3986cf21-6689-4cd1-a9d6-0f455350a85d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b507a21-53d5-4e0f-a879-9596ebf7ceb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 50,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 113
}