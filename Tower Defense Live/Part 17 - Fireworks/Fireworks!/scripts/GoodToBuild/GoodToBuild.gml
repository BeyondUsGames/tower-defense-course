///Check to see if a tower can be built

//Tile collisions
var rightCollision, topCollision, leftCollision, bottomCollision;
rightCollision = tilemap_get_at_pixel(enemyPath, bbox_right, y);
topCollision = tilemap_get_at_pixel(enemyPath, x, bbox_top);
leftCollision = tilemap_get_at_pixel(enemyPath, bbox_left, y);
bottomCollision = tilemap_get_at_pixel(enemyPath, x, bbox_bottom);

if(max(rightCollision, topCollision, leftCollision, bottomCollision) > 0)
	return false;

if(y > 740)
	return false;

if(collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objTowerParent, false, true) != -4)
	return false;
	
//When we don't have enough gold -- Check gold of player
if(objPlayer.gold < global.AvailableTowers[currentTower, TowerCost])
	return false;

return true;