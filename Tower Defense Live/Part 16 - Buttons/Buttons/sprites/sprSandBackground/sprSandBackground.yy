{
    "id": "17f3f40b-4071-4402-ad61-5b32b2e44df2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSandBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04fad7f1-bc58-415f-b853-9fab2853dc7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f3f40b-4071-4402-ad61-5b32b2e44df2",
            "compositeImage": {
                "id": "edeeca64-94d6-432c-965f-5ee0694b3f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04fad7f1-bc58-415f-b853-9fab2853dc7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "974d4fba-a34d-4914-9953-f0ade87389a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04fad7f1-bc58-415f-b853-9fab2853dc7c",
                    "LayerId": "dac85ac7-3067-4669-84f0-0881cd32cf49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "dac85ac7-3067-4669-84f0-0881cd32cf49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17f3f40b-4071-4402-ad61-5b32b2e44df2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}