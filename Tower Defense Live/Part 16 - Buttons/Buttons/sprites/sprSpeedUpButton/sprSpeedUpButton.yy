{
    "id": "56b0dcfe-cc2b-4794-88da-82f0378017b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpeedUpButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9a70421-73c2-495d-abb2-78fca81b94ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56b0dcfe-cc2b-4794-88da-82f0378017b2",
            "compositeImage": {
                "id": "cd07d45f-7327-46e7-aacc-61c53f6a823b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9a70421-73c2-495d-abb2-78fca81b94ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "480570d9-1a03-4429-84a1-6603cb1d4798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9a70421-73c2-495d-abb2-78fca81b94ac",
                    "LayerId": "9120e366-68ca-4deb-822a-7254361e7791"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "9120e366-68ca-4deb-822a-7254361e7791",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56b0dcfe-cc2b-4794-88da-82f0378017b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}