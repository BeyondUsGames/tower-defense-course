{
    "id": "d719b9db-5a47-46d0-b8da-07bf4ffa230e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSlowDownButton",
    "eventList": [
        {
            "id": "d0851b16-2adb-4ce5-94a2-6c21dc02d0b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d719b9db-5a47-46d0-b8da-07bf4ffa230e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "210e13f3-29c9-4e5a-92cd-eb44ff964bc6",
    "visible": true
}