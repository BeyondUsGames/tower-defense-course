/// @description Draw Stars
draw_self();

if(starRating >= 1)
	draw_sprite(sprStar, 1, x - 32, y + 48);
if(starRating >= 2)
	draw_sprite(sprStar, 1, x, y + 64);
if(starRating >= 3)
	draw_sprite(sprStar, 1, x + 32, y + 48);

draw_sprite(sprStar, 0, x - 32, y + 48);
draw_sprite(sprStar, 0, x, y + 64);
draw_sprite(sprStar, 0, x + 32, y + 48);