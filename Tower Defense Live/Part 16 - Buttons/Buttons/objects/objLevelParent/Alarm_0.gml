/// @description Spawn an enemy
if(currentEnemies < maxEnemies) {
	if(enemies[arraySpot, 1] <= 0)
	arraySpot = clamp(++arraySpot, 0, array_height_2d(enemies) - 1);
	var enemy = instance_create_layer(x, y, layer, enemies[arraySpot, 0]);
	with(enemy) {
		path_start(other.myPath, enemy.mySpeed, path_action_stop, true);
	}
	++currentEnemies;
	enemies[arraySpot, 1] -= 1;
	spawnRate -= decreaser;
	if(spawnRate < 1)
		spawnRate = 1;
	alarm[0] = spawnRate;
}
else if(currentEnemies == maxEnemies && !instance_exists(objEnemyParent)) {
	//Determine Star Rating
	if(objPlayer.currentHealth == objPlayer.maxHealth)
		starRating = 3;
	else if(objPlayer.currentHealth >= objPlayer.maxHealth / 2)
		starRating = 2;
	else
		starRating = 1;
		
	if(LoadStarRating() > starRating)
		starRating = LoadStarRating();
	//Save That Rating
	SaveLevel();
	
	game_set_speed(60, gamespeed_fps);
	instance_create_layer(room_width / 2, room_height / 2, layer, objContinueButton);
}
else
	alarm[0] = 1;