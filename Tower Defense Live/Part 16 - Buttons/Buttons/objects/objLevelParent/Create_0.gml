/// @description Initialize All Levels
//Create tower spawner
instance_create_layer(x, y, layer, objTowerMaker);

//Create Buttons
instance_create_layer(room_width / 2 + 96, room_height - 128, layer, objPauseButton);
instance_create_layer(room_width / 2 + 128, room_height - 64, layer, objSpeedUpButton);
instance_create_layer(room_width / 2 + 64, room_height - 64, layer, objSlowDownButton);

currentEnemies = 0;
maxEnemies = 0;
arraySpot = 0;
killCount = 0;