{
    "id": "668b3323-0197-4cb4-ba85-49cb6151cca0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPauseButton",
    "eventList": [
        {
            "id": "529bc6b4-8140-4929-9bb8-2760f9989f93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "668b3323-0197-4cb4-ba85-49cb6151cca0"
        },
        {
            "id": "551a408f-dfc5-4898-a1d0-e1b16cd3cc8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "668b3323-0197-4cb4-ba85-49cb6151cca0"
        },
        {
            "id": "a4330593-eeef-433c-9148-718ae869478b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "668b3323-0197-4cb4-ba85-49cb6151cca0"
        },
        {
            "id": "30eb330f-8c79-4aa1-9e3b-2c4a69f97940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "668b3323-0197-4cb4-ba85-49cb6151cca0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0537d472-80d8-41f0-b472-ec65589e88a7",
    "visible": true
}