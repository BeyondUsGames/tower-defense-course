{
    "id": "eb4c49ab-bbee-4411-a18b-674dfdbe8a85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCatOW",
    "eventList": [
        {
            "id": "91e59e4b-5090-4b0b-953f-a022022527de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb4c49ab-bbee-4411-a18b-674dfdbe8a85"
        },
        {
            "id": "62438ec8-20a2-451e-b7b5-70753e740e55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "eb4c49ab-bbee-4411-a18b-674dfdbe8a85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef7ecab5-8906-4937-b1db-60a601181dbd",
    "visible": true
}