{
    "id": "6469baf6-eb87-43e4-9626-6244396f76b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSystem",
    "eventList": [
        {
            "id": "eaeb0e51-e55a-4a6d-a11f-4bf47832ad13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6469baf6-eb87-43e4-9626-6244396f76b0"
        },
        {
            "id": "3e9b8156-e93a-4296-be8f-37cf31ab1369",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6469baf6-eb87-43e4-9626-6244396f76b0"
        },
        {
            "id": "056a232a-5812-457a-853f-4c60dcea588c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "6469baf6-eb87-43e4-9626-6244396f76b0"
        },
        {
            "id": "94050535-762a-4f26-833d-4a0c3be6bdbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6469baf6-eb87-43e4-9626-6244396f76b0"
        },
        {
            "id": "0311bfac-91cf-46c0-84b6-c031c48cabf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 123,
            "eventtype": 9,
            "m_owner": "6469baf6-eb87-43e4-9626-6244396f76b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}