{
    "id": "657e8d2e-c2d6-4a45-b5d6-0771103ef212",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objAirEnemyParent",
    "eventList": [
        {
            "id": "b2496fd1-33eb-4a4f-96c0-918c202284da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "657e8d2e-c2d6-4a45-b5d6-0771103ef212"
        },
        {
            "id": "7b0a207c-09dd-4d3d-815c-f9bf9a3d77e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "657e8d2e-c2d6-4a45-b5d6-0771103ef212"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e890d949-2c7f-4c73-90e5-6267e9ddf00a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}