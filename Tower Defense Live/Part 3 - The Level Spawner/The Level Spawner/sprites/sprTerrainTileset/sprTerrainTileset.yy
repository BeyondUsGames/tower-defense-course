{
    "id": "aab4c92b-cbcc-41b4-863e-5b88b41da652",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTerrainTileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 735,
    "bbox_left": 0,
    "bbox_right": 671,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70341311-0be4-4fbc-8c6a-05067d60c60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aab4c92b-cbcc-41b4-863e-5b88b41da652",
            "compositeImage": {
                "id": "471b4d46-329b-4440-ace0-331685531c64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70341311-0be4-4fbc-8c6a-05067d60c60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2f3562-ba42-4269-bf2f-14ec03a5a6dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70341311-0be4-4fbc-8c6a-05067d60c60d",
                    "LayerId": "134f9782-b88a-4cc5-b20c-5dd048a9de38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 736,
    "layers": [
        {
            "id": "134f9782-b88a-4cc5-b20c-5dd048a9de38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aab4c92b-cbcc-41b4-863e-5b88b41da652",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 672,
    "xorig": 336,
    "yorig": 368
}