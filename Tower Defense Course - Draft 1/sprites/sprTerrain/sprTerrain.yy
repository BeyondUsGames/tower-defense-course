{
    "id": "3f6e0986-56a6-4378-9f7f-eb0d4939379e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTerrain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 735,
    "bbox_left": 0,
    "bbox_right": 671,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e19d255-fff4-46d7-945b-808766548d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6e0986-56a6-4378-9f7f-eb0d4939379e",
            "compositeImage": {
                "id": "125ff88e-8ef7-4d0b-ae48-3d87a97d453d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e19d255-fff4-46d7-945b-808766548d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92cd9d59-800b-49bf-b311-f7919de023db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e19d255-fff4-46d7-945b-808766548d74",
                    "LayerId": "9eb3fa88-a466-4009-ab39-ccdf4bc21643"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 736,
    "layers": [
        {
            "id": "9eb3fa88-a466-4009-ab39-ccdf4bc21643",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f6e0986-56a6-4378-9f7f-eb0d4939379e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 672,
    "xorig": 336,
    "yorig": 368
}