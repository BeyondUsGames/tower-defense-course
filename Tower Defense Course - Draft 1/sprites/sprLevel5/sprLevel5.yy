{
    "id": "f88eb210-8373-4ca2-9a15-2308fe902098",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "032ff29c-2fae-4ac9-80ef-9d2b498d8e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88eb210-8373-4ca2-9a15-2308fe902098",
            "compositeImage": {
                "id": "050e6d9b-d3b9-4ea8-b5a7-a9d5d03470db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "032ff29c-2fae-4ac9-80ef-9d2b498d8e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "614d82fc-6bc4-49f2-8cb2-102892f165e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "032ff29c-2fae-4ac9-80ef-9d2b498d8e5e",
                    "LayerId": "91e4695f-6c29-4df0-b05d-6c4f38044bcb"
                }
            ]
        },
        {
            "id": "40c6e35a-e44e-442a-973d-1452c9865d6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88eb210-8373-4ca2-9a15-2308fe902098",
            "compositeImage": {
                "id": "061d6f13-541c-4d53-8be8-0018b63f89a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c6e35a-e44e-442a-973d-1452c9865d6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7328704-5005-4939-affa-c062df70e427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c6e35a-e44e-442a-973d-1452c9865d6f",
                    "LayerId": "91e4695f-6c29-4df0-b05d-6c4f38044bcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "91e4695f-6c29-4df0-b05d-6c4f38044bcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f88eb210-8373-4ca2-9a15-2308fe902098",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}