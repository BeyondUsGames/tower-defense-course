{
    "id": "c40d60f9-2192-407f-9558-505419735781",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTextBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 635,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec5dddd9-164a-4f74-b4e2-864db8341193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c40d60f9-2192-407f-9558-505419735781",
            "compositeImage": {
                "id": "9385d982-bd5c-439e-b6b3-0cd066072127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5dddd9-164a-4f74-b4e2-864db8341193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1da13d-8eab-4272-a707-c571b5d2d8c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5dddd9-164a-4f74-b4e2-864db8341193",
                    "LayerId": "bd9ad94d-60dd-4858-bc86-36679a2700a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "bd9ad94d-60dd-4858-bc86-36679a2700a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c40d60f9-2192-407f-9558-505419735781",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 636,
    "xorig": 318,
    "yorig": 78
}