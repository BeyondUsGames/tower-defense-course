{
    "id": "460be54f-6b90-42f1-987f-23aec7b898eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSandBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6169354b-66ca-48e4-bc22-048257bffbb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "460be54f-6b90-42f1-987f-23aec7b898eb",
            "compositeImage": {
                "id": "5a161f7d-7f12-4edb-a432-6aa42fa982a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6169354b-66ca-48e4-bc22-048257bffbb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3e24f7-b684-4e35-aebb-b94ac3e1bc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6169354b-66ca-48e4-bc22-048257bffbb2",
                    "LayerId": "ca8c8f02-5cf6-4851-87a8-4d28cfce4337"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "ca8c8f02-5cf6-4851-87a8-4d28cfce4337",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "460be54f-6b90-42f1-987f-23aec7b898eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}