{
    "id": "23b25ae3-ed93-4f7f-8888-380711100c71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1467a6af-73ff-433b-868b-308fad6f8a30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23b25ae3-ed93-4f7f-8888-380711100c71",
            "compositeImage": {
                "id": "1df05eb1-50a0-4803-9af9-63fc1baaa215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1467a6af-73ff-433b-868b-308fad6f8a30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e622d0b6-939e-490a-949a-1adc6d418361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1467a6af-73ff-433b-868b-308fad6f8a30",
                    "LayerId": "4043036a-b1f2-4a47-98d0-e612b0a80b43"
                }
            ]
        },
        {
            "id": "5449bb99-d779-4df8-a7c0-2ba740dacb45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23b25ae3-ed93-4f7f-8888-380711100c71",
            "compositeImage": {
                "id": "760f6cbd-5473-4ca2-aa82-e2814623fdb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5449bb99-d779-4df8-a7c0-2ba740dacb45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d86cad-70e1-4d3d-b410-69cedee846e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5449bb99-d779-4df8-a7c0-2ba740dacb45",
                    "LayerId": "4043036a-b1f2-4a47-98d0-e612b0a80b43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4043036a-b1f2-4a47-98d0-e612b0a80b43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23b25ae3-ed93-4f7f-8888-380711100c71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}