{
    "id": "3dac2dd5-37fb-45a8-aeac-e3e64adacf2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4a681a4-9420-4bfb-b420-1bcee72cc2f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dac2dd5-37fb-45a8-aeac-e3e64adacf2d",
            "compositeImage": {
                "id": "d77a4f59-8e24-4e59-8515-dd87ac648045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a681a4-9420-4bfb-b420-1bcee72cc2f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ce1b70-34e2-4e50-bb71-50df83f36bd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a681a4-9420-4bfb-b420-1bcee72cc2f3",
                    "LayerId": "4c1e3c1b-33eb-4a35-a2e5-9a291669591f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "4c1e3c1b-33eb-4a35-a2e5-9a291669591f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dac2dd5-37fb-45a8-aeac-e3e64adacf2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 2
}