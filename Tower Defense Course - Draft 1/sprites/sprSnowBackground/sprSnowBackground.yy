{
    "id": "5e066d93-9856-4dd3-aa90-37fb92077a8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSnowBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bd8b671-0d70-4ce6-ac09-312d44fb221c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e066d93-9856-4dd3-aa90-37fb92077a8a",
            "compositeImage": {
                "id": "9e60dae3-6dcd-4b86-9550-fe3772a2aac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd8b671-0d70-4ce6-ac09-312d44fb221c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29748f79-437e-49a9-8203-b7d795fda57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd8b671-0d70-4ce6-ac09-312d44fb221c",
                    "LayerId": "d000cbe0-58da-4c7d-b0df-03c1372efc9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "d000cbe0-58da-4c7d-b0df-03c1372efc9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e066d93-9856-4dd3-aa90-37fb92077a8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}