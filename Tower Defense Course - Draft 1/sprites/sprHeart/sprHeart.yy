{
    "id": "d36196ee-e827-4b50-992d-45118c9d695b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71fdd328-dfad-489f-8478-350625118be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d36196ee-e827-4b50-992d-45118c9d695b",
            "compositeImage": {
                "id": "70844477-929f-4f9b-ab20-6d8dabc29ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71fdd328-dfad-489f-8478-350625118be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662899f6-6bd1-4b6a-822c-4f821399746b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71fdd328-dfad-489f-8478-350625118be9",
                    "LayerId": "0a2b86c3-b4fd-4721-b79c-641df4d02978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0a2b86c3-b4fd-4721-b79c-641df4d02978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d36196ee-e827-4b50-992d-45118c9d695b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}