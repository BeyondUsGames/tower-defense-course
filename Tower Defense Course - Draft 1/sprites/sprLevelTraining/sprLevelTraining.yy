{
    "id": "6d6c3aa7-c49d-424e-8459-27ea6023bd62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevelTraining",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1327765e-07be-4854-b1e3-3d084ed5009a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6c3aa7-c49d-424e-8459-27ea6023bd62",
            "compositeImage": {
                "id": "75f8ef5a-7d30-45eb-b90c-120ae55e6913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1327765e-07be-4854-b1e3-3d084ed5009a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6df06fa-a822-49ca-99d7-b08ee651d117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1327765e-07be-4854-b1e3-3d084ed5009a",
                    "LayerId": "c49cc5e4-c19f-406e-b7da-24cf911610a4"
                }
            ]
        },
        {
            "id": "c969ba28-7bf9-45b3-aa7f-8d9f172ae0b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6c3aa7-c49d-424e-8459-27ea6023bd62",
            "compositeImage": {
                "id": "24b30f81-72e0-4b2f-9f81-d9c3772316da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c969ba28-7bf9-45b3-aa7f-8d9f172ae0b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2633c44b-b723-4191-98f4-1d5a1afccfbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c969ba28-7bf9-45b3-aa7f-8d9f172ae0b1",
                    "LayerId": "c49cc5e4-c19f-406e-b7da-24cf911610a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c49cc5e4-c19f-406e-b7da-24cf911610a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d6c3aa7-c49d-424e-8459-27ea6023bd62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}