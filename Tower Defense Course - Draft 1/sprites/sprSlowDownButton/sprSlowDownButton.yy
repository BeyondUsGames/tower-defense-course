{
    "id": "f0f35683-d806-4e1d-a433-415e05cd58ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSlowDownButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c624597-8825-4f88-9ed1-ea540d2d3e34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0f35683-d806-4e1d-a433-415e05cd58ff",
            "compositeImage": {
                "id": "8fd64d8f-18c8-4b53-96c5-745a4d5314c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c624597-8825-4f88-9ed1-ea540d2d3e34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56352462-9d12-48bd-a404-4f74c47d9907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c624597-8825-4f88-9ed1-ea540d2d3e34",
                    "LayerId": "8a369004-76e2-4f3f-b038-0e665b89cd51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "8a369004-76e2-4f3f-b038-0e665b89cd51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0f35683-d806-4e1d-a433-415e05cd58ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}