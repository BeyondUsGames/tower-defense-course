{
    "id": "0585b55f-97a7-4420-8300-80a66d44d932",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTurretLevel1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 181,
    "bbox_left": 54,
    "bbox_right": 188,
    "bbox_top": 65,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d762d2c-939e-4c61-ba2e-7238b8c2a27c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0585b55f-97a7-4420-8300-80a66d44d932",
            "compositeImage": {
                "id": "394b8205-5a6d-4c20-b98d-82d639ca7b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d762d2c-939e-4c61-ba2e-7238b8c2a27c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c2cbe6-d115-449b-82ac-4114452333bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d762d2c-939e-4c61-ba2e-7238b8c2a27c",
                    "LayerId": "856bfff9-ac1e-4963-ac5c-a68a6dcbfd60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "856bfff9-ac1e-4963-ac5c-a68a6dcbfd60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0585b55f-97a7-4420-8300-80a66d44d932",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}