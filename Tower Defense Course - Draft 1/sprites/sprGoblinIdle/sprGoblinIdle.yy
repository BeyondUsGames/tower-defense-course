{
    "id": "286e6d8d-f240-49cc-9693-da15a0174b73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 226,
    "bbox_left": 0,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00202377-2d38-4810-98e1-51a535074dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "361c9a7b-1fc8-4474-b262-9fb085c21877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00202377-2d38-4810-98e1-51a535074dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b90cf7b-98ab-47ce-82eb-4f19d291e600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00202377-2d38-4810-98e1-51a535074dbb",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "bd789607-0f9c-48ad-9037-66681e0d3616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "a7c306d2-56c7-47de-951d-497de5b85f29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd789607-0f9c-48ad-9037-66681e0d3616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e388b410-89cc-4bef-9751-39950c05f181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd789607-0f9c-48ad-9037-66681e0d3616",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "0834017a-4fb5-466c-a42e-b8247a73b7eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "cb3615a9-7aec-4db1-88e2-c46c89d95fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0834017a-4fb5-466c-a42e-b8247a73b7eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1379b6a1-43af-44d4-848a-d621b4e622a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0834017a-4fb5-466c-a42e-b8247a73b7eb",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "e373637a-a964-469c-b9ab-c7017d6751b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "b2f1df12-37fe-48f6-8ccb-bd0c951b9856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e373637a-a964-469c-b9ab-c7017d6751b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3711ba-107d-4260-8f0e-acce3d959bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e373637a-a964-469c-b9ab-c7017d6751b4",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "804e9141-9661-4358-b427-66231c7a5e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "0f24eb5d-0df1-4803-a490-a67d7a9d7191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "804e9141-9661-4358-b427-66231c7a5e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd37e9c8-6d47-423e-8c57-1bb5e8b07768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "804e9141-9661-4358-b427-66231c7a5e61",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "126a094f-68f2-4fc0-9afd-48cda9917395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "39f7da36-36e2-4111-9320-14b9f3fac843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "126a094f-68f2-4fc0-9afd-48cda9917395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c5ccc9-3805-403c-9111-ff651d0d4ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "126a094f-68f2-4fc0-9afd-48cda9917395",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "0d6f1eaa-5019-4715-a4d7-4999e7643011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "5894b31a-eacd-4ce1-989c-9b04caabede9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6f1eaa-5019-4715-a4d7-4999e7643011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951c1b6b-74d6-47f0-91c4-9da1ad1bf458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6f1eaa-5019-4715-a4d7-4999e7643011",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "700d05f7-c2f9-44dc-9619-374b884b8b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "2a8ace90-ded5-4557-8692-f44ce0ece656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "700d05f7-c2f9-44dc-9619-374b884b8b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35311bb6-1cfc-4551-a448-ef7a46a43a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "700d05f7-c2f9-44dc-9619-374b884b8b08",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "c722db2a-8a55-49fb-bbe2-b5704ebc9347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "9e1eff26-db28-4716-96d1-c788b77744a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c722db2a-8a55-49fb-bbe2-b5704ebc9347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e71af9e2-6041-4e14-98c6-036eabf62357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c722db2a-8a55-49fb-bbe2-b5704ebc9347",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "0f0c1d75-739c-42d5-9221-274f924e69ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "c086dbd4-2399-432f-8efc-9587b7aeade3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f0c1d75-739c-42d5-9221-274f924e69ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e52677d-9d83-48e9-8b22-8e4df330a918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f0c1d75-739c-42d5-9221-274f924e69ef",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "a02cbb0b-7ac7-416f-ad49-4c31afd9d3a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "3b2ed4b4-af91-4e30-92fa-49ecff4d5724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a02cbb0b-7ac7-416f-ad49-4c31afd9d3a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe973522-fafe-4c5b-9de1-8908546608be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a02cbb0b-7ac7-416f-ad49-4c31afd9d3a0",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "8b94044c-b948-477a-8369-8ff3434e3cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "16159302-29a3-491f-a8ca-6a84943e1d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b94044c-b948-477a-8369-8ff3434e3cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8710774-b1d2-4b8a-a68c-ce6d0d74bcfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b94044c-b948-477a-8369-8ff3434e3cae",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "bbe70311-b6eb-4bdf-beea-5b19fd710dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "71fba7fa-e29a-4c0c-b74f-321cf49c3e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe70311-b6eb-4bdf-beea-5b19fd710dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f79149c-fa26-49ba-83b7-435f09288578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe70311-b6eb-4bdf-beea-5b19fd710dfd",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "6923269a-ab03-488d-ad06-202c6449fd44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "715ff6b8-d780-48bb-a9c5-8c1dd1f605c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6923269a-ab03-488d-ad06-202c6449fd44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6098bf99-8b0c-4298-90d3-530d9ec1d3a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6923269a-ab03-488d-ad06-202c6449fd44",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "4381c6fd-5881-4f98-b316-e96982f3a3a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "908ff302-6081-473a-9579-ab67eb81b519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4381c6fd-5881-4f98-b316-e96982f3a3a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a8097f-12f5-4001-96cd-55a8e80ae6ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4381c6fd-5881-4f98-b316-e96982f3a3a9",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "62b7ad1b-3593-4c9f-9313-58b2cd497747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "fa50c24d-c8db-400e-be30-693ceda7e312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b7ad1b-3593-4c9f-9313-58b2cd497747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dbb5076-5d97-4197-aa52-eaffafa525f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b7ad1b-3593-4c9f-9313-58b2cd497747",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "b22a6e27-69c7-4ae3-85ee-82367066f6b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "7d9dbab0-d2f6-4b24-ae34-c7ee51f429c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22a6e27-69c7-4ae3-85ee-82367066f6b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fed78df-f4bf-4801-b22e-d19618500835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22a6e27-69c7-4ae3-85ee-82367066f6b1",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "72d8515c-a109-4b90-821f-1828a3509563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "56b55d91-5a3a-4173-8d84-46511eb7eba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72d8515c-a109-4b90-821f-1828a3509563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ff9919-942c-4a56-96b2-48391584569b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d8515c-a109-4b90-821f-1828a3509563",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "0f85f672-866c-42ab-ba03-d1e59eca562d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "1c203732-4c2a-47a0-95fa-70e2bee1d0c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f85f672-866c-42ab-ba03-d1e59eca562d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0410cb9-b04c-4757-a448-57b8ac3d4775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f85f672-866c-42ab-ba03-d1e59eca562d",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        },
        {
            "id": "9c902fc5-7e10-40a8-ad4b-c1516cdc2323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "compositeImage": {
                "id": "9930efb9-788e-43f6-a5e2-f9d4fc93ec74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c902fc5-7e10-40a8-ad4b-c1516cdc2323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005baa91-3113-490d-8ba4-fecf5e5d5d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c902fc5-7e10-40a8-ad4b-c1516cdc2323",
                    "LayerId": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 227,
    "layers": [
        {
            "id": "6ff97c5c-75e2-4000-9d48-2bfe302a23dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "286e6d8d-f240-49cc-9693-da15a0174b73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 172,
    "xorig": 86,
    "yorig": 113
}