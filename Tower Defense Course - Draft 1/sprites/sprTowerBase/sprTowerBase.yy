{
    "id": "8a625369-0676-4c62-bf59-0d43e60bba3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTowerBase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 225,
    "bbox_left": 39,
    "bbox_right": 220,
    "bbox_top": 38,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08f2fc80-4a61-4919-96b9-c953d051e1ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a625369-0676-4c62-bf59-0d43e60bba3d",
            "compositeImage": {
                "id": "bbbadee7-e291-4e73-86f1-5d5482f1113a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f2fc80-4a61-4919-96b9-c953d051e1ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06e38fdc-674b-433e-9aef-0429fb8418e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f2fc80-4a61-4919-96b9-c953d051e1ae",
                    "LayerId": "55b9491f-d188-4f83-89d4-ad0627bfa432"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "55b9491f-d188-4f83-89d4-ad0627bfa432",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a625369-0676-4c62-bf59-0d43e60bba3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}