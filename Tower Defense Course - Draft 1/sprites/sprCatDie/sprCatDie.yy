{
    "id": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCatDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 226,
    "bbox_left": 0,
    "bbox_right": 322,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74998e83-4d52-4010-9927-e5c63c0a11c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "77514efd-5fa9-40c4-9089-ca53a3c5309a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74998e83-4d52-4010-9927-e5c63c0a11c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2b5f54-2083-4cdb-8783-b9e8f406fada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74998e83-4d52-4010-9927-e5c63c0a11c2",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "868ca1af-f8c5-4dac-80ca-1f058ca5899e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "37b1becf-6cf0-43ae-b745-a7b4ce876276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "868ca1af-f8c5-4dac-80ca-1f058ca5899e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3711c66-c8e1-4494-88b4-017562e15a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "868ca1af-f8c5-4dac-80ca-1f058ca5899e",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "3a84a5dd-1e12-4b37-8c7b-90fef5631b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "d7dfdfed-efe2-4f1f-b22a-667509fcba67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a84a5dd-1e12-4b37-8c7b-90fef5631b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc912d49-90ef-49d7-9c49-86cd6682cd34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a84a5dd-1e12-4b37-8c7b-90fef5631b62",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "e7a4f8e9-ce7b-4041-a655-41e49cc12663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "bd5f9481-0771-472b-973b-482015b7c84b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a4f8e9-ce7b-4041-a655-41e49cc12663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05df4243-b69e-49b8-8c1a-5b0ccd88c503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a4f8e9-ce7b-4041-a655-41e49cc12663",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "fe5722cf-2e7e-4d9e-8aa6-e3fd4be354d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "515ddde2-88dc-4aaa-a040-32ca6ba5cf29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5722cf-2e7e-4d9e-8aa6-e3fd4be354d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0bb8f3-431e-4a4f-bd02-57ae0dcf1ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5722cf-2e7e-4d9e-8aa6-e3fd4be354d9",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "1d51a9f3-f5fc-4865-b2a9-183eb5855741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "01f989ba-3f88-4bc6-9926-675b45c51495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d51a9f3-f5fc-4865-b2a9-183eb5855741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05826363-21ea-464e-bb52-093cb7d1dd94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d51a9f3-f5fc-4865-b2a9-183eb5855741",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "6894732b-f557-428f-ac69-1303c79ead8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "34eec318-7699-4088-9eb3-1d523c7614cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6894732b-f557-428f-ac69-1303c79ead8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "497ef643-8342-454e-b593-92cb451abd60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6894732b-f557-428f-ac69-1303c79ead8b",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "f7fc284c-a0c3-4472-8181-470d732fc744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "30731522-fabe-49aa-bc58-4c05c4ede027",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7fc284c-a0c3-4472-8181-470d732fc744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a984aa6-98f6-4663-b4e3-1dea7f734f4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7fc284c-a0c3-4472-8181-470d732fc744",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "c12e632c-7d35-44a9-b691-9352c7a219a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "c914f86c-ba41-4452-acef-0bb910464a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12e632c-7d35-44a9-b691-9352c7a219a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f84966f-812a-41f8-a453-f2051a32f2ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12e632c-7d35-44a9-b691-9352c7a219a6",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "53a90fcf-76e3-4fa0-bb43-24b5c0c6a60c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "0c289ca6-55f0-4750-bf38-3a5f053d740d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a90fcf-76e3-4fa0-bb43-24b5c0c6a60c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5468412c-cf06-4913-8702-cf74b47d756a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a90fcf-76e3-4fa0-bb43-24b5c0c6a60c",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "96046154-3514-44d2-ab87-8251d9e0a907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "63b7c56f-ab34-4e1c-9dbc-e24b61576998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96046154-3514-44d2-ab87-8251d9e0a907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "388385f1-ade6-4344-bf0f-90afae6f4e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96046154-3514-44d2-ab87-8251d9e0a907",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "ce25bb70-6588-479c-8732-694a7471b00f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "69a69d2d-0d5a-4f0d-ad85-de813a4a0108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce25bb70-6588-479c-8732-694a7471b00f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "835c2749-779c-4e72-8691-b4fc188f8d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce25bb70-6588-479c-8732-694a7471b00f",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "365502fa-62db-497e-bb47-00ff1c2926e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "9f322c3e-9815-49e9-842f-53b91101fd92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365502fa-62db-497e-bb47-00ff1c2926e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b1c850-02cb-49de-bd50-5ae96559d644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365502fa-62db-497e-bb47-00ff1c2926e3",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "0e2dda76-e9bd-4ab7-89eb-026d45f82f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "12141232-3fa0-4275-b057-44f31f098ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e2dda76-e9bd-4ab7-89eb-026d45f82f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab67e68-f628-4571-9b19-cc74f5c0ae83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e2dda76-e9bd-4ab7-89eb-026d45f82f53",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "80d2a023-67d2-4b95-b356-8c97837dbb03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "4d5cf9e0-0468-4289-9198-e55b48d1bd44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d2a023-67d2-4b95-b356-8c97837dbb03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dba3c1b2-2f40-4a4c-96e4-b1e7c3b94230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d2a023-67d2-4b95-b356-8c97837dbb03",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "edb741d7-5b41-4230-ba6d-1676d9001a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "5f2f8137-f9a2-45fe-8b2d-a1bdbfc9b03f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb741d7-5b41-4230-ba6d-1676d9001a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b2b240e-9ab9-4e40-b8d8-505a02ff443a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb741d7-5b41-4230-ba6d-1676d9001a0f",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "ab766a14-37a9-4c18-8fb0-ae7003e65c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "02053828-aea3-46e5-8abd-63183d38f643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab766a14-37a9-4c18-8fb0-ae7003e65c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30dd0c97-fb07-4e70-af35-babc470b401b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab766a14-37a9-4c18-8fb0-ae7003e65c5c",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "89c395a5-743e-4b6f-aa5a-f5e12d0ed9b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "168b3839-7373-438c-926d-40726bc2f584",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c395a5-743e-4b6f-aa5a-f5e12d0ed9b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e141e955-0222-4a45-bb1d-89732ab1825a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c395a5-743e-4b6f-aa5a-f5e12d0ed9b6",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "11f54432-7d57-42c0-9d46-84848d1be73d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "e7b4f206-f897-4d2f-bc9a-ba708b68060a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11f54432-7d57-42c0-9d46-84848d1be73d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985df294-82a8-4d7d-9af8-12f0389cf820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11f54432-7d57-42c0-9d46-84848d1be73d",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        },
        {
            "id": "c7939e60-0156-4052-86fc-b935ea4ab1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "compositeImage": {
                "id": "44d925e5-465a-4c7e-a1d3-77f81996c575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7939e60-0156-4052-86fc-b935ea4ab1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a57fb93-04e9-457d-860d-f56b9b989f7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7939e60-0156-4052-86fc-b935ea4ab1e7",
                    "LayerId": "a81ab899-d78e-415e-8451-b41b42bfddcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 227,
    "layers": [
        {
            "id": "a81ab899-d78e-415e-8451-b41b42bfddcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb4f7cc3-a799-4c8f-a1b8-f6522f290a9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 323,
    "xorig": 161,
    "yorig": 113
}