{
    "id": "4a240d09-49d4-44a2-8535-289a8142a53b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprScorpionRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 173,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe36b7b3-384f-401e-8a0f-d7daa341fe7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "7af92c4a-25ed-4717-ae19-8859cf3d055e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe36b7b3-384f-401e-8a0f-d7daa341fe7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c63e07-cea9-4575-8c83-b4990e14a4b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe36b7b3-384f-401e-8a0f-d7daa341fe7c",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "fb06ab8e-f3aa-42b9-86d5-2f6bdd2db397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "6ffff1b5-3f56-4042-9622-b2e6055c95a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb06ab8e-f3aa-42b9-86d5-2f6bdd2db397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38010172-64af-4a11-8d5a-a221738259a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb06ab8e-f3aa-42b9-86d5-2f6bdd2db397",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "aa0efe82-3892-4dba-93f3-175eba357cc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "c954d90a-0b53-4713-ab43-5323ba019012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0efe82-3892-4dba-93f3-175eba357cc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfcf22c1-02ee-439e-870f-ffcacea44b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0efe82-3892-4dba-93f3-175eba357cc9",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "b1e0f8da-bfb4-444c-a6cc-3219535814a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "1faf9ce2-5e11-432d-a3f1-fc1f471c3cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e0f8da-bfb4-444c-a6cc-3219535814a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "892d08bd-749b-4bfe-b0d9-81fced958a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e0f8da-bfb4-444c-a6cc-3219535814a2",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "5d26bb8d-f0ac-40f1-8287-9f294e31ba42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "8bc73552-436c-41cc-a0db-9506a990dc53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d26bb8d-f0ac-40f1-8287-9f294e31ba42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4c497f-3afd-4680-b9dd-cdfe364ba051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d26bb8d-f0ac-40f1-8287-9f294e31ba42",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "8340b2eb-c70d-4bb3-8a5b-7f836eab542d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "ac62b78f-45e4-474f-8c65-bf286d369953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8340b2eb-c70d-4bb3-8a5b-7f836eab542d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e971125-95b3-40ad-bcdc-4844fdeb0a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8340b2eb-c70d-4bb3-8a5b-7f836eab542d",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "fdd15df7-e1a7-4907-ab46-ce5db986a847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "69ce45e5-caa3-47e4-80a2-9d870220e1e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd15df7-e1a7-4907-ab46-ce5db986a847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a973f5-2897-4a73-8d05-b51e9d6826d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd15df7-e1a7-4907-ab46-ce5db986a847",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "e79cd7a9-2144-432b-bfe5-2db8ac543d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "4205f4d4-9340-48a3-a9e8-d62ad62b62c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79cd7a9-2144-432b-bfe5-2db8ac543d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1331739e-d9c8-4a5e-ad85-fb8d4f2c50cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79cd7a9-2144-432b-bfe5-2db8ac543d5e",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "25a705ce-4367-4042-89a9-12160565fead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "e3ecdba9-d2d3-4b09-a982-51349c57dee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a705ce-4367-4042-89a9-12160565fead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e70331-fd98-4134-963f-ae8d3cc2c496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a705ce-4367-4042-89a9-12160565fead",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "5aa02050-59dc-40fc-8e77-049e66d22b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "fae81d9d-7e1d-439e-90c2-0cf865fe547a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aa02050-59dc-40fc-8e77-049e66d22b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05708b1-1e53-417b-b876-6f43a63a29d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aa02050-59dc-40fc-8e77-049e66d22b45",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "d318b0c5-12e9-44b3-be1c-beac4d9f38cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "074ba71b-948a-43b9-8dab-91cd437eed43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d318b0c5-12e9-44b3-be1c-beac4d9f38cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23c83470-9dcd-46ee-bd47-f0a6569f0808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d318b0c5-12e9-44b3-be1c-beac4d9f38cb",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "8169c285-a62d-403d-9cd0-766eddcfbe8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "40b5fe88-91bb-431c-8741-353ad27a48c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8169c285-a62d-403d-9cd0-766eddcfbe8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "908cf390-b829-4a34-8f6c-5d2c76ab7694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8169c285-a62d-403d-9cd0-766eddcfbe8d",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "6de22102-2d23-476f-9818-7c798ec2a93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "ef4f843a-55b0-478e-b102-bb426082fe49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de22102-2d23-476f-9818-7c798ec2a93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5190aa4a-9a38-4521-a400-9910ddc9ff26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de22102-2d23-476f-9818-7c798ec2a93c",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "e1944cb4-fd6e-469b-a21a-a5db7d6ad114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "8ae75df5-dde4-4991-977b-93ac2ccf26ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1944cb4-fd6e-469b-a21a-a5db7d6ad114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871afbff-8d62-4fce-8fa7-275b2e19b7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1944cb4-fd6e-469b-a21a-a5db7d6ad114",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "46d91979-df04-4a8f-a620-6e392850c736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "cf16047f-b097-46ca-a947-2f3d896ed1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d91979-df04-4a8f-a620-6e392850c736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f28ec76-1749-45b3-a27d-9a0fe0dcf97c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d91979-df04-4a8f-a620-6e392850c736",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "4f3b4ff1-bdf1-4e64-9ea9-b7a67d6b87d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "388aabfb-375b-4368-828e-406a0e730d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f3b4ff1-bdf1-4e64-9ea9-b7a67d6b87d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3744560d-3f51-4128-af53-1eab6ed39807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f3b4ff1-bdf1-4e64-9ea9-b7a67d6b87d5",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "a856293c-c8ef-47f0-a27e-b9e8ce9e2d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "2cda14b6-ee8a-4e81-ac0c-015183c04e0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a856293c-c8ef-47f0-a27e-b9e8ce9e2d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1050b763-9c48-4080-ba37-09ed79103fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a856293c-c8ef-47f0-a27e-b9e8ce9e2d7c",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "da9a67c7-1d47-477f-afdf-af5cbbbf3e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "63ca769a-24e0-40e6-9bf4-0c9fcb84afc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9a67c7-1d47-477f-afdf-af5cbbbf3e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54cc5643-f8a6-48ae-ba4e-6ddbd0dac7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9a67c7-1d47-477f-afdf-af5cbbbf3e7b",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "08e94bc7-80ee-4489-9a7c-9ece7ae07d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "f8429fc9-0b88-4763-bd5f-d62113377ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08e94bc7-80ee-4489-9a7c-9ece7ae07d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0636e0aa-e086-454e-a39b-e26bbd43e3b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08e94bc7-80ee-4489-9a7c-9ece7ae07d56",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        },
        {
            "id": "cd4e5e34-47ea-4d45-8ea2-433413d1a2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "compositeImage": {
                "id": "9b4789ba-a9ab-4605-931f-c177676f0fee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4e5e34-47ea-4d45-8ea2-433413d1a2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd5e142-e04b-4d4c-b82c-2ad2650f69b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4e5e34-47ea-4d45-8ea2-433413d1a2d0",
                    "LayerId": "97c0725b-df37-48a1-afe5-ffa2579dcde0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 174,
    "layers": [
        {
            "id": "97c0725b-df37-48a1-afe5-ffa2579dcde0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 50,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 87
}