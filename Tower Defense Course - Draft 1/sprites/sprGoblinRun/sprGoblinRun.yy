{
    "id": "5fdee4ac-84cc-441b-9813-6d38428e1047",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 0,
    "bbox_right": 161,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96fb8a87-8c93-4773-afa6-dc251004e977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "f73635aa-823b-4082-a51d-ef2360e00935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96fb8a87-8c93-4773-afa6-dc251004e977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f976085-585b-40af-ae8c-f903a5206d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96fb8a87-8c93-4773-afa6-dc251004e977",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "287d60c5-6b6f-4e4a-958c-06ff898f3d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "268935a1-f748-4820-bb9b-822fa1803ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287d60c5-6b6f-4e4a-958c-06ff898f3d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e58f986-9f7e-4f65-98ec-26efee7d067f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287d60c5-6b6f-4e4a-958c-06ff898f3d23",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "1c52f9ce-b033-4ffa-8193-3228fdf924da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "1831f716-9975-4d0f-a831-a3e2390442b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c52f9ce-b033-4ffa-8193-3228fdf924da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb8f22e3-34e2-4662-bb84-9c643cc0fe28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c52f9ce-b033-4ffa-8193-3228fdf924da",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "7a057bb4-3100-4cd9-905a-a7ac6b9db4e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "ccd3fb8e-da18-436c-9f25-07ec40b1c583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a057bb4-3100-4cd9-905a-a7ac6b9db4e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ffcd03-ac63-4dd5-ace2-0e72c2812e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a057bb4-3100-4cd9-905a-a7ac6b9db4e1",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "2bc2b89d-61d2-4040-8e20-c732fd1840ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "45e4c6d5-92d3-4bce-8e7c-44a9ef928f77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc2b89d-61d2-4040-8e20-c732fd1840ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c41d8a-2899-4f21-806a-add6272b4312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc2b89d-61d2-4040-8e20-c732fd1840ea",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "cc134978-088c-4323-a83d-6dfd1f4ea813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "55e724b0-2c3a-4b2a-973b-f49871cb97c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc134978-088c-4323-a83d-6dfd1f4ea813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08972464-70ed-4d78-b535-9ae9cf17a09b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc134978-088c-4323-a83d-6dfd1f4ea813",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "a0717ecd-c426-4854-b7d5-9e8b719d0799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "321f767f-0585-4b5d-9732-147e74780695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0717ecd-c426-4854-b7d5-9e8b719d0799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf1b632-4a13-4562-90a5-3f60f5edf6ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0717ecd-c426-4854-b7d5-9e8b719d0799",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "bc2d1b24-babb-4f07-a4e9-e2985e533f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "6acd8c16-3c9c-4053-a3a5-d4ccaa166eed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2d1b24-babb-4f07-a4e9-e2985e533f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b1f0fe3-8697-4b5c-96a8-e8f3c5694e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2d1b24-babb-4f07-a4e9-e2985e533f0f",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "cc7218a1-1271-4dfa-bd9f-a185c970273a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "15d9acda-689b-4fae-80f6-2141e9621209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7218a1-1271-4dfa-bd9f-a185c970273a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c0e3a56-1ade-41c0-a084-e969168e4c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7218a1-1271-4dfa-bd9f-a185c970273a",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "6db10aab-4a52-4815-9fe9-6736e4aad869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "c70e98c4-db89-4169-a8e9-6744374ced47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db10aab-4a52-4815-9fe9-6736e4aad869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32db29e2-028f-4130-98cd-0ec5e90c3576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db10aab-4a52-4815-9fe9-6736e4aad869",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "4c864ca2-014b-431a-80a2-b7d7ede1861f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "2df947b8-979e-4735-a29a-e66313f8db72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c864ca2-014b-431a-80a2-b7d7ede1861f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "effdfaa8-a7d1-4f98-8bbe-bff258339e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c864ca2-014b-431a-80a2-b7d7ede1861f",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "4d922b0c-a71b-488f-8b6d-c6f7aaf14b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "d59e24c3-2961-440b-ba00-5a9b48faaec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d922b0c-a71b-488f-8b6d-c6f7aaf14b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12273d79-ede7-4a06-bfe6-f7b0c53261d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d922b0c-a71b-488f-8b6d-c6f7aaf14b25",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "77d2a683-39c7-4917-94ff-62d72df38fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "905c1c97-3f46-47ff-8c58-fdac097ab74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d2a683-39c7-4917-94ff-62d72df38fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db435871-a14f-4acc-a2af-b7149cfb1cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d2a683-39c7-4917-94ff-62d72df38fd8",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "91056401-14d5-40f7-968f-b5248e269962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "ec4f0649-f1be-4321-add8-86a62f1cdf55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91056401-14d5-40f7-968f-b5248e269962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe8befd-ea84-451c-b405-dc9bc4bd3eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91056401-14d5-40f7-968f-b5248e269962",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "da4631e9-54ff-4c2f-93a7-e16054d93ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "5e38da04-80cc-477f-97ec-dd462207a824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4631e9-54ff-4c2f-93a7-e16054d93ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036a7971-8fb0-4509-b018-0e9ac52243cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4631e9-54ff-4c2f-93a7-e16054d93ffc",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "78f0d21e-09d9-40b2-840d-6c3f01ae0bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "714faf10-438f-4ea6-9f4d-ff1a5fc12e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f0d21e-09d9-40b2-840d-6c3f01ae0bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c695bc-22fc-4684-94ac-bbc04f07b7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f0d21e-09d9-40b2-840d-6c3f01ae0bc7",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "f794ec1e-b2a7-4105-b1e1-8682a89350dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "d79a5075-083f-442e-b431-c73f3f329c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f794ec1e-b2a7-4105-b1e1-8682a89350dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0468b0e2-38b5-4330-8fc3-057ff9b187d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f794ec1e-b2a7-4105-b1e1-8682a89350dc",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "6618e251-09f3-423f-8d5f-1e59f4b0b0b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "c129b050-93b0-49cd-b34f-6bfcc5f29e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6618e251-09f3-423f-8d5f-1e59f4b0b0b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58abef99-5942-46fc-9f50-2b864f39395e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6618e251-09f3-423f-8d5f-1e59f4b0b0b9",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "b145b5cc-0fb1-421a-8576-d1ce2f5c7e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "ef663e48-e44b-42fc-ab7e-363a99b774dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b145b5cc-0fb1-421a-8576-d1ce2f5c7e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0e43ea-a0be-466e-9248-501c4cbcac6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b145b5cc-0fb1-421a-8576-d1ce2f5c7e0d",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        },
        {
            "id": "dbecf6e5-16ee-46c3-87c3-3c3c1f3c5464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "compositeImage": {
                "id": "bd479714-dc83-468d-9ee2-15047c1a1e23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbecf6e5-16ee-46c3-87c3-3c3c1f3c5464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41f57161-e109-44ee-a534-9764315c81c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbecf6e5-16ee-46c3-87c3-3c3c1f3c5464",
                    "LayerId": "c304a1c8-b30e-4f46-a639-08e2c195dbbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 228,
    "layers": [
        {
            "id": "c304a1c8-b30e-4f46-a639-08e2c195dbbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fdee4ac-84cc-441b-9813-6d38428e1047",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 81,
    "yorig": 114
}