{
    "id": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSkeletonFly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 398,
    "bbox_left": 0,
    "bbox_right": 295,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "384fcfa5-c78f-4816-ad89-470295d5895c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "bd5e11a0-6c94-4532-9d49-5e08945833c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "384fcfa5-c78f-4816-ad89-470295d5895c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a82f00-4d8c-4f64-a8ab-a29f6de864db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "384fcfa5-c78f-4816-ad89-470295d5895c",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "71a0cd75-ab9e-42de-9b1a-0a244b6dd5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "402e4b57-544a-4aeb-afaf-faf41a73a78b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a0cd75-ab9e-42de-9b1a-0a244b6dd5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b17e68-380a-4560-a498-daae21b12924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a0cd75-ab9e-42de-9b1a-0a244b6dd5c6",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "960c3d14-4332-483c-9a4f-d04108e4cd76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "18d314a5-29c4-457f-a267-69cba98049bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960c3d14-4332-483c-9a4f-d04108e4cd76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7459f24b-1f5d-405d-a656-605ba24adee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960c3d14-4332-483c-9a4f-d04108e4cd76",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "be461bac-0d01-4bf9-8812-c12fabcce99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "4bd85758-3a39-4929-a30e-3fbd18978916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be461bac-0d01-4bf9-8812-c12fabcce99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68721639-7004-4557-9c2f-d06e8a77bee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be461bac-0d01-4bf9-8812-c12fabcce99a",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "8a53a7af-26b3-40dd-bc58-83f8d94d21c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "abda1d0d-4655-4d97-a712-ba18f1224ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a53a7af-26b3-40dd-bc58-83f8d94d21c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d23b74-58f5-4aa5-9db5-9c0196faec59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a53a7af-26b3-40dd-bc58-83f8d94d21c1",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "228d87c0-c6c6-4931-acf9-3985f9ef9767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "de6f709c-b92c-43f2-a076-06ded92ac801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228d87c0-c6c6-4931-acf9-3985f9ef9767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b57c03d-7a70-46ec-b22f-d0ab3658c24b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228d87c0-c6c6-4931-acf9-3985f9ef9767",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "2ac2cb0d-0d8d-472f-aba1-f62ee239b7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "f13fd3c8-ff35-4b45-ad12-8381eaf967b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ac2cb0d-0d8d-472f-aba1-f62ee239b7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9844cc1-bae9-49ad-b0e2-5186216cff05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ac2cb0d-0d8d-472f-aba1-f62ee239b7d2",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "1a2e926f-8d15-4013-855d-a0ac6b84deb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "26558056-8ed6-4f37-af64-95bbb3fb553e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2e926f-8d15-4013-855d-a0ac6b84deb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8191017e-a7ca-4ff5-b131-c6a4afc04059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2e926f-8d15-4013-855d-a0ac6b84deb3",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "b9cb4572-8a60-486a-87c1-a96361f7f840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "79bab884-55f9-47fd-bb59-9ea6d54fae9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cb4572-8a60-486a-87c1-a96361f7f840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3e88bc2-5206-4742-91ef-702b9696d88f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cb4572-8a60-486a-87c1-a96361f7f840",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        },
        {
            "id": "7c67bef8-f98c-42b8-8b68-d20cfb2e6d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "compositeImage": {
                "id": "d46deba3-980d-41d9-a424-f1998c5a68e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c67bef8-f98c-42b8-8b68-d20cfb2e6d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bafc19d-b203-4667-9738-c13edcce0962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c67bef8-f98c-42b8-8b68-d20cfb2e6d6d",
                    "LayerId": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 399,
    "layers": [
        {
            "id": "cf484fd6-106a-4dc6-a651-a29ec33fc1ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 296,
    "xorig": 148,
    "yorig": 199
}