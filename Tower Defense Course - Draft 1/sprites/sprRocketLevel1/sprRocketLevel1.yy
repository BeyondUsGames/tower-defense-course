{
    "id": "6ce492ad-b49d-4087-9d1b-8ec4a90ceae8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRocketLevel1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 41,
    "bbox_right": 219,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4faa976a-e2dc-4725-879b-066b270a84b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ce492ad-b49d-4087-9d1b-8ec4a90ceae8",
            "compositeImage": {
                "id": "f9ba745f-6bc7-4b1b-ba8f-79f62f70d468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4faa976a-e2dc-4725-879b-066b270a84b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f242dae4-354c-4f09-b4f7-36841c249250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4faa976a-e2dc-4725-879b-066b270a84b5",
                    "LayerId": "6e20ff32-c19c-4844-a115-a656880d0c7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6e20ff32-c19c-4844-a115-a656880d0c7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ce492ad-b49d-4087-9d1b-8ec4a90ceae8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}