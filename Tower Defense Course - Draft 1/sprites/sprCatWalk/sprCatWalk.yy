{
    "id": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCatWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 230,
    "bbox_left": 0,
    "bbox_right": 225,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93ad45b5-b49f-4fb9-95bc-1521afc4e714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "4a2c823f-bcf5-4fb9-a055-b145cabe6bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93ad45b5-b49f-4fb9-95bc-1521afc4e714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff2d86b-d2b4-41cc-96d2-232d85bdc8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93ad45b5-b49f-4fb9-95bc-1521afc4e714",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "c55c742f-4f2e-4995-91da-fc3c89e1faa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "7b0b390a-5515-45f2-bcb5-ececeac4878d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c55c742f-4f2e-4995-91da-fc3c89e1faa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "757ca50c-d4b8-4abb-828e-50703290c3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c55c742f-4f2e-4995-91da-fc3c89e1faa4",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "62945aa1-d266-4e06-800a-4fd9ebd6df34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "acb658fb-6b30-4f18-9b04-27afc0c010a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62945aa1-d266-4e06-800a-4fd9ebd6df34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5030e8b9-ed39-41c7-8c38-2fe0575d5fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62945aa1-d266-4e06-800a-4fd9ebd6df34",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "526d8206-5640-46c8-bc0d-b26802cd7b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "8ca26d73-2c9a-45c2-822c-d2be21fcb150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526d8206-5640-46c8-bc0d-b26802cd7b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc26ed14-8a59-4a1d-972f-a793ce532b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526d8206-5640-46c8-bc0d-b26802cd7b0a",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "2466400b-99a8-4074-9b4a-3de823d3b02f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "af5af43d-720a-41cb-a3cd-78ecd52590ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2466400b-99a8-4074-9b4a-3de823d3b02f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a65a6af5-4d50-41db-97e9-93972afa2951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2466400b-99a8-4074-9b4a-3de823d3b02f",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "117a23b7-3798-4d47-bb82-47160499c8da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "5c3a4702-a6d5-48c5-8e61-48ccccb414f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "117a23b7-3798-4d47-bb82-47160499c8da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e63a38c6-e9d6-4f0a-8fa7-1f38730c86fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "117a23b7-3798-4d47-bb82-47160499c8da",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "2217dfa1-391b-44e2-9b1e-8dec9c7e4154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "3f11ba83-9a36-490f-8181-796ecefdcf17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2217dfa1-391b-44e2-9b1e-8dec9c7e4154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eee8af9-8cee-4741-bddf-4b44edb2931c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2217dfa1-391b-44e2-9b1e-8dec9c7e4154",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "4dc69b73-c3e1-48a9-bce8-e2f012ae25e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "a9246828-33fe-4871-895a-29fab47a4649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc69b73-c3e1-48a9-bce8-e2f012ae25e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c5504e-75a6-404d-aa1e-4407be1551a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc69b73-c3e1-48a9-bce8-e2f012ae25e1",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "22fac3c0-c141-4910-884b-a1f22559e964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "3dcf6eed-53d3-4294-ac90-325ec6eae75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22fac3c0-c141-4910-884b-a1f22559e964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56cd29fc-9bd4-4883-a148-6a6951a029c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22fac3c0-c141-4910-884b-a1f22559e964",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "d68f5d8a-3e0f-453f-867f-603692d105cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "e1cf606f-897d-46f4-baed-12f6646a2f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d68f5d8a-3e0f-453f-867f-603692d105cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e909199b-71bf-4c43-80f4-fe76b41ee11f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d68f5d8a-3e0f-453f-867f-603692d105cf",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "5fe517f2-a9c8-4a46-9f66-646d7b495f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "4717ba0f-97ff-432e-a174-2d4da1aac245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe517f2-a9c8-4a46-9f66-646d7b495f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6910ec36-65f2-4622-9945-e1098b9795ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe517f2-a9c8-4a46-9f66-646d7b495f42",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "90ed4db1-bfe1-4943-80df-c62e6a75f3e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "131de736-1c89-49c6-b7cb-efa8b95cbbb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90ed4db1-bfe1-4943-80df-c62e6a75f3e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bc79c7-8c6c-4ec1-b3cd-468e1b70abd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90ed4db1-bfe1-4943-80df-c62e6a75f3e4",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "2ea53c13-4944-47a8-a171-9f7455bc7330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "439ca989-b164-4a19-84d1-6498b6b0a3c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ea53c13-4944-47a8-a171-9f7455bc7330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be004a0c-e72c-46f0-afff-c1679dd94409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea53c13-4944-47a8-a171-9f7455bc7330",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "a472a922-7fef-4ed1-8c20-90297a779267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "a04f0f84-3627-40dc-a9fe-970c905be9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a472a922-7fef-4ed1-8c20-90297a779267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1676cad-eb5c-40e5-95c7-17cc6600670e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a472a922-7fef-4ed1-8c20-90297a779267",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "9205615a-c843-42d3-b12b-5d966d4a2d69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "c595fd0f-731a-4a16-9b6c-1b7cd08fd1f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9205615a-c843-42d3-b12b-5d966d4a2d69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adb4e64b-7c6b-44b8-9d61-12ce35a88bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9205615a-c843-42d3-b12b-5d966d4a2d69",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "758eb352-4028-4088-962e-93d280d09f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "3ab4b58d-0370-4eb5-b754-9b716ba73ab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "758eb352-4028-4088-962e-93d280d09f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d5831e-ead4-4d10-b8ad-72697e865932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "758eb352-4028-4088-962e-93d280d09f62",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "e8a7cefc-bca5-4397-bbf6-97f3cabc78e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "86e2cb19-88e4-4f5b-921e-d1726c6b1f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a7cefc-bca5-4397-bbf6-97f3cabc78e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8fb4ccd-86db-434a-af16-73dd71330a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a7cefc-bca5-4397-bbf6-97f3cabc78e3",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "7007a5f3-4496-45df-8aca-95e5eedc0d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "837f0a9f-c971-4c79-b7e8-bd10e18a2d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7007a5f3-4496-45df-8aca-95e5eedc0d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d51106e8-8026-4ed1-b1f2-d9a43365dd7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7007a5f3-4496-45df-8aca-95e5eedc0d58",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "1ec0b9a6-3114-4db1-9a30-fc4ec5467774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "a4be3e0e-1acd-4fd0-9851-cbccbbb02e81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec0b9a6-3114-4db1-9a30-fc4ec5467774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b23cd20-b6bf-4704-8a72-3cf37c20843d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec0b9a6-3114-4db1-9a30-fc4ec5467774",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        },
        {
            "id": "0ffff912-e7b6-4ef4-86a9-0b7f11c05b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "compositeImage": {
                "id": "21922b13-0397-4455-97fc-4f1e3c4fa9b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ffff912-e7b6-4ef4-86a9-0b7f11c05b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa7599b-1ec3-42ae-bdfa-85a23c974f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ffff912-e7b6-4ef4-86a9-0b7f11c05b2c",
                    "LayerId": "9f652488-76f5-42d3-b54c-8d34ac6c9f34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 231,
    "layers": [
        {
            "id": "9f652488-76f5-42d3-b54c-8d34ac6c9f34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 226,
    "xorig": 113,
    "yorig": 115
}