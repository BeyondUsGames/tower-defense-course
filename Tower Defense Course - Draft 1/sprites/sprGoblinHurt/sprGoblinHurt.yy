{
    "id": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinHurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 225,
    "bbox_left": 0,
    "bbox_right": 202,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b2f7900-78c2-4c4e-adb2-8ea85d54ca8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "ff54f819-631c-494d-bd07-af92e2b5f23c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2f7900-78c2-4c4e-adb2-8ea85d54ca8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d303a71-094b-48db-b97b-f8d6268421b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2f7900-78c2-4c4e-adb2-8ea85d54ca8d",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "5e94bf8d-2473-409e-81d4-a82c13f830b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "5ee6d561-b98b-492e-bf59-68095d2b3b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e94bf8d-2473-409e-81d4-a82c13f830b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9100066-979b-4a9b-b58a-3da510dc4ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e94bf8d-2473-409e-81d4-a82c13f830b3",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "5eb95c3a-ff8e-40ab-92f7-6369e397ec26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "bd03a29f-f1ba-46ce-baee-af3c0eea5be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb95c3a-ff8e-40ab-92f7-6369e397ec26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847efeb3-5fd6-408e-9b64-794e750f34a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb95c3a-ff8e-40ab-92f7-6369e397ec26",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "ab7cfbe8-1eea-4930-a381-68a81ec7e657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "7c2faa7c-37f9-418a-8ffb-678fdfcf31ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab7cfbe8-1eea-4930-a381-68a81ec7e657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d754577-b07c-429f-905c-db76d6d3043b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab7cfbe8-1eea-4930-a381-68a81ec7e657",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "0d8599f2-6237-424d-862b-1795cedec59c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "c960b5c9-2e0a-4891-a237-6b4c39032177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d8599f2-6237-424d-862b-1795cedec59c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d72a860-13c5-49bb-baa7-c1a69a568301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d8599f2-6237-424d-862b-1795cedec59c",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "43556ecd-95ed-49c3-9d6c-0616855f7553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "b09cdf9d-2358-4729-a1f7-e2570414200e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43556ecd-95ed-49c3-9d6c-0616855f7553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f44d290-ec5f-4cf0-a3a6-211cf9a4f9f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43556ecd-95ed-49c3-9d6c-0616855f7553",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "0bb79a34-3dae-4a2a-be58-94f4ee4f8679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "666d7f82-5a50-4bbd-bba2-1068b2f994ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb79a34-3dae-4a2a-be58-94f4ee4f8679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c44da01-7312-4fb9-8538-1a5041d6b760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb79a34-3dae-4a2a-be58-94f4ee4f8679",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "e97ae11e-5bb9-4169-8c5e-0ea0ffa2ac66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "21f79023-fdf9-4ef8-b053-b7d9a9a18eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97ae11e-5bb9-4169-8c5e-0ea0ffa2ac66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7923a1b-324f-42a7-bd66-25f36da9966e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97ae11e-5bb9-4169-8c5e-0ea0ffa2ac66",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "21643caf-b98d-4403-8920-58f13e678df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "5933afdb-cf90-470b-b7ab-017a4b6b2782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21643caf-b98d-4403-8920-58f13e678df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6e8d85-08c8-49a3-acc7-5b3ece0a0ae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21643caf-b98d-4403-8920-58f13e678df3",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "b4e8ff9e-6c7d-4596-850d-e4668ce362cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "9b1d20ed-588f-4d0a-93f5-9034f22a1691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e8ff9e-6c7d-4596-850d-e4668ce362cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843f8431-530a-4fd9-ab91-5c408f26db23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e8ff9e-6c7d-4596-850d-e4668ce362cc",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "5c7d60ce-542b-4df5-b43f-9f11e33cf9cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "cbf0149c-a19a-40d5-a084-7951c883b39b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7d60ce-542b-4df5-b43f-9f11e33cf9cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f64afc-019b-4ffd-a246-248d15977d69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7d60ce-542b-4df5-b43f-9f11e33cf9cf",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "09b707f2-8a73-4094-a249-ec995cbe0a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "81f08d15-dc2b-4611-b576-3b2a25b50eaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09b707f2-8a73-4094-a249-ec995cbe0a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f4de236-0f97-4c0b-9408-025cf0508617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09b707f2-8a73-4094-a249-ec995cbe0a68",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "8d38d1c4-055a-49e1-a847-b8412e1f1951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "bce817f9-97b1-4864-8d6e-3a4d2a093478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d38d1c4-055a-49e1-a847-b8412e1f1951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab7b83a-a0a1-4587-be2d-69145719f501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d38d1c4-055a-49e1-a847-b8412e1f1951",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "f2eb98a5-6d8b-4d9b-8ee3-0e7159d536cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "6da02028-27ed-41aa-8a89-d3efd8d6bf21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2eb98a5-6d8b-4d9b-8ee3-0e7159d536cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d657a2-a57b-4cf1-a508-c3dc2acb2898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2eb98a5-6d8b-4d9b-8ee3-0e7159d536cf",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "e76646a9-e0a7-4de6-a2bc-99fbfea7e2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "ae944d8c-f433-45e0-bf85-d12b33cc450a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e76646a9-e0a7-4de6-a2bc-99fbfea7e2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112926aa-6931-4c05-8b30-7c08770819f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e76646a9-e0a7-4de6-a2bc-99fbfea7e2f2",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "4e5c34a4-62b9-4d3f-9ec2-fc70209ece89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "5746181a-9caf-4999-ba5e-c5b5bcd3fd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e5c34a4-62b9-4d3f-9ec2-fc70209ece89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bbb243d-aa0c-456c-962a-59e168614519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e5c34a4-62b9-4d3f-9ec2-fc70209ece89",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "a859f3eb-07d1-4d34-b281-3c9166d6f4f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "777353a1-141a-4e60-9161-a434dbfa11fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a859f3eb-07d1-4d34-b281-3c9166d6f4f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf61971-a972-47eb-bfa7-01622632fc5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a859f3eb-07d1-4d34-b281-3c9166d6f4f7",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "9517c31a-aba5-4063-9ce8-d78585b0885c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "45bbdaba-e858-45f0-b9dc-60aa612497f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9517c31a-aba5-4063-9ce8-d78585b0885c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3edc5bb5-c7c7-4c42-af92-06c7d283e6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9517c31a-aba5-4063-9ce8-d78585b0885c",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "ca0e38ff-2583-4494-b006-43477daad847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "e4f435db-741d-4639-b401-45278d60551b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0e38ff-2583-4494-b006-43477daad847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ccc13d-d73c-4a3d-8798-6d104396b645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0e38ff-2583-4494-b006-43477daad847",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        },
        {
            "id": "5964e583-5596-43f5-b82d-6b28e42dd799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "compositeImage": {
                "id": "d43a950c-7bb0-409a-be2d-b11e39eabfe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5964e583-5596-43f5-b82d-6b28e42dd799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916629f4-9e91-45a1-84a6-332cfcd2a36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5964e583-5596-43f5-b82d-6b28e42dd799",
                    "LayerId": "2f95a44f-4deb-45fd-913c-ec8487f8de70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 226,
    "layers": [
        {
            "id": "2f95a44f-4deb-45fd-913c-ec8487f8de70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "542a10ad-6e92-4d20-8862-30a50cbcfe87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 75,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 203,
    "xorig": 101,
    "yorig": 113
}