{
    "id": "f2ae7bdc-9644-4dc8-bea5-7644a481d7dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "220146c5-7c32-4602-9248-77c6847c4035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ae7bdc-9644-4dc8-bea5-7644a481d7dc",
            "compositeImage": {
                "id": "30692778-ae92-4006-b534-4f58dbe671b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220146c5-7c32-4602-9248-77c6847c4035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e39631dd-889a-4075-83a0-8e197b7db24a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220146c5-7c32-4602-9248-77c6847c4035",
                    "LayerId": "eae6a521-93cd-4502-86fa-a4e0b1b4b7ff"
                }
            ]
        },
        {
            "id": "82f4a0b2-6f1b-484c-8119-3115839f7008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ae7bdc-9644-4dc8-bea5-7644a481d7dc",
            "compositeImage": {
                "id": "415b4d95-1e39-49ac-bdf1-68114066888d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f4a0b2-6f1b-484c-8119-3115839f7008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47e3dbd1-f7bd-4b39-8c2c-f1c01f29fd09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f4a0b2-6f1b-484c-8119-3115839f7008",
                    "LayerId": "eae6a521-93cd-4502-86fa-a4e0b1b4b7ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eae6a521-93cd-4502-86fa-a4e0b1b4b7ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2ae7bdc-9644-4dc8-bea5-7644a481d7dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}