{
    "id": "bbff83ed-219a-45cc-a3e0-8ed145c8846a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprContinueButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 227,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eb2e8f3-d924-4bb8-8dcf-c8e7fe6c71df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbff83ed-219a-45cc-a3e0-8ed145c8846a",
            "compositeImage": {
                "id": "ecf67a51-c6ac-4d42-90db-60177a97c69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eb2e8f3-d924-4bb8-8dcf-c8e7fe6c71df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc8d9a6-4b0f-46b4-ac3c-8c31246182c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eb2e8f3-d924-4bb8-8dcf-c8e7fe6c71df",
                    "LayerId": "504ca2b7-5f28-4c30-984a-edd86d7a54df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "504ca2b7-5f28-4c30-984a-edd86d7a54df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbff83ed-219a-45cc-a3e0-8ed145c8846a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 228,
    "xorig": 114,
    "yorig": 17
}