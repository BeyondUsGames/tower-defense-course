{
    "id": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinDie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 296,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "247aa692-f6f7-4cdf-8c85-a4866c9926cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "499c7f09-2a8e-4859-8329-4cfb3a03b803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "247aa692-f6f7-4cdf-8c85-a4866c9926cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca83fe6-c9b3-4789-a293-64ba07cb05ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "247aa692-f6f7-4cdf-8c85-a4866c9926cb",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "72297bb1-544e-4624-8d22-397c32825e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "1450a20d-ad3f-4afb-a0a2-2639e6197c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72297bb1-544e-4624-8d22-397c32825e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9a0cef-7c8d-4f50-8116-30a1718ba446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72297bb1-544e-4624-8d22-397c32825e67",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "8224af97-b7ab-4b78-989f-974acf970c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "7bf68683-7f26-4a41-9b71-0646a58c832f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8224af97-b7ab-4b78-989f-974acf970c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad7e8f2-d446-4879-8591-4da174a111be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8224af97-b7ab-4b78-989f-974acf970c20",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "5c76e893-bd77-4fad-a185-43c50ca276e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "c199beba-6248-4d86-b98c-3d9585663f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c76e893-bd77-4fad-a185-43c50ca276e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a54224a-4604-4e72-8041-37697f5462fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c76e893-bd77-4fad-a185-43c50ca276e9",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "e041529c-fc17-4200-9b3b-24cf138b36e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "8263da92-5ed2-493c-a232-f51231463d09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e041529c-fc17-4200-9b3b-24cf138b36e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873f0b82-70f3-4848-b44b-76633556f141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e041529c-fc17-4200-9b3b-24cf138b36e2",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "aa5496dc-33b1-4e1c-bb77-93e2bc84d37e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "b89a9088-cbae-4474-b64d-0c0e82abd9eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5496dc-33b1-4e1c-bb77-93e2bc84d37e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6868891a-bb95-48a0-a74b-7dba2f991f7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5496dc-33b1-4e1c-bb77-93e2bc84d37e",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "03a16998-a10a-48b9-a214-8f9faa651c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "354ff793-0ac6-4eda-8962-88c6db99e605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a16998-a10a-48b9-a214-8f9faa651c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4e2684-7105-4664-b3b1-5b25e1c4a6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a16998-a10a-48b9-a214-8f9faa651c1f",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "875ec558-00dc-487e-8ace-2d70d54f2003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "776237b6-8c9a-4d18-9fae-00e887193f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875ec558-00dc-487e-8ace-2d70d54f2003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8f23cb-5e9a-40da-99da-d10439e4430b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875ec558-00dc-487e-8ace-2d70d54f2003",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "d5330c47-e99d-4797-90bb-e794a309f081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "52d70057-5835-40bd-9109-1d68673d9f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5330c47-e99d-4797-90bb-e794a309f081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "254a8e4f-539b-4eed-8fdf-d008097f0fe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5330c47-e99d-4797-90bb-e794a309f081",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "0670e21c-6fcf-45fc-b2c7-26ab29a71f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "aafb1e19-65a1-45c4-91aa-701e47e9c845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0670e21c-6fcf-45fc-b2c7-26ab29a71f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fe7b169-ca21-4c45-b0a1-b08dc741f3d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0670e21c-6fcf-45fc-b2c7-26ab29a71f91",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "3a70b026-d9c0-4793-99be-51ca7ef7b72f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "c1217d1a-db09-4d1c-b22e-19b0f2dc2bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a70b026-d9c0-4793-99be-51ca7ef7b72f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c0e5b4-b8d1-44f4-9bfc-540606dbef28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a70b026-d9c0-4793-99be-51ca7ef7b72f",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "f6a316be-f1bd-4625-98e6-1ea99f3f5249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "1449824e-6de4-4d54-a913-7420fb33869c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a316be-f1bd-4625-98e6-1ea99f3f5249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "516e31b9-c054-4953-b678-29b0f96b31bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a316be-f1bd-4625-98e6-1ea99f3f5249",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "40493c90-058b-4182-a73d-d93724a2614d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "199bfbeb-b120-43b8-9b73-d81fe2b45735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40493c90-058b-4182-a73d-d93724a2614d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58da7212-e799-4191-b637-7318a0da750c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40493c90-058b-4182-a73d-d93724a2614d",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "56f1e932-e445-4fc2-9861-2b207797b2e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "d0f6d4d4-d05f-48ef-b453-1b2904e0cd3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f1e932-e445-4fc2-9861-2b207797b2e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aad9f01-918a-43d7-9006-39893e25c094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f1e932-e445-4fc2-9861-2b207797b2e7",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "426e1060-1f1c-40f1-b552-95535fdc9061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "b91673a6-3b1e-4b93-9052-2e4d04231007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426e1060-1f1c-40f1-b552-95535fdc9061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e97c0e0-0104-4a54-8639-0ac800cf839d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426e1060-1f1c-40f1-b552-95535fdc9061",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "84b7b2df-6a94-4277-af1a-4c0539285ab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "36777c79-aa27-4434-9eb4-90a8090d2924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b7b2df-6a94-4277-af1a-4c0539285ab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d510756-547e-432d-924b-bf31c0d8fdb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b7b2df-6a94-4277-af1a-4c0539285ab6",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "26a99409-7a57-418b-91ee-234301c4df4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "c240d773-5aa8-4834-93b6-9d78b7831db1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a99409-7a57-418b-91ee-234301c4df4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae0c2e4-5dcf-4897-a10c-39181aa59a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a99409-7a57-418b-91ee-234301c4df4b",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "8323ec6b-0d94-4849-af1c-550e615fc118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "44acf0dc-e3b5-422f-9ef6-f1291624f5e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8323ec6b-0d94-4849-af1c-550e615fc118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638527b5-1761-4c80-b48c-5e130252ed5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8323ec6b-0d94-4849-af1c-550e615fc118",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "7022d82f-81a5-4bba-a9c7-07a58d7f2694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "c1b65c8c-047b-47ce-aee9-2d7ab8f01a23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7022d82f-81a5-4bba-a9c7-07a58d7f2694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af95f724-a51a-4b06-aa3c-d084d4594ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7022d82f-81a5-4bba-a9c7-07a58d7f2694",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        },
        {
            "id": "57e5ba6d-44f5-4d5f-a09a-ac36055746d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "compositeImage": {
                "id": "38b8614d-0edf-4eac-aafb-da4b54a1ddf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e5ba6d-44f5-4d5f-a09a-ac36055746d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4674ea7-758c-44a4-8ad2-79dbb4f8e0d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e5ba6d-44f5-4d5f-a09a-ac36055746d0",
                    "LayerId": "e5b6d094-d11c-4667-b7db-208d281d7b6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "e5b6d094-d11c-4667-b7db-208d281d7b6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "452394dc-81b4-4caf-88a3-e1a9ce42858b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 297,
    "xorig": 148,
    "yorig": 120
}