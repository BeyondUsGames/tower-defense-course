{
    "id": "52d5a707-a807-473a-b49c-14ed9e60ecd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "987b9637-396d-4441-a052-8872dcbbcab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52d5a707-a807-473a-b49c-14ed9e60ecd1",
            "compositeImage": {
                "id": "60f296a4-b71a-4bc7-88d2-bb3bb7c6fa94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "987b9637-396d-4441-a052-8872dcbbcab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f97b7c-6491-4c69-8d37-dcc0722b894e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "987b9637-396d-4441-a052-8872dcbbcab3",
                    "LayerId": "8edd7a2e-3007-47ca-8c35-33f8318565cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8edd7a2e-3007-47ca-8c35-33f8318565cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52d5a707-a807-473a-b49c-14ed9e60ecd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}