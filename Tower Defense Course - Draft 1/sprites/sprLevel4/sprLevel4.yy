{
    "id": "17c3e913-a6d1-45e0-8f0a-01a62860a5b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "467b1725-fa83-4d73-b03c-f52c93816ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c3e913-a6d1-45e0-8f0a-01a62860a5b7",
            "compositeImage": {
                "id": "ea604a3b-a76f-44f5-8307-a139af272df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467b1725-fa83-4d73-b03c-f52c93816ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a564a442-5b3c-427f-bdcc-fce964ec69ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467b1725-fa83-4d73-b03c-f52c93816ffc",
                    "LayerId": "3cc88c7b-1d53-411f-a020-56ab56c7709a"
                }
            ]
        },
        {
            "id": "dfb2d17f-3b93-45cd-8459-81328d657c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c3e913-a6d1-45e0-8f0a-01a62860a5b7",
            "compositeImage": {
                "id": "db7b8a58-b7a4-46d0-a631-b3ca9ce5432c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb2d17f-3b93-45cd-8459-81328d657c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6bcf763-7115-416d-bfbd-ae1edeb53b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb2d17f-3b93-45cd-8459-81328d657c6e",
                    "LayerId": "3cc88c7b-1d53-411f-a020-56ab56c7709a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3cc88c7b-1d53-411f-a020-56ab56c7709a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17c3e913-a6d1-45e0-8f0a-01a62860a5b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}