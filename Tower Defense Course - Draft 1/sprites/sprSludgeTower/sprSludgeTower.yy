{
    "id": "f9521dea-5ee8-482d-9f01-c5dd27d1bfd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSludgeTower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 174,
    "bbox_left": 85,
    "bbox_right": 254,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f63c27ca-07a6-4223-b333-5796d6b8f888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9521dea-5ee8-482d-9f01-c5dd27d1bfd8",
            "compositeImage": {
                "id": "c1483747-1103-46a7-83c3-72fea4d91d39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63c27ca-07a6-4223-b333-5796d6b8f888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4b43b83-03ec-4e84-ac8d-0dc1e05cea4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63c27ca-07a6-4223-b333-5796d6b8f888",
                    "LayerId": "f270c687-98fc-44bb-961b-e312be40d29f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f270c687-98fc-44bb-961b-e312be40d29f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9521dea-5ee8-482d-9f01-c5dd27d1bfd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}