{
    "id": "e4afb13b-1e8f-4a2c-bb52-7c3baf291f00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLightDesertBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "394d9901-8dd7-45ee-a9ce-75c2431f6501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4afb13b-1e8f-4a2c-bb52-7c3baf291f00",
            "compositeImage": {
                "id": "a5311701-5d7d-4484-8940-825eef9573ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "394d9901-8dd7-45ee-a9ce-75c2431f6501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125fa111-6b7d-4cad-bf34-1a400fe51557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "394d9901-8dd7-45ee-a9ce-75c2431f6501",
                    "LayerId": "91c177e3-a3e5-4ddf-b813-fb83913f4964"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "91c177e3-a3e5-4ddf-b813-fb83913f4964",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4afb13b-1e8f-4a2c-bb52-7c3baf291f00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 360
}