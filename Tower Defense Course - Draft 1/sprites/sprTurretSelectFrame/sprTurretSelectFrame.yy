{
    "id": "d61e9d81-74a9-49b3-9942-49a49c2f7d01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTurretSelectFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 162,
    "bbox_left": 0,
    "bbox_right": 713,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e00b1998-e665-4f46-9dce-0c42b9afc296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61e9d81-74a9-49b3-9942-49a49c2f7d01",
            "compositeImage": {
                "id": "de998322-1a39-482f-8d45-78bacc68913b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e00b1998-e665-4f46-9dce-0c42b9afc296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c070df0-2b37-4059-8b4b-5c69b0555496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e00b1998-e665-4f46-9dce-0c42b9afc296",
                    "LayerId": "b6c06b53-c8fa-4a2d-aa91-0186f533f336"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 163,
    "layers": [
        {
            "id": "b6c06b53-c8fa-4a2d-aa91-0186f533f336",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d61e9d81-74a9-49b3-9942-49a49c2f7d01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 714,
    "xorig": 357,
    "yorig": 81
}