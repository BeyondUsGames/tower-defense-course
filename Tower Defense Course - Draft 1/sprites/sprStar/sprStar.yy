{
    "id": "78bdf8b3-f1df-4b3a-bc94-2ae92295ac76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5240ca00-d2c7-4fa2-ac33-e96b36cd2b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78bdf8b3-f1df-4b3a-bc94-2ae92295ac76",
            "compositeImage": {
                "id": "2edd3af6-cdaa-4fb5-9f4f-66014f8dac0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5240ca00-d2c7-4fa2-ac33-e96b36cd2b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21d7303c-f2af-482c-a9fa-3e172b2abdc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5240ca00-d2c7-4fa2-ac33-e96b36cd2b63",
                    "LayerId": "78480a60-e37b-43e6-9ee5-e4b8d8a929b6"
                }
            ]
        },
        {
            "id": "baaf0881-1676-4f2d-804d-1aed039b1726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78bdf8b3-f1df-4b3a-bc94-2ae92295ac76",
            "compositeImage": {
                "id": "a3574ee3-6321-4958-85fd-759d9561aa65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baaf0881-1676-4f2d-804d-1aed039b1726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ac9d9a-69cd-4236-8774-74203fa94d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baaf0881-1676-4f2d-804d-1aed039b1726",
                    "LayerId": "78480a60-e37b-43e6-9ee5-e4b8d8a929b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "78480a60-e37b-43e6-9ee5-e4b8d8a929b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78bdf8b3-f1df-4b3a-bc94-2ae92295ac76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}