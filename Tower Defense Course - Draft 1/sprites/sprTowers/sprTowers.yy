{
    "id": "b0343b0a-c77b-47b3-8600-3319b06dbe76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTowers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1658,
    "bbox_left": 0,
    "bbox_right": 2943,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea66e3fb-07be-46ad-ae2d-bff6d4ee81f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0343b0a-c77b-47b3-8600-3319b06dbe76",
            "compositeImage": {
                "id": "ad56c37f-6a3b-4d14-8728-75e768f99eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea66e3fb-07be-46ad-ae2d-bff6d4ee81f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e8b8927-832d-4142-b557-46e3478c2743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea66e3fb-07be-46ad-ae2d-bff6d4ee81f4",
                    "LayerId": "7eb6f7a3-ec53-4f0c-a1d9-d78d2f75e3a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1664,
    "layers": [
        {
            "id": "7eb6f7a3-ec53-4f0c-a1d9-d78d2f75e3a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0343b0a-c77b-47b3-8600-3319b06dbe76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2944,
    "xorig": 1472,
    "yorig": 832
}