{
    "id": "ef456b74-90f5-4328-b793-0241ad8e1eee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1283cbf9-f576-49e7-a3ce-c4d34ad5d5e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef456b74-90f5-4328-b793-0241ad8e1eee",
            "compositeImage": {
                "id": "12316707-ee47-4889-9593-49721896d2ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1283cbf9-f576-49e7-a3ce-c4d34ad5d5e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bae72f-a0c5-41db-9059-dd29fa82d9c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1283cbf9-f576-49e7-a3ce-c4d34ad5d5e0",
                    "LayerId": "22d639f2-7152-4bb0-8998-9487ac08100b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "22d639f2-7152-4bb0-8998-9487ac08100b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef456b74-90f5-4328-b793-0241ad8e1eee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}