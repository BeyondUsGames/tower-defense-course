{
    "id": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 264,
    "bbox_left": 0,
    "bbox_right": 185,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80e04624-1868-4b26-89a8-33761add730e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "8cbc343d-af9d-4b20-99c9-feed6788b7f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e04624-1868-4b26-89a8-33761add730e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47b8d4d-b0e3-444d-909e-c7778026f76c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e04624-1868-4b26-89a8-33761add730e",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "08176082-4c59-4010-97da-bf452dbf10a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "9efb0ea6-fd25-4e27-8181-114cbc786be9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08176082-4c59-4010-97da-bf452dbf10a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bac4dc74-6f77-4c3b-80d6-01eb773d5d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08176082-4c59-4010-97da-bf452dbf10a9",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "a2d702c6-df64-4e7d-b7a6-38cfb533ae10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "4dea4156-6258-4c2e-b4e4-7d122040b762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2d702c6-df64-4e7d-b7a6-38cfb533ae10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77fe41e3-7a33-4b04-a4c0-8e505082511a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2d702c6-df64-4e7d-b7a6-38cfb533ae10",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "f18c0875-7838-4a92-9238-16f01221f527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "22d4da6e-a442-4edb-b9ab-9ad38b977509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18c0875-7838-4a92-9238-16f01221f527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30932988-8c51-40ef-a569-eb8e9c686df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18c0875-7838-4a92-9238-16f01221f527",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "c44069c3-0c75-4dfe-8985-a98999d1b1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "5c7a0677-37c6-4b77-8f0d-45d5ed89332b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c44069c3-0c75-4dfe-8985-a98999d1b1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace4e5da-e2c6-4a62-9bde-eb081629770d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44069c3-0c75-4dfe-8985-a98999d1b1f5",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "296ed362-f333-446b-9220-c5ffc296cdf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "e99a01c6-fb01-4053-8d19-815029867954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "296ed362-f333-446b-9220-c5ffc296cdf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c11268-2d35-4d82-996d-333b4f829e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "296ed362-f333-446b-9220-c5ffc296cdf4",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "cf6b7e8a-9dcf-4b0f-be13-a22c030ca7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "754a3d72-976e-4865-9f04-a6c87fec2c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf6b7e8a-9dcf-4b0f-be13-a22c030ca7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab583178-11ca-4685-80f6-e3157ca459d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6b7e8a-9dcf-4b0f-be13-a22c030ca7ea",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "a12a35d1-02ee-4d56-9163-6aec017a187e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "748c00bf-1f06-4492-b0dd-7738343d0ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a12a35d1-02ee-4d56-9163-6aec017a187e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3bec11-4078-42b3-b807-34f9dcf2966a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a12a35d1-02ee-4d56-9163-6aec017a187e",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "2e06942b-650d-49eb-81d4-374fda4b494c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "df47432b-b429-4426-b5df-421f3c4e0087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e06942b-650d-49eb-81d4-374fda4b494c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7b1f21-01bc-4125-aacd-f2fde30c330e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e06942b-650d-49eb-81d4-374fda4b494c",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "b35acdfd-18df-4433-9fab-16a4bcbb9324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "dcfef72b-299e-43e4-b96c-28a10db73a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b35acdfd-18df-4433-9fab-16a4bcbb9324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50c1dcd2-3bcd-471c-a370-d4d470048690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b35acdfd-18df-4433-9fab-16a4bcbb9324",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "ab828fac-2c03-4165-bfb2-dea866c0c7cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "c93c9392-5d8d-47e3-a1ee-b60523a4223c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab828fac-2c03-4165-bfb2-dea866c0c7cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde81330-5257-418b-a140-c5c3950ecf9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab828fac-2c03-4165-bfb2-dea866c0c7cd",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "9c3d501c-7dd0-41a9-8ebf-c3389e4e25ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "f2711d6c-ce6c-4bd6-a20e-94be13e74566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c3d501c-7dd0-41a9-8ebf-c3389e4e25ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a43b040-f888-4b33-96ed-bfb9159b972f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c3d501c-7dd0-41a9-8ebf-c3389e4e25ea",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "952c5875-aff0-49ee-9931-26b84c9565bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "8ec47ac8-ab61-4690-8950-ba54e38d7446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "952c5875-aff0-49ee-9931-26b84c9565bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1e0721-796a-4880-8a82-d9da9dc99371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "952c5875-aff0-49ee-9931-26b84c9565bd",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "f64e6d2f-81db-4de3-a32a-2f232c8c396a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "b9569778-5a6c-4755-8b57-61b1daf4f26c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64e6d2f-81db-4de3-a32a-2f232c8c396a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "792e17aa-b03c-4819-8c3b-a02d44c014b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64e6d2f-81db-4de3-a32a-2f232c8c396a",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "17df5c3f-d873-4e0d-ac0f-75446d61aa6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "113c7456-48a0-4549-8e35-8eb308fcd794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17df5c3f-d873-4e0d-ac0f-75446d61aa6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf93571f-6473-4e13-966c-ba6e4971c1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17df5c3f-d873-4e0d-ac0f-75446d61aa6e",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "d9429bad-1538-44b2-9dab-e4951967df66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "a884de3b-68be-4bfb-866b-7752f02a7714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9429bad-1538-44b2-9dab-e4951967df66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7aa4281-6ce9-4840-a9e8-a8a744ab03e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9429bad-1538-44b2-9dab-e4951967df66",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "7c6b61c7-b3b8-4b0a-a629-abe164062212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "813c5274-efb6-4d43-b4e9-2f3be86c5170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c6b61c7-b3b8-4b0a-a629-abe164062212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6282151c-410c-4b6a-8326-15c421d8c735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c6b61c7-b3b8-4b0a-a629-abe164062212",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "530852bf-9a29-4ede-9885-ee911df0ee8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "1a51155b-6b73-498e-9939-4b22be0547b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "530852bf-9a29-4ede-9885-ee911df0ee8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "110cfbfd-dfdc-4cb8-bcd1-6855055d7db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "530852bf-9a29-4ede-9885-ee911df0ee8e",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "dcbc7d87-797c-431f-ac2c-ecb4c99f3332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "8c051306-0639-4254-84f4-0ae791ccc479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcbc7d87-797c-431f-ac2c-ecb4c99f3332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e7e69b-95ba-42b4-a966-5583a270e133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcbc7d87-797c-431f-ac2c-ecb4c99f3332",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "34887461-fc4c-4285-8e37-a9983d61c7e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "5d3b6f55-d36b-4217-b1d6-81beb58e4d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34887461-fc4c-4285-8e37-a9983d61c7e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4adbec98-7212-4013-beb2-2d50d70f1680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34887461-fc4c-4285-8e37-a9983d61c7e7",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        },
        {
            "id": "7525e742-0901-4234-8bd8-93903ce02c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "compositeImage": {
                "id": "b357bee8-890f-4cc4-9cda-9a70b8e95f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7525e742-0901-4234-8bd8-93903ce02c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4538a210-2e60-4085-800a-fdd8f06d302e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7525e742-0901-4234-8bd8-93903ce02c0f",
                    "LayerId": "b0a82870-24d1-43ca-9656-eaa366c5e017"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 265,
    "layers": [
        {
            "id": "b0a82870-24d1-43ca-9656-eaa366c5e017",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "136551dd-d869-4e4d-81f3-bdc85fd6cafe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 40,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 186,
    "xorig": 93,
    "yorig": 132
}