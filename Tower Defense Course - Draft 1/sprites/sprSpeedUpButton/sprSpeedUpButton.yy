{
    "id": "64e16c90-5e54-47a3-88f1-a9ee6f5fd3d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpeedUpButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76d78f0b-8201-47a1-9fc3-0f68e108c07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e16c90-5e54-47a3-88f1-a9ee6f5fd3d5",
            "compositeImage": {
                "id": "f61fed90-aafd-4f6e-8cd3-8bb1414b0931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d78f0b-8201-47a1-9fc3-0f68e108c07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cdc9918-ba09-423c-baaa-1eadb81ded2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d78f0b-8201-47a1-9fc3-0f68e108c07d",
                    "LayerId": "370843ef-727c-4d58-a868-48d70f7fd4b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "370843ef-727c-4d58-a868-48d70f7fd4b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64e16c90-5e54-47a3-88f1-a9ee6f5fd3d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}