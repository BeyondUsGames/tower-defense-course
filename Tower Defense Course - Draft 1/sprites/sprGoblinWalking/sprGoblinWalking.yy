{
    "id": "22904b76-7934-418d-beb6-8a8ace039ae9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinWalking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fcf9a7d-109a-4ff5-a004-5211ec61c432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "6ddf5cc7-b47a-4191-80a2-6fc16c0b2637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcf9a7d-109a-4ff5-a004-5211ec61c432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddab3c3a-e672-401a-ab00-c59fed1a8708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcf9a7d-109a-4ff5-a004-5211ec61c432",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "2ebec112-ad93-4f35-bf1f-42dcdf673724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "63047081-ddeb-4e7f-b5e6-206f7b1fa629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ebec112-ad93-4f35-bf1f-42dcdf673724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f817f7a-aeb8-4bf6-ae43-e0f86bf12404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ebec112-ad93-4f35-bf1f-42dcdf673724",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "9ae487b9-7b1c-47d4-9663-6356d2ccccc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "ccc95345-184a-498d-b712-15ecb8a33cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ae487b9-7b1c-47d4-9663-6356d2ccccc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77cf64c2-94da-408b-8541-4e37fcbc913b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ae487b9-7b1c-47d4-9663-6356d2ccccc1",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "c7cd41e8-fb21-4501-bbc1-fedeeaca653e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "1916cc54-6e93-49c0-acec-11b7fd892a9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7cd41e8-fb21-4501-bbc1-fedeeaca653e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b2ee40-70e3-4643-90d2-497d58b8414f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7cd41e8-fb21-4501-bbc1-fedeeaca653e",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "a75ead89-aa3c-451d-a9c4-97ed33c17bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "62bd3b90-7bd4-4f79-950b-90863e5eb93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a75ead89-aa3c-451d-a9c4-97ed33c17bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77583d2f-c620-4b95-b8b8-d9131cb2e59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a75ead89-aa3c-451d-a9c4-97ed33c17bab",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "1ae067a3-ef76-480f-950e-b9f73c87e0d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "4e1ec74c-06ae-4644-9461-51deb61ef102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ae067a3-ef76-480f-950e-b9f73c87e0d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ff9c9e-69b5-4fad-9a8b-0dc2ae0db957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ae067a3-ef76-480f-950e-b9f73c87e0d4",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "66e64df1-4de8-4ada-84f7-37da3537f8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "351932e4-1737-4d10-9d4d-f6abe886213c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66e64df1-4de8-4ada-84f7-37da3537f8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a699a3cb-1af9-4988-a66e-eedcebf019e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e64df1-4de8-4ada-84f7-37da3537f8e4",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "e49ce4f2-ddf0-4d6d-b809-3e0011c960f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "d8888243-aafb-4d7e-9eb0-2fbb4f170f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e49ce4f2-ddf0-4d6d-b809-3e0011c960f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b83abc64-87c6-4c17-afee-c0dc5b9e5e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e49ce4f2-ddf0-4d6d-b809-3e0011c960f5",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "2d593269-594e-4258-8b44-90fb23ca3d2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "8c02d51e-cad1-4875-b70d-86e51a51a1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d593269-594e-4258-8b44-90fb23ca3d2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad17580-2907-4bc3-a825-93f033424c84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d593269-594e-4258-8b44-90fb23ca3d2d",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "1db97efe-0389-4668-9d68-53e72c8e0373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "b35d4b8e-8710-4214-bdf1-2f7283db240c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db97efe-0389-4668-9d68-53e72c8e0373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f1d0c3-954c-4eae-88a6-b5d249df7c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db97efe-0389-4668-9d68-53e72c8e0373",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "1df6abf0-0587-4a20-bc40-8d2d19bf3101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "baec3008-89b4-49ac-a0c2-bde602ffa302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df6abf0-0587-4a20-bc40-8d2d19bf3101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9979c97-e22c-4400-ac65-828dec45b052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df6abf0-0587-4a20-bc40-8d2d19bf3101",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "7e2a4bd3-1f92-49c8-bf90-7f25f6403939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "744b4832-1e4f-4338-b5bb-244719e47e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e2a4bd3-1f92-49c8-bf90-7f25f6403939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595a3ed4-5be8-4014-b0c9-62eaf5767823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e2a4bd3-1f92-49c8-bf90-7f25f6403939",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "99760348-366e-42dc-b210-db8ec8ed4deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "7a28b16a-15dc-45bd-9a66-c108c3771f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99760348-366e-42dc-b210-db8ec8ed4deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18e697f8-8e7d-4071-ad65-86e5ac7e9f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99760348-366e-42dc-b210-db8ec8ed4deb",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "ab5d0db7-d9e6-430d-a90f-c880403ee717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "def5723d-c598-4530-b801-e66b598ede66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab5d0db7-d9e6-430d-a90f-c880403ee717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101301e7-fff0-46b9-b17a-b3423a03c825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab5d0db7-d9e6-430d-a90f-c880403ee717",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "9ef2d78c-9df8-4d8b-b960-0e96455a75ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "17ef36a6-5bf8-47fb-a903-39fb8763fac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef2d78c-9df8-4d8b-b960-0e96455a75ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172343f7-eb60-4648-99ca-fbec64ef41ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef2d78c-9df8-4d8b-b960-0e96455a75ef",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "70440d17-cd47-4a6c-a0bb-85e20ef2962f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "446be210-f363-4517-9b0d-d9c797e0e029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70440d17-cd47-4a6c-a0bb-85e20ef2962f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc05b314-97a2-4004-befb-830b81553672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70440d17-cd47-4a6c-a0bb-85e20ef2962f",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "698c15b2-d14a-4e71-acaa-0aaed4897f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "699e1d26-8845-46b7-9ddc-3253b6089418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698c15b2-d14a-4e71-acaa-0aaed4897f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223faded-06fa-4a1b-a1a6-2846c372ac42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698c15b2-d14a-4e71-acaa-0aaed4897f7b",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "f0db8b1f-45bf-4fc7-8f20-4f151a2b5264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "bd37e72d-70b6-40f1-9f13-1011c97800df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0db8b1f-45bf-4fc7-8f20-4f151a2b5264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "195e090a-6ef5-464f-b1a3-443a1d3a2c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0db8b1f-45bf-4fc7-8f20-4f151a2b5264",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "960a3946-b8fb-47fb-8816-7c43d35759c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "b535ada8-7ccf-4fbf-a3cc-cf4ae3e5b6a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960a3946-b8fb-47fb-8816-7c43d35759c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45487665-1f98-4426-9b31-88a2547e6741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960a3946-b8fb-47fb-8816-7c43d35759c6",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        },
        {
            "id": "bf6548ac-d2f8-4d04-9e0b-4d1812a46ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "compositeImage": {
                "id": "ada159f1-2351-4372-9ef0-a40aa35b0837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6548ac-d2f8-4d04-9e0b-4d1812a46ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a6b0176-1573-4368-9fb7-8112237d0aa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6548ac-d2f8-4d04-9e0b-4d1812a46ff5",
                    "LayerId": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 228,
    "layers": [
        {
            "id": "7b306fff-28bf-4bcf-a4e2-5e6eb09b95bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 91,
    "yorig": 114
}