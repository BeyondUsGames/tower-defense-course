{
    "id": "df3ac57f-43c9-452c-9fce-c01d0e49e5d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevelInfoFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 162,
    "bbox_left": 0,
    "bbox_right": 522,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d653a3c-5fb6-4981-b457-38157e2feb71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df3ac57f-43c9-452c-9fce-c01d0e49e5d2",
            "compositeImage": {
                "id": "38f3851c-7003-4570-a935-b8ab869e7b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d653a3c-5fb6-4981-b457-38157e2feb71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62a9240-8404-4cce-9f5f-1adf9a8ecdbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d653a3c-5fb6-4981-b457-38157e2feb71",
                    "LayerId": "d5269f1e-6562-47b9-b343-6f34b03d5c8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 163,
    "layers": [
        {
            "id": "d5269f1e-6562-47b9-b343-6f34b03d5c8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df3ac57f-43c9-452c-9fce-c01d0e49e5d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 523,
    "xorig": 261,
    "yorig": 81
}