{
    "id": "fb60d124-53a6-4c7c-a83c-798b9a94304e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprUpgradeTowerButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed06c31d-9a5e-491b-9eac-3d7893ac8eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb60d124-53a6-4c7c-a83c-798b9a94304e",
            "compositeImage": {
                "id": "d0794422-0ef0-4550-aeb7-8f3bbe839fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed06c31d-9a5e-491b-9eac-3d7893ac8eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef7007b5-dab7-41ba-ac90-cfc2a3f71b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed06c31d-9a5e-491b-9eac-3d7893ac8eee",
                    "LayerId": "9b6c3718-b421-4e5b-9a2b-12e5c0e56ddb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "9b6c3718-b421-4e5b-9a2b-12e5c0e56ddb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb60d124-53a6-4c7c-a83c-798b9a94304e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 23,
    "yorig": 24
}