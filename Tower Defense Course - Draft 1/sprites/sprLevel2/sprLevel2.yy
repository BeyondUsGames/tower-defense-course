{
    "id": "98681fb7-2cdd-419e-8674-11dc71b75743",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLevel2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cdadf04-c9d0-45e7-8eb7-4767499fd912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98681fb7-2cdd-419e-8674-11dc71b75743",
            "compositeImage": {
                "id": "3e122280-791c-40b8-b655-d2abe6f522c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cdadf04-c9d0-45e7-8eb7-4767499fd912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51663572-bf36-471c-82e0-b1043a3fb80a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cdadf04-c9d0-45e7-8eb7-4767499fd912",
                    "LayerId": "e9f7d8c9-a11e-4b75-a977-3b097c30b6d5"
                }
            ]
        },
        {
            "id": "5c51a606-873a-4f1b-8205-b82cb422b0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98681fb7-2cdd-419e-8674-11dc71b75743",
            "compositeImage": {
                "id": "9a529316-3952-4e37-b54e-49d781fbf308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c51a606-873a-4f1b-8205-b82cb422b0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e068f43-a6db-4cf7-ba7f-4e98d65a1091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c51a606-873a-4f1b-8205-b82cb422b0cd",
                    "LayerId": "e9f7d8c9-a11e-4b75-a977-3b097c30b6d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e9f7d8c9-a11e-4b75-a977-3b097c30b6d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98681fb7-2cdd-419e-8674-11dc71b75743",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}