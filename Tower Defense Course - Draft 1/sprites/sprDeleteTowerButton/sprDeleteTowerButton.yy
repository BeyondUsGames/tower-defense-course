{
    "id": "fad774cf-eee0-4082-af0f-26fb225a0cac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDeleteTowerButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "579e12d1-41a1-4111-93ca-eb0c465eb4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fad774cf-eee0-4082-af0f-26fb225a0cac",
            "compositeImage": {
                "id": "7af4155d-5242-41fa-b244-36c62d20c95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579e12d1-41a1-4111-93ca-eb0c465eb4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b38ff68-5ac2-487b-b9d8-fb726fdbca91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579e12d1-41a1-4111-93ca-eb0c465eb4c3",
                    "LayerId": "229c39fe-a7d3-42d4-a932-81782c2cf174"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "229c39fe-a7d3-42d4-a932-81782c2cf174",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fad774cf-eee0-4082-af0f-26fb225a0cac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 23,
    "yorig": 24
}