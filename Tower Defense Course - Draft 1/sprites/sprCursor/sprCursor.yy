{
    "id": "62a655ba-6e7c-47d7-81cd-414f11d84aec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2aacc80-ea71-4835-a6d9-8d7531ad25a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62a655ba-6e7c-47d7-81cd-414f11d84aec",
            "compositeImage": {
                "id": "e5cfc12d-2b3a-4c44-a641-69e7ad421b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2aacc80-ea71-4835-a6d9-8d7531ad25a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "502c5b77-46ca-4fc8-b645-48c417da6981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2aacc80-ea71-4835-a6d9-8d7531ad25a8",
                    "LayerId": "c02f9ec3-a28f-40bf-bddb-3b6fae91adbc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c02f9ec3-a28f-40bf-bddb-3b6fae91adbc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62a655ba-6e7c-47d7-81cd-414f11d84aec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}