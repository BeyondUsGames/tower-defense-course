{
    "id": "03e81491-59c8-45a4-b959-13a4b05211d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPauseButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e6aba8e-d157-45e8-a02b-e8b766acb475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e81491-59c8-45a4-b959-13a4b05211d8",
            "compositeImage": {
                "id": "15312d73-6726-43e6-88ad-3b37916ab1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e6aba8e-d157-45e8-a02b-e8b766acb475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4b96496-a1fa-471d-9fad-27d64c20f6dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e6aba8e-d157-45e8-a02b-e8b766acb475",
                    "LayerId": "e836ba60-12d5-4700-a94b-98e7691865a7"
                }
            ]
        },
        {
            "id": "147480a6-3cb0-49f3-be81-83afa2798efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e81491-59c8-45a4-b959-13a4b05211d8",
            "compositeImage": {
                "id": "aa8b56b6-2060-4cb2-9047-ccbd2890068c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147480a6-3cb0-49f3-be81-83afa2798efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e60440-0a22-4a5f-baad-bd2013bb741f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147480a6-3cb0-49f3-be81-83afa2798efe",
                    "LayerId": "e836ba60-12d5-4700-a94b-98e7691865a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "e836ba60-12d5-4700-a94b-98e7691865a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03e81491-59c8-45a4-b959-13a4b05211d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}