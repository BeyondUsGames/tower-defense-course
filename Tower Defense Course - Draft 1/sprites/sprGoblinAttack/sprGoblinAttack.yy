{
    "id": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGoblinAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 226,
    "bbox_left": 0,
    "bbox_right": 198,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ca4336d-68ab-46ed-910c-f0f0d08f22ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "c23c4064-6d9e-4f26-a4d3-a24ce6ecec98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca4336d-68ab-46ed-910c-f0f0d08f22ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7cd32e1-9f3a-4333-bb3f-9dcc529ca242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca4336d-68ab-46ed-910c-f0f0d08f22ab",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "74f6b8f1-9bbb-4bc9-8960-83517305716c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "6484d709-e70e-4d9e-b284-ab28f4790d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f6b8f1-9bbb-4bc9-8960-83517305716c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18936636-f9de-40b4-95ba-6a51e8716178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f6b8f1-9bbb-4bc9-8960-83517305716c",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "a13c2094-8e9f-4f95-9290-102e41dc25b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "0fbd20c6-7d55-483c-b0db-6b605a3ada06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a13c2094-8e9f-4f95-9290-102e41dc25b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb0e3dc2-b270-4c16-a22f-b207a1cebdd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a13c2094-8e9f-4f95-9290-102e41dc25b6",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "8fd8635c-87d7-4fd6-aac9-f1c59e38e339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "3f6d6d4b-347d-4807-8fe7-bbaa69c200d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd8635c-87d7-4fd6-aac9-f1c59e38e339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b2b8b88-1983-4e73-a46d-8e61a4c2a042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd8635c-87d7-4fd6-aac9-f1c59e38e339",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "d03908c5-7ec2-444a-aa67-d91c39cb8b79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "53959218-8998-432a-8eba-99c9c5024779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03908c5-7ec2-444a-aa67-d91c39cb8b79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ccdc45-6b37-4397-905c-ad499adb4e74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03908c5-7ec2-444a-aa67-d91c39cb8b79",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "91f27aca-5f62-4d2d-ae27-4a1ad09486c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "b640d365-e4d4-450f-b75a-7b86a59c89ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91f27aca-5f62-4d2d-ae27-4a1ad09486c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60af4327-972f-449b-9db6-eb8802232029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91f27aca-5f62-4d2d-ae27-4a1ad09486c9",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "34349a1a-a348-4025-a97a-ac51f8afec3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "aca6cff2-19fe-4437-8c56-80022b4d3eea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34349a1a-a348-4025-a97a-ac51f8afec3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bff34308-afe6-4723-bab4-7cfce1de8aa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34349a1a-a348-4025-a97a-ac51f8afec3d",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "a695fd5f-6edf-452a-b3a2-8d07dc3e5137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "63d910a9-977b-4d7b-9224-58cd761b03f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a695fd5f-6edf-452a-b3a2-8d07dc3e5137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4e588d-ba56-4240-b248-a0b49a10d434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a695fd5f-6edf-452a-b3a2-8d07dc3e5137",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "0a6bdc9b-53b0-46b5-9595-9a47e7cd0764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "db24f9d2-89c1-4603-aa26-34be360cca46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a6bdc9b-53b0-46b5-9595-9a47e7cd0764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f5810a0-3be0-4acb-8400-4c2e947e06d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a6bdc9b-53b0-46b5-9595-9a47e7cd0764",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "707f6cf7-7ce3-4ec7-9951-159a7f3c26da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "17ca0a72-93f2-49f6-9d0c-46140298a5a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707f6cf7-7ce3-4ec7-9951-159a7f3c26da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df9e6403-b17f-43ac-a59c-b6e25ff3e071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707f6cf7-7ce3-4ec7-9951-159a7f3c26da",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "1ae3f6d9-f1c2-42dc-98cd-3b17c16d570f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "7aa8c5d8-1ef3-4487-8dbb-a6072b00a5be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ae3f6d9-f1c2-42dc-98cd-3b17c16d570f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "345adbbe-0628-43bf-a681-aa1aacdc9309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ae3f6d9-f1c2-42dc-98cd-3b17c16d570f",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "bf93ff16-dfc2-4c9f-94e6-1a191bd9ea39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "0ab4ff6e-43ec-4554-9225-0f39afc991e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf93ff16-dfc2-4c9f-94e6-1a191bd9ea39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9780b8af-4c55-4e2f-8a7e-d7341b9c4122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf93ff16-dfc2-4c9f-94e6-1a191bd9ea39",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "d2efa4d8-f6ad-4c44-a456-694e7b343333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "f8a88951-b93a-4170-8acb-6378403e5d33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2efa4d8-f6ad-4c44-a456-694e7b343333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbdd827-b24d-4bed-bbe3-deff683981ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2efa4d8-f6ad-4c44-a456-694e7b343333",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "e1a9267b-373e-486b-bd5b-3a329936f362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "6360ee99-3cd7-466d-b0a9-1be974e688ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1a9267b-373e-486b-bd5b-3a329936f362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10cbe026-1a77-4df8-9e79-929e859f9dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1a9267b-373e-486b-bd5b-3a329936f362",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "18fa53be-dc63-4e60-b877-9334140da4ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "c912e4a6-834a-4af7-a6f4-7921302df08f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18fa53be-dc63-4e60-b877-9334140da4ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4099f1b9-1cc0-470e-8643-ec3da23ae708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18fa53be-dc63-4e60-b877-9334140da4ca",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "a185180e-19e8-4cd5-b754-e96a31c00e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "0efc2d42-3e0e-46fe-99c2-9349a0179374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a185180e-19e8-4cd5-b754-e96a31c00e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89185f1-15b0-4d42-9cfe-e44abe304e31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a185180e-19e8-4cd5-b754-e96a31c00e8e",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "292c6bcd-3ee9-42d1-b139-5638a0da3491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "9b023696-4cfd-4152-ab76-455746bb047d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "292c6bcd-3ee9-42d1-b139-5638a0da3491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc19787f-2176-444c-93db-d2a98e719798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292c6bcd-3ee9-42d1-b139-5638a0da3491",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "934f5ea1-52be-4fb8-b674-c95ba1e9cd5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "77f70d64-cadc-4cca-8319-384ccbedbf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934f5ea1-52be-4fb8-b674-c95ba1e9cd5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c84cdb8-edc4-45f9-80ed-7df99783430a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934f5ea1-52be-4fb8-b674-c95ba1e9cd5c",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "655fb1a5-b3fd-4e35-92e4-62fbfa86935a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "1271cf8b-6cd9-4924-8afa-e0e17c742ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "655fb1a5-b3fd-4e35-92e4-62fbfa86935a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0676e4-adb9-4fc0-b71a-15e3242c6256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "655fb1a5-b3fd-4e35-92e4-62fbfa86935a",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        },
        {
            "id": "c0d5c439-57cd-4ed7-9222-2fc14413ec06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "compositeImage": {
                "id": "cf7bae6f-5f06-4cdd-a863-9df19129ed42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d5c439-57cd-4ed7-9222-2fc14413ec06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b249f29e-cda9-4f24-9528-f9829df8aac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d5c439-57cd-4ed7-9222-2fc14413ec06",
                    "LayerId": "c712af63-cb13-4ba4-bdef-e343d8fe727a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 227,
    "layers": [
        {
            "id": "c712af63-cb13-4ba4-bdef-e343d8fe727a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60d4bc18-038b-4ad0-9472-3e362b9f3de6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 35,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 199,
    "xorig": 99,
    "yorig": 113
}