{
    "id": "360a69aa-3dd3-41b3-bfef-6b3480dcfd77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRestDataButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 227,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "787c3fc5-35dd-4644-ac90-983f85bd1a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "360a69aa-3dd3-41b3-bfef-6b3480dcfd77",
            "compositeImage": {
                "id": "bc21c931-5906-4eb5-a846-498690c93645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "787c3fc5-35dd-4644-ac90-983f85bd1a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26819046-4ec6-4904-a52d-7ce5934ed77d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "787c3fc5-35dd-4644-ac90-983f85bd1a78",
                    "LayerId": "2cdac671-929d-4714-b2c5-baae04eb5d9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "2cdac671-929d-4714-b2c5-baae04eb5d9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "360a69aa-3dd3-41b3-bfef-6b3480dcfd77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 228,
    "xorig": 114,
    "yorig": 17
}