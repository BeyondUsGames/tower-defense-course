{
    "id": "0251e049-be1a-485b-b502-df76aabcb144",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSludge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29033676-b006-418e-a1f6-d705246c7ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0251e049-be1a-485b-b502-df76aabcb144",
            "compositeImage": {
                "id": "77982ea1-a969-4b7c-914a-ee4208e62a9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29033676-b006-418e-a1f6-d705246c7ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff2daca-c3ce-410b-8c44-f7177254803c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29033676-b006-418e-a1f6-d705246c7ca9",
                    "LayerId": "7b27f1d0-6bbd-411e-ae48-17188da394ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7b27f1d0-6bbd-411e-ae48-17188da394ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0251e049-be1a-485b-b502-df76aabcb144",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}