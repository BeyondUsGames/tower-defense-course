{
    "id": "1d325d57-63a3-4a7e-a1d1-323ab1cc18ac",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntLevel",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Chiller",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "793ed054-30c0-4d7c-ba87-91337d8febd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f560b57f-d2ac-45df-a861-6a1f29f1acf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 36,
                "y": 95
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d2902ed5-a016-4ce3-bcaa-c62ae8c61c8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 29,
                "y": 95
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "625f10ea-fff1-443a-bd5f-2079d2672221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e268994a-e360-4691-9e55-750db3048e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 64
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b5e312b6-04b9-48b1-a012-8a2e262e1f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 205,
                "y": 64
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "95e20b38-ea8c-4b30-b1ee-48ac904df8f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 191,
                "y": 64
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8da71088-e401-41a3-8720-a8f038e85a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 186,
                "y": 64
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4e42355e-f238-4c00-9ba2-cb6e7483bd19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 176,
                "y": 64
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "78c626d8-d75c-4608-a2f4-7eb98a067703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 64
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2ed44cc8-3a69-4738-88a1-62f6d1c3a2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 42,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "213de336-cc4b-425f-9f90-2dff25541148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 151,
                "y": 64
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "424fd5c8-fdd0-4b27-a32b-65e4f0671448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 132,
                "y": 64
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3c0bb001-7025-4e7f-9647-e3e362a6eb0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 121,
                "y": 64
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6fcc3593-7c87-4a77-b81c-2d66955e1082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 115,
                "y": 64
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e10500b6-f902-456e-ad09-899c8c13d5b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 101,
                "y": 64
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d326a79d-358a-42bf-8d85-24afbc5075c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 64
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4dc89558-4d8d-44dd-9cc1-5585e0203fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 79,
                "y": 64
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "baa11a80-6bac-4f10-9372-2729a5534757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 67,
                "y": 64
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "52641e95-7833-4354-b7fa-8646796b34f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "861a5c4b-91b6-4ab7-9c55-e05219d87742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 41,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fcf59c13-ed88-436c-984b-9a12ad0cfe6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 138,
                "y": 64
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a29e0c1b-ecc5-44c8-bd05-c38f841a6a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5153edd0-e555-4f6c-96cc-90d9551f3875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 64,
                "y": 95
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "88db2894-cf77-4a16-8bac-964d63a5874e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 77,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0611e831-a796-47df-9f7c-d0c2d41a59b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 88,
                "y": 126
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "fac49f3a-10c0-4695-8b53-1fab665119e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 82,
                "y": 126
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "eecccd3a-19fd-486c-8ead-5a5b14d42be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 76,
                "y": 126
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a5a9ede7-41e7-4457-bed6-2492ed1d2eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 126
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "28a9c786-e0a4-4b03-b641-4c87bf55c039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 126
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1a98a486-0483-499f-ab5f-811fbb843ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 42,
                "y": 126
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "dae577cb-0593-4f2a-af2d-316a6fb2c46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 126
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "03db26ad-c62b-4902-a0d8-71d1a816fc1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 16,
                "y": 126
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "438b8607-627b-413d-b925-c035a2dcf37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "10bc2595-a954-46bc-826a-91867279966f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 230,
                "y": 95
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ba0e4254-6526-4696-b55c-19505ec84403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 216,
                "y": 95
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b919ef97-8925-4b71-9e6d-6a7c5c84bd42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 95
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f7e8a27d-4128-4c34-9661-14f93a485910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 191,
                "y": 95
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "479d31bd-8f1e-4e2c-8b18-ac8f8aaf031b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 180,
                "y": 95
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4f89043f-bda0-4c9f-8abc-0b2c9d5fb117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 169,
                "y": 95
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3a620dcf-3677-4c25-83c5-6651b3859c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 158,
                "y": 95
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3b88c814-2445-4803-b9eb-4b7eeb8b967f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 146,
                "y": 95
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f815e969-9c1b-44f5-9aad-52d769c700d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 13,
                "x": 131,
                "y": 95
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6d058916-6281-462a-8c0e-d8b4f11ff797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 118,
                "y": 95
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3c5c3388-c613-4b97-9864-3b927cfe243e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 106,
                "y": 95
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5897597f-1c3a-4f60-be03-bb39779b19ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 91,
                "y": 95
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "25275882-1175-4174-8e20-d8b0114734be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 27,
                "y": 64
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7d2145a1-7626-401a-9180-9a9564af1010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 14,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8e010dd0-f113-4ce4-83ed-9a08594c8a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ab95dfd2-6c7a-407f-869b-23e1ac88ab24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 15,
                "x": 15,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "27bdcea5-f736-4fe1-8bec-e31b59ee956d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "be1c4397-ba62-49cc-9983-1937ec7405c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b0f95b9a-c325-4c65-9c6e-625a780d8654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "52553f50-877f-41af-8a86-5fc7755b610b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "74e93caa-0ac1-4c82-b071-bb693ac9d384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ae6c509b-d319-40e7-9bd1-2dd56bc5cd7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5d5dda5b-9118-4de4-9502-a5b064a6cf20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c132d60a-240f-45d3-a219-32e851798d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 12,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "1ac08659-0c75-40e3-a6d3-db023651d313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8b445416-e0c3-4db8-a325-66f39f4aa7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b86d7d34-3c38-4559-9987-ab4f0bdead08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7e3dbc9c-2f43-47fc-b02c-f081efba4ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "085d736f-dd15-4a90-99e4-3a6cfedefc17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9ea6bf78-f0ac-4acb-ba10-44cf6669b065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cfb3d3b3-d36a-43bf-b83e-e57f5e7475ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "13cc00b7-9f53-4b1b-aa87-14aaf56fd4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5e6b9ef7-6ba2-4279-b2d0-52105d9af01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7e690a8c-bdd5-4dd0-b62d-7f3283d07f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a6247da8-7b41-4c92-bb59-0ce26113e583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c01d1342-06c9-4a19-87d3-2b6a1cadcd05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0cbf9494-68e5-4389-9562-6bf2c8551c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "64b65946-94cb-4459-b57f-a3ba694b3a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 32,
                "y": 33
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "60310a1a-5423-4b9a-b4c8-8be5c9728966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 135,
                "y": 33
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e1df6cb3-fd37-441f-8d39-ea4f56019533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 33
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "37fbc332-37c7-4afc-ae19-d1a0770331ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": -3,
                "shift": 5,
                "w": 8,
                "x": 231,
                "y": 33
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e7c2d741-31d0-40da-a8e4-8d9d906d3c07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 221,
                "y": 33
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "162795f1-bdaa-4100-a1ab-59de9e3a2484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 215,
                "y": 33
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f5ba86ae-73fe-467a-8bf4-303eba27568c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 202,
                "y": 33
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f908b7f1-8a47-41e2-8e05-46e590186577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 192,
                "y": 33
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b7a020c2-4f58-4f35-bc10-4f2d8cff292a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 183,
                "y": 33
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b12cb62e-e1b2-4b5e-9b2d-093c8f710506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 173,
                "y": 33
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6ed808f3-d2cb-412c-b874-de3187eff07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 163,
                "y": 33
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9eb92ecf-3bf9-40af-a80f-102dcf36a6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 155,
                "y": 33
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6b9143d0-f4d6-4e5d-90c1-b6c00a4d99be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 241,
                "y": 33
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "410b8001-a1f3-4285-b833-97eb265eeebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 145,
                "y": 33
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3538b73c-9872-40db-9e48-23270d3526d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 125,
                "y": 33
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1ff5e570-ff25-487e-ac2a-d9526dfdd006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 116,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "309bf646-a629-4c3c-844c-a42e5246dc08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 33
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0e482ccc-0240-48f4-892e-5e33faf0dff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "202e962c-b11d-4b40-8885-d98073078cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 85,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b8286c89-6e41-426a-b752-c91dbaa073ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 33
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "31e7ff33-e16c-4e50-b74e-d990bef0c36d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 63,
                "y": 33
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "48e5d32d-4ecf-429d-9182-cb6b87385d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 3,
                "shift": 8,
                "w": 6,
                "x": 55,
                "y": 33
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "766dce86-b777-4737-9f56-5fd0a5a439d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 46,
                "y": 33
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "17ae91ad-a260-42f5-96a6-e7bfcb71ce56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 101,
                "y": 126
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "71a57511-e76b-4916-8f20-340666666e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 29,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 113,
                "y": 126
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}