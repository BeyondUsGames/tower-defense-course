{
    "id": "2c3e49cb-bbf0-4add-9d26-bf8f8600761b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fntBigText",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c8056cf4-d5a3-4a6f-8c56-f407085906b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 47,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "40b8d021-ba1f-494a-a6d7-6cdd3557177f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 47,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 118,
                "y": 100
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5ae9d2ae-12ed-45fc-be10-b9cff5a7bc94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 104,
                "y": 100
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "02068b2d-9d61-4755-a852-42243eb95482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 83,
                "y": 100
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a05f2e30-bb4e-400d-9c5d-c1e033ac5937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 69,
                "y": 100
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "42d14260-1024-4fc1-84f9-f8fe5a3b1bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 41,
                "y": 100
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4b218c28-4c17-4de2-83b9-76a526e80d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 21,
                "y": 100
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "63aab0f8-a1a6-4dc1-98c3-23053bb11932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 47,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 13,
                "y": 100
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b3374a25-2f85-49f1-9e7e-562fe66ba8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 2,
                "y": 100
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cd197ca3-80e7-4e52-9eb3-cc454107c6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 495,
                "y": 51
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "06807e79-e120-4871-8437-0e8cceab7ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 128,
                "y": 100
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "868aa129-3353-4966-84df-f3172153e9b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 476,
                "y": 51
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "df682acb-80a9-4045-9b29-1b5f6ffdbfb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 450,
                "y": 51
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e5dbfad2-884e-4722-a3d4-2fd4b25b6699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 440,
                "y": 51
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "575eeca9-626e-4c5d-b45e-80f590db4ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 430,
                "y": 51
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f4d08ce3-b8a2-42f5-95ee-c730b6beafb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 418,
                "y": 51
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "91e9adcb-f482-4710-a4be-14e7433ccd8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 51
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8b562929-06c0-41cf-a81c-daced59548a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 47,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 391,
                "y": 51
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ad10d0dd-982d-45a4-9df0-e421f8c35f85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 373,
                "y": 51
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6f8aa3c6-73fa-4ed3-8bfb-14e05f07ffd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 359,
                "y": 51
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a2a385d5-943e-46c2-8b4f-ba46d912c1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 341,
                "y": 51
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f8528295-0191-4747-b42f-e1061588726b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 460,
                "y": 51
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6b3cde0c-1b67-4bc8-9760-161c349fc2f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 142,
                "y": 100
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "35d630b1-39ac-48c2-8ea9-65e54382df8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 162,
                "y": 100
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "282402c2-e309-460e-9736-cf107770b9cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 180,
                "y": 100
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c5b870e7-535e-4a64-9754-9734d1d14499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 57,
                "y": 149
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "78b3222c-9d9b-4217-975c-4ef84479a220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 47,
                "y": 149
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b11a7be3-5183-49e0-8e50-8752f9d6d5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 37,
                "y": 149
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4f51ef65-00b7-40eb-b933-e612c4c67023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 21,
                "y": 149
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7a27e2d6-c735-44d4-bb69-72b3a2020885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bd235e62-2ff4-466a-a898-8815887bc2e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 478,
                "y": 100
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4ece29d0-7889-4335-a016-ff91b3cf79b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 461,
                "y": 100
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b9f7395a-fbe9-480f-b59e-07ef190b6fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 47,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 437,
                "y": 100
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "215a9c93-50f0-46b5-a2c5-2b4625807696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 418,
                "y": 100
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7dd1e33d-e295-4bc8-920e-9dd243272067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 100
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2b958895-6198-48a1-8bb8-fb110018f3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 374,
                "y": 100
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2e7d284f-f8bb-4e38-be20-2c7456de7d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 351,
                "y": 100
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0afc313b-f8c4-4a8e-a216-d28881eb3a45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 336,
                "y": 100
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "30ac270d-ba9e-4359-b3a5-4fa1b6e44c2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 318,
                "y": 100
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "635e2046-eaf1-44b5-8fa6-5b95c35ad027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 47,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 296,
                "y": 100
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "33adef9b-6391-47b6-acb7-84b39a101a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 47,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 278,
                "y": 100
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "50a7bb4e-9feb-4faf-93ca-7c17098238fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 47,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 269,
                "y": 100
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a097bea3-2513-412f-a7df-f7da6f6183da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 47,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 257,
                "y": 100
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "573988cf-cabc-4fec-b1cb-a2c54f88c1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 236,
                "y": 100
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5f81d5ce-1026-4ef2-822d-2562f463f785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 224,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "29e27b1a-b1ab-416e-8052-eb5f104fb1fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 198,
                "y": 100
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6bd3f28c-c538-4027-a77e-c6c03970ad86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 322,
                "y": 51
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e2e6423c-df09-4a8e-a240-2e7e7d1cdc4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 296,
                "y": 51
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0a24834e-43be-4b35-a710-2e68e6c78aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 277,
                "y": 51
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "dc7cc14d-f25e-4932-b4c6-77add31d3cf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8231e7a2-e2b2-4861-b7fa-abc13efdcfb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 356,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "937cc247-5270-49a4-88d2-6d1fd1cb5e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 47,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bce9c53d-6a68-450a-b810-cf58c984ffbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f9ce9bcb-fdcf-4170-a71a-dbe4c14ec562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2577c685-f07e-4836-b5af-41f1e66e909c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 47,
                "offset": -2,
                "shift": 17,
                "w": 20,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "10e556ac-a097-4b2e-820e-12bd3e14b14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "361945ae-c9cf-49dc-b6a5-880836e950ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "411334fc-69d3-4a58-8047-84d957690a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "53a5230c-52cc-4f6b-9bd9-db5f4594235c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 47,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3600d254-7362-4ad9-857d-c35fbe5224a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "be44ffdf-7b73-4a79-bfc6-794f9cd1caf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "603ac21c-ba8d-4d78-b03c-ac9e0a7d698a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f092f57b-215c-4bbf-9255-406f2b38f5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6cbb0d4f-7585-4c44-aa61-1eb1fea54863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6b609a99-60eb-4a80-ba05-9e923f59831e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 47,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dc50480e-ac9b-4ef1-a1e1-002cf41f8f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d5df670e-bd80-4e5f-a381-74113b0b3b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7543f9cf-64ac-4c45-88d9-31f9db87b373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e3af9fd3-55ad-439b-bbc3-8889a51ae699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "218ec9b8-baa5-4a9f-b353-cd6d3462aea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ca6f8c67-98b3-4229-86e1-7d2a1788ebae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2357b53e-665e-4b62-a8a5-e1f798f4c052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2400d9ff-1cda-42b0-a9d1-7079c2f4c3a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 79,
                "y": 51
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f515fa6f-40e0-4bf2-a6b6-55e0fc03e8ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d9218e2d-7cd4-4b1d-b170-2b8735203f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 47,
                "offset": -3,
                "shift": 11,
                "w": 13,
                "x": 248,
                "y": 51
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "55e92098-2001-4950-a058-510435ca5171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 229,
                "y": 51
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "97a10347-da89-4c5e-9b03-a046ded2ab48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 220,
                "y": 51
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ad3c09a1-15f1-4f89-9837-dd745686fbb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 195,
                "y": 51
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c56fae49-b36b-42db-9b20-e67e276982f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 178,
                "y": 51
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6b7e8e44-6fb5-4df6-9c1a-42f4c95db110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 158,
                "y": 51
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "176af241-64e4-4606-b2fb-80c487c11496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 139,
                "y": 51
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c4e10916-39d2-494d-9c13-3bc0f2ed5450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 119,
                "y": 51
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c2242d53-ccbe-4914-acf5-dfb6e7411e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 51
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "96e0c701-0c07-45a1-8d91-bffa929865a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 47,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 263,
                "y": 51
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "201379ba-4f93-4526-bd9d-a4421e8c941c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 96,
                "y": 51
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "59245d57-a71c-472d-ba3c-50fbb973e65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 62,
                "y": 51
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c18709a2-4ab3-460f-8d52-c7f24fbb2632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 47,
                "offset": -2,
                "shift": 12,
                "w": 16,
                "x": 44,
                "y": 51
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5a0b15fe-778a-439b-a369-d299ddb047f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 19,
                "y": 51
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ed126add-341e-45e1-9019-b05024af3795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 51
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "87488cb8-a0cc-4e0c-9b07-be8ef0dcab36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "dadfae17-af51-418e-9597-201d53cafd40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a4cb5572-2e7d-42a0-9260-940d54045426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "258d8b68-8e7a-4d77-8a71-0858648a275e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 47,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f72a3549-9576-4570-822e-7a91c859433d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4ec1942e-9e55-4db4-b8ab-11fce1aa4c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 77,
                "y": 149
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "344fe8f2-0fba-424f-ab95-f921bebe0695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 47,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 96,
                "y": 149
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}