///Change to a new room with a fancy transition
/// @description Transition to a new room
/// @arg NewRoom Room to switch to
newRoom = argument0;

changer = instance_create_depth(x, y, depth - 1000, objTransitionQuarters);
changer.newRoom = newRoom;