////Shake the screen a certain amount and for a certain time
/// @description Shake the screen
/// @param amount The intensity of the shake
/// @param length How long to last
screenShakeAmount = argument0;
screenShakeLength = argument1;

shaker = instance_create_depth(0, 0, depth, objScreenShake);
shaker.shakeAmount = screenShakeAmount;
shaker.alarm[0] = screenShakeLength * 60;