///Draw the level info
draw_set_alpha(1);
draw_set_colour(c_dkgray);
draw_set_halign(fa_center);
draw_set_font(fntBigText);

//Current Level
draw_text(room_width - sprite_get_width(sprLevelInfoFrame) / 2 - 16, room_height - 180, "Level " + string(objLevelParent.myLevelNumber));
draw_line(room_width - sprite_get_width(sprLevelInfoFrame) / 2 - 80, room_height - 140, room_width - sprite_get_width(sprLevelInfoFrame) / 2 + 50, room_height - 140);

//Monsters spawned
draw_text(room_width - sprite_get_width(sprLevelInfoFrame) + 100, room_height - 125, "Spawned: " + 
string(spawnCount) + "\\" + string(maxMonsters));

//Draw time to start
draw_set_halign(fa_left);
if(spawnCount == 0) {
	draw_text(room_width / 2 - 128, room_height / 2 - 32, "Level Will Begin In: ");
	draw_text(room_width / 2 + string_width("Level Will Begin In: ") - 128, room_height / 2 - 32, alarm[0] / 60);
}

draw_set_halign(fa_center);
if(spawnCount == maxMonsters && !instance_exists(objEnemyParent)) {
	draw_text(room_width / 2, room_height / 2, "Congratz, you beat the level!");
}