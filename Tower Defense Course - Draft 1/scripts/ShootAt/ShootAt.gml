///Return the x or y location to shoot at
/// @description Get value of where enemy will be
/// @param x_or_y Which value to return
type = argument0;

if(type == "x")
	return path_get_x(other.nearestEnemy.path_index, other.nearestEnemy.path_position + other.nearestEnemy.mySpeed / 250);
else if(type == "y")
	return path_get_y(other.nearestEnemy.path_index, other.nearestEnemy.path_position + other.nearestEnemy.mySpeed / 250);
else
	return undefined;