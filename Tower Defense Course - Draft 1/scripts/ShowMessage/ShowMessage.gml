///Take input and show it in a dialogue box, pausing the game
message = argument0;

box = instance_create_depth(room_width / 2, room_height - sprite_get_height(sprTextBox), depth - 100, objTextBox);
with(box) {
	message = other.message;
}