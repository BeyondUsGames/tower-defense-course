//Draw turret options
draw_set_alpha(1);
draw_set_font(fntBigText);
draw_set_halign(fa_center);
for(var i = 0; i < array_height_2d(global.AvailableTurrets); ++i) {
	if(objPlayer.gold < global.AvailableTurrets[i, TowerCost])
		draw_set_colour(c_red);
	else
		draw_set_colour(make_color_rgb(255, 212, 0));
	//Draw their cost
	draw_text((i * 112) + 128, room_height - 170, "$" + string(global.AvailableTurrets[i, TowerCost]));
	//Draw the border
	//Active border
	if(i == currentTower)
		draw_sprite_ext(sprFrameActive, 0, (i * 112) + 128, room_height - 80, 1.5, 1.5, 0, c_white, 1);
	else
		draw_sprite_ext(sprFrame, 0, (i * 112) + 128, room_height - 80, 1.5, 1.5, 0, c_white, 1);
	//Draw the tower & base
	draw_sprite_ext(sprTowerBase, 0,(i * 112) + 128, room_height - 80, .3, .3, 0, c_white, 1);
	draw_sprite_ext(global.AvailableTurrets[i, TowerSprite], 0,(i * 112) + 128, room_height - 80, .3, .3, 0, c_white, 1);
}