////Define Macros
//Available Towers Properites
#macro TowerSprite 0
#macro TowerObject 1
#macro TowerCost 2

//Tower Order
#macro TurretTower 0
#macro SludgeTower 1
#macro RocketTower 2

//Tower Costs
#macro TurretLevel1Cost 25
#macro RocketLevel1Cost 50
#macro SludgeLevel1Cost 35

//Saving and Loading
#macro SaveFileName "TD_Data.ini"
#macro Level "Level"
#macro StarRating "Stars"
#macro KillCount "Total Enemies Killed"

//Tower Upgrades
#macro TowerLevel1 0
#macro TowerLevel2 1
#macro TowerLevel3 2
#macro TowerLevel4 3

#macro FireRate 0
#macro Radius 1
#macro Damage 2
#macro UpgradeCost 3

#macro MaxedOut "Maxed Out"