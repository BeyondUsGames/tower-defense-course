//Saves the players star rating for that level
var currentLevel = Level + string(objLevelParent.myLevelNumber);

ini_open(SaveFileName);
//Star Rating
ini_write_real(currentLevel, StarRating, objLevelParent.starRating);
//Kill Count
var prevKills = ini_read_real(currentLevel, KillCount, 0);
ini_write_real(currentLevel, KillCount, objLevelParent.enemiesKilled + prevKills);
ini_close();