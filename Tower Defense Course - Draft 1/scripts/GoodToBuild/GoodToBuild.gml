///Check to see if a turret can be built here
//Check Tile collisions
var rightCollision, topCollision, leftCollision, bottomCollision;
rightCollision = tilemap_get_at_pixel(enemyGround, bbox_right, y);
topCollision = tilemap_get_at_pixel(enemyGround, x, bbox_top);
leftCollision = tilemap_get_at_pixel(enemyGround, bbox_left, y);
bottomCollision = tilemap_get_at_pixel(enemyGround, x, bbox_bottom);

if(max(rightCollision, topCollision, leftCollision, bottomCollision) > 0)
	return false;

//Check other turret collisions
var otherTurret = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, objTowerParent, false, true);
if(otherTurret != -4)
	return false;

//Check cash of player
if(objPlayer.gold < global.AvailableTurrets[currentTower, TowerCost])
	return false;

//Check if below line
if(y > 760)
	return false;

return true;