{
    "id": "df72ca00-a5e5-450d-ad4b-3789605e40b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemyAirParent",
    "eventList": [
        {
            "id": "a466a26f-67a0-4365-ad95-f3f51a2a2edb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df72ca00-a5e5-450d-ad4b-3789605e40b0"
        },
        {
            "id": "06582947-611f-4c23-a8e2-ad96c948f8cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df72ca00-a5e5-450d-ad4b-3789605e40b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9e44c05-4276-463b-bc66-71b57c3b7682",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}