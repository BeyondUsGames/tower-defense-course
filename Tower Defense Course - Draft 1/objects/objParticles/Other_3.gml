/// @description Destroy all particles
part_system_destroy(global.PartSystem);
part_type_destroy(global.FlamePart);
part_type_destroy(global.PartExplode);
part_type_destroy(global.PartRocket);
part_type_destroy(global.PartTrail);
part_emitter_destroy(global.PartFireworkSystem, global.PartFireworkEmitter);