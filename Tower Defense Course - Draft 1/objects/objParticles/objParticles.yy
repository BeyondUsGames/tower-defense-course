{
    "id": "92c45d0d-4802-48fa-89c1-6c18c6354ee5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objParticles",
    "eventList": [
        {
            "id": "c3fa0f53-e0dc-4bfc-a381-bd7b5b34f09e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "92c45d0d-4802-48fa-89c1-6c18c6354ee5"
        },
        {
            "id": "265b8be8-c18e-44c9-b1f3-20d0c1ae13d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92c45d0d-4802-48fa-89c1-6c18c6354ee5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}