/// @description Spawn units randomly
enemy = instance_create_layer(0, random_range(32, room_height - 32), layer, allEnemies[random(array_length_1d(allEnemies))]);

alarm[0] = random_range(30, 90);