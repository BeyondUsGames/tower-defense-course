{
    "id": "f227bc7e-2728-405d-bd24-9d8791263046",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objArrow",
    "eventList": [
        {
            "id": "205fc481-aee4-44b6-99c7-c16f39ea2ba3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64b83edf-33c7-4240-ba53-a73a2e59b0ca",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f227bc7e-2728-405d-bd24-9d8791263046"
        },
        {
            "id": "5ac774eb-d238-4c85-bdc0-1f10f649deb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f227bc7e-2728-405d-bd24-9d8791263046"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3dac2dd5-37fb-45a8-aeac-e3e64adacf2d",
    "visible": true
}