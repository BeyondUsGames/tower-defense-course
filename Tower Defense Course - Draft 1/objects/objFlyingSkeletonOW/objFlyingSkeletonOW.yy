{
    "id": "72fc64d6-e949-422a-ae4d-6c9035102826",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFlyingSkeletonOW",
    "eventList": [
        {
            "id": "dc8ceefa-1069-46e8-995f-932783935b57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72fc64d6-e949-422a-ae4d-6c9035102826"
        },
        {
            "id": "f789a11e-69dd-4667-9246-b406ca9e66b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "72fc64d6-e949-422a-ae4d-6c9035102826"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
    "visible": true
}