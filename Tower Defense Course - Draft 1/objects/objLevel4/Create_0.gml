/// @description Variables for level 1
event_inherited();

var player = instance_create_layer(x, y, layer, objPlayer);
with(player) {
	player.maxHealth = 5;
	player.currentHealth = player.maxHealth;
	player.gold = 75;
}
//Monster List for level
enemyList = ds_grid_create(3, 2);
ds_grid_add(enemyList, 0, 0, objGoblin);
ds_grid_add(enemyList, 0, 1, 5);
ds_grid_add(enemyList, 1, 0, objCat);
ds_grid_add(enemyList, 1, 1, 10);
ds_grid_add(enemyList, 2, 0, objScorpion);
ds_grid_add(enemyList, 2, 1, 15);

maxMonsters = ds_grid_get(enemyList, 0, 1) + 
ds_grid_get(enemyList, 1, 1) + 
ds_grid_get(enemyList, 2, 1);
currentEnemy = 0;
spawnRate = 120;
decreaser = 3.5;
myPath = pLevel4;
alarm[0] = spawnRate + 300;

//Saving and Loading
myLevelNumber = 4;

//Reset all the towers
global.AvailableTurrets = 0;
//Define just the ones I want for level
global.AvailableTurrets[0, TowerSprite] = sprTurretLevel1;
global.AvailableTurrets[0, TowerObject] = objTurretTower;
global.AvailableTurrets[0, TowerCost] = TurretLevel1Cost;

global.AvailableTurrets[2, TowerSprite] = sprRocketLevel1;
global.AvailableTurrets[2, TowerObject] = objFireballTower;
global.AvailableTurrets[2, TowerCost] = RocketLevel1Cost;

global.AvailableTurrets[1, TowerSprite] = sprSludgeTower;
global.AvailableTurrets[1, TowerObject] = objSludgeTower;
global.AvailableTurrets[1, TowerCost] = SludgeLevel1Cost;

ShowMessage("If you right click on a tower you've built, you can destroy it and redeem half the gold, or upgrade it to a more powerful version of itself.");