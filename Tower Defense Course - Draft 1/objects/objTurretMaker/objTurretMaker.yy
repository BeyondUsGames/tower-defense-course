{
    "id": "8c92f5d6-1e05-414c-9474-d3b570e30e19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTurretMaker",
    "eventList": [
        {
            "id": "396723fa-fb98-4507-9f43-e6bc603ca834",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8c92f5d6-1e05-414c-9474-d3b570e30e19"
        },
        {
            "id": "eda80e5e-e562-4ffa-9240-d9e1bc82c817",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8c92f5d6-1e05-414c-9474-d3b570e30e19"
        },
        {
            "id": "17c34895-3b7f-4f97-b46d-40a98766f8e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8c92f5d6-1e05-414c-9474-d3b570e30e19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62a655ba-6e7c-47d7-81cd-414f11d84aec",
    "visible": true
}