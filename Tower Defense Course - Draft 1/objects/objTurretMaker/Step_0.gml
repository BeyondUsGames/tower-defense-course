/// @description Move curser around
x = mouse_x;
y = mouse_y;
myGridSpotX = round(mouse_x / gridSize);
myGridSpotY = round(mouse_y / gridSize);

if(mouse_check_button_released(mb_left) && !instance_exists(objTowerOptionsParent)) {
	sprite_index = sprCursor;
	//Align tower to grid
	if(myGridSpotX mod 2 == 0) {
		++myGridSpotX;
		x = myGridSpotX * gridSize;
	}
	if(myGridSpotY mod 2 == 0) {
		++myGridSpotY;
		y = myGridSpotY * gridSize;
	}
	//Check if good to build
	if(GoodToBuild()) {
		//Subtract Gold from player
		objPlayer.gold -= global.AvailableTurrets[currentTower, TowerCost];
		//Create the tower
		instance_create_layer(myGridSpotX * gridSize, myGridSpotY * gridSize, "Instances", global.AvailableTurrets[currentTower, TowerObject]);
	}
}

//Choose between towers
if(array_height_2d(global.AvailableTurrets) > 0 && keyboard_check_pressed(ord("1")))
	currentTower = 0;
if(array_height_2d(global.AvailableTurrets) > 1 && keyboard_check_pressed(ord("2")))
	currentTower = 1;
if(array_height_2d(global.AvailableTurrets) > 2 && keyboard_check_pressed(ord("3")))
	currentTower = 2;
if(array_height_2d(global.AvailableTurrets) > 3 && keyboard_check_pressed(ord("4")))
	currentTower = 3;