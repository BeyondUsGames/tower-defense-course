/// @description Draw
if(mouse_check_button(mb_left) && !instance_exists(objTowerOptionsParent) && y < 760) {
	sprite_index = sprTowerBase;
	image_xscale = .25;
	image_yscale = .25;
	image_alpha = .5;
	//Draw grid along map
	draw_set_alpha(1);
	draw_set_colour(c_black);
	for(var i = 0; i < gridSize; ++i) {
		//Horizontal Lines
		if(i < 13)
			draw_line(0, i * 64, room_width, i * 64);
		//Vertical
		draw_line(i * 64, 0, i * 64, room_height - 192);
	}
	if(!GoodToBuild())
		image_blend = c_red;
	else
		image_blend = c_white;
}
else {
	sprite_index = sprCursor;
	image_alpha = 1;
	image_blend = c_white;
	image_xscale = 1;
	image_yscale = 1;
}

DrawAvailableTowers();

draw_self();