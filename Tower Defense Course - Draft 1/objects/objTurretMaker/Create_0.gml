/// @description Initialize
window_set_cursor(cr_none);
gridSize = 32;
gridWidth = floor(room_width / gridSize);
gridHeight = floor(room_height / gridSize);

//Tile checking
enemyGround = layer_tilemap_get_id("EnemyPath");

drawX = undefined;
drawY = undefined;

currentTower = 0;

depth -= 100;