{
    "id": "ca6740a5-7616-4000-950e-dd95467c50a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFireball",
    "eventList": [
        {
            "id": "528bae0c-7e11-4733-b76d-6aee56c64693",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca6740a5-7616-4000-950e-dd95467c50a3"
        },
        {
            "id": "904f5651-5e97-48ec-87fd-55a4ce2f656f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b9e44c05-4276-463b-bc66-71b57c3b7682",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ca6740a5-7616-4000-950e-dd95467c50a3"
        },
        {
            "id": "236da0ac-722d-4667-98a9-8b5faea073ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ca6740a5-7616-4000-950e-dd95467c50a3"
        },
        {
            "id": "c7a177a5-5b3f-4eaa-9627-cd259e8186b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca6740a5-7616-4000-950e-dd95467c50a3"
        },
        {
            "id": "1db8b276-71d5-4947-b73c-11baf89a6c7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ca6740a5-7616-4000-950e-dd95467c50a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52d5a707-a807-473a-b49c-14ed9e60ecd1",
    "visible": true
}