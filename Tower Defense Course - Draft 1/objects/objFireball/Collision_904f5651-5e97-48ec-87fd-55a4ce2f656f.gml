/// @description Destroy self, damage enemy
--waitToDie;
if(waitToDie <= 0) {
	instance_destroy();
	with(other)
		currentHealth -= other.myDamage;
}