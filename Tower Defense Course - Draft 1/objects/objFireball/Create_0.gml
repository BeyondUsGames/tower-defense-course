/// @description Data
alarm[0] = 1;

flameOffsetX = -18;
flameDirection = point_direction(0, 0, flameOffsetX, 0);
flameLength = point_distance(0, 0, flameOffsetX, 0);

waitToDie = 10;