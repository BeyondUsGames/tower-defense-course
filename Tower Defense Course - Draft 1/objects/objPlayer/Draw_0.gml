/// @description Draw Health & Info
draw_set_alpha(1);
//Hearts
for(var i = 0; i < currentHealth; ++i) {
	draw_sprite(sprHeart, 0, room_width - (sprite_get_width(sprHeart) * maxHealth) + (i * 32) - 64, room_height - 78);
}
//Gold
draw_set_font(fntBigText);
draw_set_colour(make_color_rgb(255, 212, 0));
if(gold <= 0)
	draw_set_colour(c_red);
draw_set_halign(fa_left);
draw_text(room_width - sprite_get_width(sprLevelInfoFrame) - 10, room_height - 80, "$" + string(gold));