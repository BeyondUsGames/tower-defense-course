/// @description Check Health
if(currentHealth <= 0) {
	ShowMessage("You've lost! Try again.");
	++deathTimer;
	if(deathTimer > 120)
		game_restart();
}