/// @description Inherit & Do More
event_inherited();

//Draw rocket slowly reappearing
if(alarm[0] <= 1)
	draw_sprite_ext(sprArrow, 0, x, y, 1, 1, image_angle, c_white, 1);
else
	draw_sprite_ext(sprArrow, 0, x, y, 1, 1, image_angle, c_white, ((fireRate / alarm[0]) / 10));