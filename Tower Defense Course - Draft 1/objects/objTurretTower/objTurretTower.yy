{
    "id": "a600c565-2162-4b0b-89fa-0d3a38d067ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTurretTower",
    "eventList": [
        {
            "id": "b9433210-a86a-430f-83da-1729e4c3f1a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a600c565-2162-4b0b-89fa-0d3a38d067ec"
        },
        {
            "id": "243b3a0e-0a09-4771-912f-eaad96d8a07b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a600c565-2162-4b0b-89fa-0d3a38d067ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0585b55f-97a7-4420-8300-80a66d44d932",
    "visible": true
}