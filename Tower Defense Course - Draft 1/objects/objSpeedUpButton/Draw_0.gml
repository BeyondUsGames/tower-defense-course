/// @description Draw Description & Speed
draw_self();

draw_set_colour(c_black);
draw_set_font(fntLevel);
draw_set_halign(fa_center);

draw_text(x - 32, y + 32, "Current Speed: " + string(game_get_speed(gamespeed_fps)));