{
    "id": "9bb2d5fa-6446-40f3-b2c1-64190da97240",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSpeedUpButton",
    "eventList": [
        {
            "id": "c84fb848-bcbc-47dd-ad0a-f24811255fa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9bb2d5fa-6446-40f3-b2c1-64190da97240"
        },
        {
            "id": "e825d4a9-ffc1-4177-af72-075644cb40c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9bb2d5fa-6446-40f3-b2c1-64190da97240"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "64e16c90-5e54-47a3-88f1-a9ee6f5fd3d5",
    "visible": true
}