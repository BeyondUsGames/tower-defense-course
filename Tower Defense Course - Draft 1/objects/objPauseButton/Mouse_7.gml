/// @description Pause the game
++image_index;
if(isPaused == false) {
	screenshot = sprite_create_from_surface(application_surface, 0, 0, room_width, room_height, false, false, 0, 0);
	instance_deactivate_all(true);
	alarm[0] = 1;
	isPaused = true;
}
else {
	isPaused = false;
	instance_activate_all();
	instance_destroy(objPausedCursor);
	sprite_delete(screenshot);
}