{
    "id": "ca59d471-29a0-43db-aaeb-172a010b418c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPauseButton",
    "eventList": [
        {
            "id": "5331dd11-350e-429a-92d8-e9d6705eee5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ca59d471-29a0-43db-aaeb-172a010b418c"
        },
        {
            "id": "f978659b-6204-473b-ade4-c828241bf211",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca59d471-29a0-43db-aaeb-172a010b418c"
        },
        {
            "id": "a19e4aea-8525-4fec-aa6a-c7ed72c5be97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ca59d471-29a0-43db-aaeb-172a010b418c"
        },
        {
            "id": "82f8a154-811d-4aeb-9143-fa970774f9d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ca59d471-29a0-43db-aaeb-172a010b418c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03e81491-59c8-45a4-b959-13a4b05211d8",
    "visible": true
}