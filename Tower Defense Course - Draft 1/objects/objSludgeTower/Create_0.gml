/// @description Initalize
image_xscale = .25;
image_yscale = .25;

//Level Stats
levelStats[TowerLevel1, FireRate] = 250;
levelStats[TowerLevel1, Radius] = 196;
levelStats[TowerLevel1, Damage] = 0;
levelStats[TowerLevel1, UpgradeCost] = 50;

levelStats[TowerLevel2, FireRate] = 225;
levelStats[TowerLevel2, Radius] = 229;
levelStats[TowerLevel2, Damage] = 0;
levelStats[TowerLevel2, UpgradeCost] = 75;

levelStats[TowerLevel3, FireRate] = 200;
levelStats[TowerLevel3, Radius] = 256;
levelStats[TowerLevel3, Damage] = 0.1;
levelStats[TowerLevel3, UpgradeCost] = MaxedOut;

levelStats[TowerLevel4, FireRate] = MaxedOut;
levelStats[TowerLevel4, Radius] = MaxedOut;
levelStats[TowerLevel4, Damage] = MaxedOut;


currentLevel = 0;

fireRate = levelStats[currentLevel, FireRate];
radius = levelStats[currentLevel, Radius];
damage = levelStats[currentLevel, Damage];

enemyType = objEnemyGroundParent;
myProjectile = objSludge;
mySound = sndSludgeShot;

gunOffsetX = 28;
gunOffsetY = 0;
gunDirection = point_direction(0, 0, gunOffsetX, gunOffsetY);
gunLength = point_distance(0, 0, gunOffsetX, gunOffsetY);
bulletSpawnX = x + lengthdir_x(gunLength, image_angle + gunDirection);
bulletSpawnY = y + lengthdir_y(gunLength, image_angle + gunDirection);