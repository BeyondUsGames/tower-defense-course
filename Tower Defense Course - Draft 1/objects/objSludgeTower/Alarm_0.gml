/// @description Fire at enemy
if(nearestEnemy != -4) {
	audio_play_sound(mySound, 10, false);
	bullet = instance_create_layer(bulletSpawnX, bulletSpawnY, layer, myProjectile);
	with(bullet) {
		speed = 7.5;
		direction = point_direction(x, y, ShootAt("x"), ShootAt("y"));
		image_angle = direction;
		myDamage = other.damage;
		goalX = ShootAt("x");
		goalY = ShootAt("y");
	}
	alarm[0] = fireRate;
}