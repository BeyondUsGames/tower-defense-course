{
    "id": "aa695c91-d2b3-4373-935e-7398402e2f05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSludgeTower",
    "eventList": [
        {
            "id": "4fea6da3-f8a9-4383-a2cc-fd174b3df6b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa695c91-d2b3-4373-935e-7398402e2f05"
        },
        {
            "id": "c6af58c9-486f-4842-afa5-4f52781ee9e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aa695c91-d2b3-4373-935e-7398402e2f05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f9521dea-5ee8-482d-9f01-c5dd27d1bfd8",
    "visible": true
}