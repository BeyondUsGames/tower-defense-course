{
    "id": "b20cead1-47e6-466e-98a2-310302ad6094",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCat",
    "eventList": [
        {
            "id": "9503c2d1-f462-4353-b8d5-09e21e1545ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b20cead1-47e6-466e-98a2-310302ad6094"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "64b83edf-33c7-4240-ba53-a73a2e59b0ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
    "visible": true
}