{
    "id": "f7a8067c-d367-40c4-8079-d786a14cb7fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPausedCursor",
    "eventList": [
        {
            "id": "a76f0bd9-57e1-4381-bbea-7ac48d20eb4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7a8067c-d367-40c4-8079-d786a14cb7fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62a655ba-6e7c-47d7-81cd-414f11d84aec",
    "visible": true
}