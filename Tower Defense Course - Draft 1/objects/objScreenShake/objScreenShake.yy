{
    "id": "aacd0395-8a3a-4127-8e3b-6772da48b2bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objScreenShake",
    "eventList": [
        {
            "id": "2ff40a21-a5df-4f90-a50a-8af72430c08a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aacd0395-8a3a-4127-8e3b-6772da48b2bf"
        },
        {
            "id": "451c53d5-b722-4f9d-bad3-363eb01ae2cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aacd0395-8a3a-4127-8e3b-6772da48b2bf"
        },
        {
            "id": "81c25402-71d1-419b-bd39-4282f89546cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aacd0395-8a3a-4127-8e3b-6772da48b2bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}