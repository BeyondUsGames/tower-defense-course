/// @description Check for audio
if(!audio_is_playing(currentSong)) {
	var temp = currentSong;
	while(temp == currentSong) {
		currentSong = irandom(2);
	}
	audio_play_sound(currentSong, 100, false);
}
