{
    "id": "c132dba4-a499-4308-aaf6-6e598b0b3170",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSystem",
    "eventList": [
        {
            "id": "11f3cf41-fe08-45e5-8067-11bc031d4647",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c132dba4-a499-4308-aaf6-6e598b0b3170"
        },
        {
            "id": "a81d2a66-6aa2-435a-b182-e8631179479a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 123,
            "eventtype": 9,
            "m_owner": "c132dba4-a499-4308-aaf6-6e598b0b3170"
        },
        {
            "id": "f77e00c6-9800-4338-a81a-0040cfdbe118",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c132dba4-a499-4308-aaf6-6e598b0b3170"
        },
        {
            "id": "49d3af64-7805-4bcf-a208-57b3e2b1cc47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c132dba4-a499-4308-aaf6-6e598b0b3170"
        },
        {
            "id": "37bd2a0f-5be6-4bbd-8c53-68950a0b76de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "c132dba4-a499-4308-aaf6-6e598b0b3170"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}