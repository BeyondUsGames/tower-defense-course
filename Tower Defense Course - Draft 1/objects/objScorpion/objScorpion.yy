{
    "id": "b3abe7fb-5d16-4bb1-b93e-8b6b6c7f8ada",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objScorpion",
    "eventList": [
        {
            "id": "aeb4ca74-d17c-42d5-a5f2-6a095c6e259b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b3abe7fb-5d16-4bb1-b93e-8b6b6c7f8ada"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "64b83edf-33c7-4240-ba53-a73a2e59b0ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
    "visible": true
}