{
    "id": "cc14b1b0-a77c-4092-8e4e-9d59062d990f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoblinOW",
    "eventList": [
        {
            "id": "9b093f4b-caa7-4a38-b348-da0b2481da70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc14b1b0-a77c-4092-8e4e-9d59062d990f"
        },
        {
            "id": "cea4b4d6-2521-48fe-8fe3-e7a4246be1fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "cc14b1b0-a77c-4092-8e4e-9d59062d990f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
    "visible": true
}