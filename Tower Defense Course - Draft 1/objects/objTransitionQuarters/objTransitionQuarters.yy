{
    "id": "4875b071-7b2b-4e19-94c3-9bf02cad6093",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTransitionQuarters",
    "eventList": [
        {
            "id": "7415e43b-ca0f-451d-b625-6b3cc4d43c15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4875b071-7b2b-4e19-94c3-9bf02cad6093"
        },
        {
            "id": "1cab1870-8c05-40b8-b1e6-63e98850929d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4875b071-7b2b-4e19-94c3-9bf02cad6093"
        },
        {
            "id": "2f4e6238-e916-4d71-be6a-24dfeac21f4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4875b071-7b2b-4e19-94c3-9bf02cad6093"
        },
        {
            "id": "06a89023-6288-48bc-a353-bae7e650d0c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4875b071-7b2b-4e19-94c3-9bf02cad6093"
        },
        {
            "id": "1b9b76e8-2a49-496d-b970-f5340ff161ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4875b071-7b2b-4e19-94c3-9bf02cad6093"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}