/// @description Destroy self
currentFrame++;

if (currentFrame > maxFrames) {
    instance_destroy(); // The transition has finished so destroy it
}
