/// @description Draw the transitions
if (currentFrame > 0) {

    // draw a fade under everything 
    draw_set_colour(c_black)
    draw_set_alpha(1-EaseInQuad(currentFrame,0,1,maxFrames))
    draw_rectangle(GetLeftX(), GetTopY(), GetRightX(), GetBottomY(), false)
    draw_set_alpha(1)

    // convert the number of frames that have passed into a number between 0 and 90 for the angle
    var rot = EaseOutCubic(currentFrame,0,90,maxFrames)

    draw_sprite_ext(spr_roomtl, 0, GetLeftX(), GetTopY(), 1, 1, rot, c_white, 1)
    draw_sprite_ext(spr_roomtr, 0, GetRightX(), GetTopY(), 1, 1, rot, c_white, 1)
    draw_sprite_ext(spr_roombl, 0, GetLeftX(), GetBottomY(), 1, 1, rot, c_white, 1)
	draw_sprite_ext(spr_roombr, 0, GetRightX(), GetBottomY(), 1, 1, rot, c_white, 1)
}
