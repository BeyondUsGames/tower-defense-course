/// @description Set up transition data
currentFrame = 0;
maxFrames = 160;
persistent = true;

var halfHeight = surface_get_height(application_surface) / 2
var halfWidth = surface_get_width(application_surface) / 2

leftX = GetLeftX();
rightX = GetRightX();
topY = GetTopY();
bottomY = GetBottomY();

spr_roomtl = sprite_create_from_surface(application_surface, 0, 0, halfWidth, halfHeight, false, false, 0, 0);
spr_roomtr = sprite_create_from_surface(application_surface, halfWidth, 0, halfWidth, halfHeight, false, false, halfWidth, 0);
spr_roombl = sprite_create_from_surface(application_surface, 0, halfHeight, halfWidth, halfHeight, false, false, 0, halfHeight);
spr_roombr = sprite_create_from_surface(application_surface, halfWidth, halfHeight, halfWidth, halfHeight, false, false, halfWidth, halfHeight);

//Define the room to change to, and set alarm to do so
newRoom = undefined;
alarm[0] = 1;	