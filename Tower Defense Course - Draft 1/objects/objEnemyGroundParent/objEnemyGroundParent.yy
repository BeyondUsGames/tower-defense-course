{
    "id": "64b83edf-33c7-4240-ba53-a73a2e59b0ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemyGroundParent",
    "eventList": [
        {
            "id": "d00f68f5-0314-4f00-8df3-8351fd61654b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0d5a2fe4-1422-451d-8ff8-78a9608bbe7e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "64b83edf-33c7-4240-ba53-a73a2e59b0ca"
        },
        {
            "id": "32f2bab2-235f-4c31-91fb-527e5ad108f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "64b83edf-33c7-4240-ba53-a73a2e59b0ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9e44c05-4276-463b-bc66-71b57c3b7682",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}