{
    "id": "cb0ed5b9-e060-4d38-b782-f7c658ca7eb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTowerOptionsParent",
    "eventList": [
        {
            "id": "8619d69d-e218-4ae2-9f76-24b638280428",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb0ed5b9-e060-4d38-b782-f7c658ca7eb5"
        },
        {
            "id": "24b02909-42b9-4fa4-b557-0d2357764e8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 57,
            "eventtype": 6,
            "m_owner": "cb0ed5b9-e060-4d38-b782-f7c658ca7eb5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}