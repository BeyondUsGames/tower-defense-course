{
    "id": "63b0764d-7101-488d-bdcb-222d8c20bd84",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFlyingSkeleton",
    "eventList": [
        {
            "id": "4f0d0ea0-c41c-46b0-839b-c2667101e6cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63b0764d-7101-488d-bdcb-222d8c20bd84"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "df72ca00-a5e5-450d-ad4b-3789605e40b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6815a9eb-9a7a-49a2-9f40-b3465ddc2066",
    "visible": true
}