{
    "id": "3d6cac78-7661-45f1-924c-4c5c9b3482f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGoblin",
    "eventList": [
        {
            "id": "7ead69bb-d060-44fb-b98c-1e016ecb9ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d6cac78-7661-45f1-924c-4c5c9b3482f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "64b83edf-33c7-4240-ba53-a73a2e59b0ca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22904b76-7934-418d-beb6-8a8ace039ae9",
    "visible": true
}