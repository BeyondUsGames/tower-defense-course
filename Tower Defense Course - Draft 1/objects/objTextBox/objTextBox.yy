{
    "id": "62567f4b-025c-405d-a79e-a3cb29b91e51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTextBox",
    "eventList": [
        {
            "id": "5ff7606b-3b18-4df1-9445-f4e750cd99d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62567f4b-025c-405d-a79e-a3cb29b91e51"
        },
        {
            "id": "0e7173bb-5681-4835-9919-1b5e0b025a13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "62567f4b-025c-405d-a79e-a3cb29b91e51"
        },
        {
            "id": "1a5a74f0-7f68-4e5f-ab0d-bf50bef229b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62567f4b-025c-405d-a79e-a3cb29b91e51"
        },
        {
            "id": "a6730e19-81ad-4b84-bb67-f2a676c53b64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "62567f4b-025c-405d-a79e-a3cb29b91e51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c40d60f9-2192-407f-9558-505419735781",
    "visible": true
}