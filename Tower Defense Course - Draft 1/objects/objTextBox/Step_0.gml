/// @description Add to currentText
count = clamp(++count, 0, string_length(message));
currentText = string_copy(message, 0, count);

//Destroy self
if(keyboard_check(vk_anykey) || mouse_check_button(mb_any))
	instance_destroy();