/// @description Draw Text
draw_self();
draw_set_font(fntBigText);
draw_set_halign(fa_left);
draw_set_colour(c_white);
draw_text_ext(bbox_left + 12, bbox_top + 12, currentText, seperation, width);