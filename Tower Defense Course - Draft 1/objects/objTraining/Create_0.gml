/// @description Variables for level
event_inherited();

var player = instance_create_layer(x, y, layer, objPlayer);
with(player) {
	player.maxHealth = 5;
	player.currentHealth = player.maxHealth;
	player.gold = 300;
}

enemyList = ds_grid_create(2, 2);
ds_grid_add(enemyList, 0, 0, objGoblin);
ds_grid_add(enemyList, 0, 1, 5);

currentEnemy = 0;
maxMonsters = ds_grid_get(enemyList, 0, 1);
spawnRate = 120;
decreaser = 4;
myPath = pTraining;
alarm[0] = spawnRate + 300;

//For saving and loading
myLevelNumber = 0;

//Reset all the towers
global.AvailableTurrets = 0;
//Define just the ones I want for level 1
global.AvailableTurrets[0, TowerSprite] = sprTurretLevel1;
global.AvailableTurrets[0, TowerObject] = objTurretTower;
global.AvailableTurrets[0, TowerCost] = TurretLevel1Cost;

ShowMessage("Welcome to Tower Defense! Enemies are going to come from the left side of the path, and travel to the end. Build a tower to stop them! Left click to build a tower near the path.");