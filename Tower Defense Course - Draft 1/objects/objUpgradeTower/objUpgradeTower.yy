{
    "id": "7007ec67-f193-4a10-aaa2-64335ad9d3a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objUpgradeTower",
    "eventList": [
        {
            "id": "fbc41c8a-f6a1-4c12-a50c-3643a0ddb2b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7007ec67-f193-4a10-aaa2-64335ad9d3a6"
        },
        {
            "id": "1b17c258-4ac7-497d-84e6-4a8677248c1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7007ec67-f193-4a10-aaa2-64335ad9d3a6"
        },
        {
            "id": "b81aae53-9dcf-4ffb-83b8-f9f2686ae359",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7007ec67-f193-4a10-aaa2-64335ad9d3a6"
        },
        {
            "id": "3c2dd4f4-c2a6-4fe2-bb60-dee972eb8f05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7007ec67-f193-4a10-aaa2-64335ad9d3a6"
        },
        {
            "id": "fea42f14-01e4-4c48-9a97-782531cca423",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 6,
            "m_owner": "7007ec67-f193-4a10-aaa2-64335ad9d3a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cb0ed5b9-e060-4d38-b782-f7c658ca7eb5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb60d124-53a6-4c7c-a83c-798b9a94304e",
    "visible": true
}