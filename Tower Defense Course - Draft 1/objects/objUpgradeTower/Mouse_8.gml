/// @description Upgrade That Tower
if(myTower.levelStats[myTower.currentLevel + 1, Damage] != MaxedOut && objPlayer.gold >= myTower.levelStats[myTower.currentLevel, UpgradeCost]) {
	myTower.damage = myTower.levelStats[myTower.currentLevel + 1, Damage];
	myTower.radius = myTower.levelStats[myTower.currentLevel + 1, Radius];
	myTower.fireRate = myTower.levelStats[myTower.currentLevel + 1, FireRate];
	objPlayer.gold -= myTower.levelStats[myTower.currentLevel, UpgradeCost];
	++myTower.currentLevel;
}
else if(myTower.levelStats[myTower.currentLevel + 1, Damage] == MaxedOut)
	ShowMessage("That tower is maxed out.");
else
	ShowMessage("Not enough gold.");