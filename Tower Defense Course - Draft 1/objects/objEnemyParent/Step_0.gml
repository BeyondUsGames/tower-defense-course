/// @description Check Path & Health
if(path_position == 1) {
	--objPlayer.currentHealth;
	ShakeScreen(.5, .5);
	instance_destroy();
}

if(currentHealth <= 0) {
	instance_destroy();
	objPlayer.gold += myValue;
	objLevelParent.enemiesKilled++;
}