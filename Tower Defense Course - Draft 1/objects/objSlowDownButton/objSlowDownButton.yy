{
    "id": "959b3305-9d5a-45d0-98fd-c8c74176acf1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSlowDownButton",
    "eventList": [
        {
            "id": "65d5a119-e157-42fa-b936-1c1f05ad14a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "959b3305-9d5a-45d0-98fd-c8c74176acf1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0f35683-d806-4e1d-a433-415e05cd58ff",
    "visible": true
}