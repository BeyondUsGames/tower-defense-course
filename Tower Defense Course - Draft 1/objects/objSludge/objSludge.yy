{
    "id": "0d5a2fe4-1422-451d-8ff8-78a9608bbe7e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSludge",
    "eventList": [
        {
            "id": "00f56fcd-61cd-4074-aab0-b9bb9a531179",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d5a2fe4-1422-451d-8ff8-78a9608bbe7e"
        },
        {
            "id": "cb712f4a-625e-42cb-ad61-9df4f50cdb13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d5a2fe4-1422-451d-8ff8-78a9608bbe7e"
        },
        {
            "id": "1bc3a3e8-dce7-4ac4-9d63-9675b75d4a9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d5a2fe4-1422-451d-8ff8-78a9608bbe7e"
        },
        {
            "id": "1beb1af4-2c7c-4532-8d33-7fb75feadd89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64b83edf-33c7-4240-ba53-a73a2e59b0ca",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0d5a2fe4-1422-451d-8ff8-78a9608bbe7e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0251e049-be1a-485b-b502-df76aabcb144",
    "visible": true
}