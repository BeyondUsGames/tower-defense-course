{
    "id": "41c0246d-1f8e-4fd8-a418-688a12304e87",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTowerBase",
    "eventList": [
        {
            "id": "8bc5fe82-47dc-4b30-adc5-f6e302b91381",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "41c0246d-1f8e-4fd8-a418-688a12304e87"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a625369-0676-4c62-bf59-0d43e60bba3d",
    "visible": true
}