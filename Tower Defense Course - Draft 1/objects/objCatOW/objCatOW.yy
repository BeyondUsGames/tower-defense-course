{
    "id": "960e4398-a9ae-4049-b842-b9ec42da00f0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCatOW",
    "eventList": [
        {
            "id": "b3b9f59d-5987-43b8-a597-a99f2857a20c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "960e4398-a9ae-4049-b842-b9ec42da00f0"
        },
        {
            "id": "4b8d879d-8983-44fa-a38b-ef6bef5a1681",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "960e4398-a9ae-4049-b842-b9ec42da00f0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c9321dd-d552-4ed9-b9b9-e41c6b448bfd",
    "visible": true
}