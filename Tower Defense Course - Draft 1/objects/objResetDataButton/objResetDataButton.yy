{
    "id": "9ea9e2c1-b543-45d6-b182-e222730255a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objResetDataButton",
    "eventList": [
        {
            "id": "4979a6da-479c-4220-b3e1-9a4e5cc9a3bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9ea9e2c1-b543-45d6-b182-e222730255a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "360a69aa-3dd3-41b3-bfef-6b3480dcfd77",
    "visible": true
}