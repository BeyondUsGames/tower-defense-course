{
    "id": "03340b58-b34e-489f-8be0-6f8cd3bfdf4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDeleteTower",
    "eventList": [
        {
            "id": "6fc8826e-fe31-4301-bc82-de84519b3afd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 6,
            "m_owner": "03340b58-b34e-489f-8be0-6f8cd3bfdf4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cb0ed5b9-e060-4d38-b782-f7c658ca7eb5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fad774cf-eee0-4082-af0f-26fb225a0cac",
    "visible": true
}