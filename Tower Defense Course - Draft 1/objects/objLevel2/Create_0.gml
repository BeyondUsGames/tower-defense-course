/// @description Variables for level 1
event_inherited();

var player = instance_create_layer(x, y, layer, objPlayer);
with(player) {
	player.maxHealth = 5;
	player.currentHealth = player.maxHealth;
	player.gold = 60;
}
//Monster List for level
enemyList = ds_grid_create(2, 2);
ds_grid_add(enemyList, 0, 0, objGoblin);
ds_grid_add(enemyList, 0, 1, 5);
ds_grid_add(enemyList, 1, 0, objCat);
ds_grid_add(enemyList, 1, 1, 15);

maxMonsters = ds_grid_get(enemyList, 0, 1) + ds_grid_get(enemyList, 1, 1);
currentEnemy = 0;
spawnRate = 120;
decreaser = 4;
myPath = pLevel2;
alarm[0] = spawnRate + 300;

//Saving and Loading
myLevelNumber = 2;

//Reset all the towers
global.AvailableTurrets = 0;
//Define just the ones I want for level 1
global.AvailableTurrets[0, TowerSprite] = sprTurretLevel1;
global.AvailableTurrets[0, TowerObject] = objTurretTower;
global.AvailableTurrets[0, TowerCost] = TurretLevel1Cost;

global.AvailableTurrets[1, TowerSprite] = sprSludgeTower;
global.AvailableTurrets[1, TowerObject] = objSludgeTower;
global.AvailableTurrets[1, TowerCost] = SludgeLevel1Cost;

ShowMessage("There's a new tower! It will slow enemies down, but not hurt them. So build wisely.");