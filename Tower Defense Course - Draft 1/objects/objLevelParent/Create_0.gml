/// @description All Level Data
//Create Objects
instance_create_layer(room_width / 2, room_height / 2, layer, objTurretMaker);
instance_create_layer(room_width / 2 + 128, room_height - 64, layer, objSpeedUpButton);
instance_create_layer(room_width / 2 + 64, room_height - 64, layer, objSlowDownButton);
instance_create_layer(room_width / 2 + 96, room_height - 128, layer, objPauseButton);

//Saving & Loading
starRating = 0;

//Enemies
enemiesKilled = 0;
spawnCount = 0;

