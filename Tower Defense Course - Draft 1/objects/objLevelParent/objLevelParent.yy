{
    "id": "205e3276-8ac2-40ba-adb9-6dda647d1865",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLevelParent",
    "eventList": [
        {
            "id": "7c2ec74c-997d-42e7-a2db-35ab27bfffa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "205e3276-8ac2-40ba-adb9-6dda647d1865"
        },
        {
            "id": "55132118-c7bf-4d61-b6ce-ea2f83302892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "205e3276-8ac2-40ba-adb9-6dda647d1865"
        },
        {
            "id": "f93c8d56-afd4-45d7-92b8-3423d8cdc6d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "205e3276-8ac2-40ba-adb9-6dda647d1865"
        },
        {
            "id": "71240b69-c09a-4fa9-abb4-9d03e2e1b6d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "205e3276-8ac2-40ba-adb9-6dda647d1865"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}