/// @description Spawn a Monster
if(spawnCount < maxMonsters) {
	//Check amount of enemy left
	if(ds_grid_get(enemyList, currentEnemy, 1) <= 0)
		++currentEnemy;
	enemyToSpawn = ds_grid_get(enemyList, currentEnemy, 0);
	ds_grid_add(enemyList, currentEnemy, 1, -1);
	enemy = instance_create_layer(x, y, layer, enemyToSpawn);
	with(enemy) {
		path_start(other.myPath, other.enemyToSpawn.mySpeed, path_action_stop, true);
	}

	++spawnCount;
	//Increase speed of spawner
	spawnRate -= decreaser;

	alarm[0] = spawnRate;
}
//Party time!
else if(spawnCount == maxMonsters && !instance_exists(objEnemyParent)) {
	//Reset game speed
	game_set_speed(60, gamespeed_fps);
	//Start fireworks
	part_emitter_stream(global.PartFireworkSystem, global.PartFireworkEmitter, global.PartRocket, -20);
	sprite_index = sprContinueButton;
	x = room_width / 2;
	y = room_height / 2 + 128;
	image_xscale = 2;
	image_yscale = 2;
	window_set_cursor(cr_default);
	instance_destroy(objTurretMaker);
	//Determine Star Rating
	if(objPlayer.currentHealth == objPlayer.maxHealth)
		starRating = 3;
	else if(objPlayer.currentHealth >= objPlayer.maxHealth / 2)
		starRating = 2;
	else
		starRating = 1;
		
	//Don't record a lower score
	var fileRating = LoadStarRating();
	if(starRating < fileRating) {
		starRating = fileRating;
	}
	SaveLevel();
}
else
	alarm[0] = 1;