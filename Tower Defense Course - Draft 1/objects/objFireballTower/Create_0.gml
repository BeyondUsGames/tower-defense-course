/// @description Data
image_xscale = .25;
image_yscale = .25;

//Level Stats
levelStats[TowerLevel1, FireRate] = 80;
levelStats[TowerLevel1, Radius] = 212;
levelStats[TowerLevel1, Damage] = 22.5;
levelStats[TowerLevel1, UpgradeCost] = 50;

levelStats[TowerLevel2, FireRate] = 70;
levelStats[TowerLevel2, Radius] = 236;
levelStats[TowerLevel2, Damage] = 25;
levelStats[TowerLevel2, UpgradeCost] = 75;

levelStats[TowerLevel3, FireRate] = 60;
levelStats[TowerLevel3, Radius] = 250;
levelStats[TowerLevel3, Damage] = 30;
levelStats[TowerLevel3, UpgradeCost] = MaxedOut;

levelStats[TowerLevel4, FireRate] = MaxedOut;
levelStats[TowerLevel4, Radius] = MaxedOut;
levelStats[TowerLevel4, Damage] = MaxedOut;


currentLevel = 0;

fireRate = levelStats[currentLevel, FireRate];
radius = levelStats[currentLevel, Radius];
damage = levelStats[currentLevel, Damage];

enemyType = objEnemyParent;
myProjectile = objFireball;
mySound = sndFireball;

bulletSpawnX = x;
bulletSpawnY = y;