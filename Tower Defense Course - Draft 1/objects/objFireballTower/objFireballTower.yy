{
    "id": "249b70f3-bc29-4d61-91d9-d3822ac99a55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objFireballTower",
    "eventList": [
        {
            "id": "21c8e996-9758-47e8-a04b-9982b4a0e635",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "249b70f3-bc29-4d61-91d9-d3822ac99a55"
        },
        {
            "id": "38ea8a67-7cd9-4006-bbbb-187491747294",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "249b70f3-bc29-4d61-91d9-d3822ac99a55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ce492ad-b49d-4087-9d1b-8ec4a90ceae8",
    "visible": true
}