/// @description Check for enemies and touch
audio_sound_gain(mySound, .1 / instance_number(object_index), 0);
nearestEnemy = collision_circle(x, y, radius, enemyType, false, true);

//Rotate to face enemy
if(nearestEnemy != -4) {
	var player = point_direction(x, y, nearestEnemy.x, nearestEnemy.y);
	var angle = angle_difference(image_angle, player);
	image_angle -= min(abs(angle), 10) * sign(angle) / 2;
	
	//Fire
	if(alarm[0] == -1) {
		event_perform(ev_alarm, 0);
		//alarm[0] = fireRate;
	}
}
else { //Rotate around
	image_angle += .75
}