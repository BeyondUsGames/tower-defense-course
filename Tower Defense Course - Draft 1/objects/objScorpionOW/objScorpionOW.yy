{
    "id": "d6031de3-6515-4aaf-82cc-0e2faa1b3df6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objScorpionOW",
    "eventList": [
        {
            "id": "f64ad662-c067-40f8-89fc-bd93667680ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6031de3-6515-4aaf-82cc-0e2faa1b3df6"
        },
        {
            "id": "41790a78-8625-4a48-bcc5-0b2af7ac35eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d6031de3-6515-4aaf-82cc-0e2faa1b3df6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a240d09-49d4-44a2-8535-289a8142a53b",
    "visible": true
}