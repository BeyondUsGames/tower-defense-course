/// @description Reimburse Owner
var refundAmount = 0;
//Find the tower I am, and the original cost
for(var i = 0; i < array_height_2d(global.AvailableTurrets); ++i) {
	if(object_get_name(id.object_index) == object_get_name(global.AvailableTurrets[i, TowerObject])) {
		refundAmount = global.AvailableTurrets[i, TowerCost];
		break;
	}
}
//Add any upgrades
for(var i = 0; i < currentLevel; ++i) {
	refundAmount += levelStats[i, UpgradeCost];
}
//Refund half of the total amount
objPlayer.gold += round(refundAmount / 2);