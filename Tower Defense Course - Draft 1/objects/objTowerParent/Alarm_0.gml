/// @description Fire at enemy
if(nearestEnemy != -4) {
	audio_play_sound(mySound, 1, false);
	bullet = instance_create_layer(bulletSpawnX, bulletSpawnY, layer, myProjectile);
	with(bullet) {
		speed = 6;
		direction = point_direction(x, y, ShootAt("x"), ShootAt("y"));
		image_angle = direction;
		myDamage = other.damage;
	}
	alarm[0] = fireRate;
}