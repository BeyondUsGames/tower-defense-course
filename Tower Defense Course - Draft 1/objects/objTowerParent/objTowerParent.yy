{
    "id": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTowerParent",
    "eventList": [
        {
            "id": "5d7cf439-c1c9-4405-a73d-c34fc01623a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730"
        },
        {
            "id": "55d871e7-170b-480f-92be-82582eede1ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730"
        },
        {
            "id": "ce875c72-6e12-4385-913f-61b2891c4ca0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730"
        },
        {
            "id": "a09be469-4a24-4fac-a99a-8baf8e8b2fe4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730"
        },
        {
            "id": "f7f8b891-209b-4988-969e-8da5fa1bdd6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "08e0f6ba-bbd2-469b-88dc-9ba4ff0d3730"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}