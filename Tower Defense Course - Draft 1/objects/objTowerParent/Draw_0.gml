/// @description Draw base underneath and tower
draw_set_alpha(1);
draw_sprite_ext(sprTowerBase, 0, x, y, .25, .25, 0, c_white, 1);
draw_self();
//Draw radius
draw_set_alpha(.25);
draw_set_colour(c_red);
draw_circle(x, y, radius, true);