/// @description Create Tower Options
with(instance_create_depth(x - 64, y, depth - 10, objDeleteTower))
	myTower = other.id;
with(instance_create_depth(x + 64, y, depth - 10, objUpgradeTower))
	myTower = other.id;