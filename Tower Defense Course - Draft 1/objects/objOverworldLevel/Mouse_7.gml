/// @description Move to that level
if(prevLevelObject.starRating != 0 || myLevelNumber == 0)
	ChangeRoom(myRoom);
else
	ShowMessage("You must complete the levels in order.");