{
    "id": "a7edd8ee-b5ea-4056-ad16-4310de498676",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel3",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "003a819f-d0c0-4da7-b07d-389bbc6bf03d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 224,
            "speed": 100
        },
        {
            "id": "475c99e5-2261-4bcd-b8b7-97103008d2c2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 224,
            "speed": 100
        },
        {
            "id": "220af536-bb43-48d1-acfa-400eaa756130",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 32,
            "speed": 100
        },
        {
            "id": "0d1d11b8-09e2-468f-ab96-cf2f0e824d1c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 32,
            "speed": 100
        },
        {
            "id": "aa0e0d82-37cd-42df-81fc-9866c2b05033",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 288,
            "speed": 100
        },
        {
            "id": "70a85657-de05-45a7-9618-5f45c9fbc82a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 288,
            "speed": 100
        },
        {
            "id": "15bebafc-ff17-4fe3-bbcb-b093677d454a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 416,
            "speed": 100
        },
        {
            "id": "5a208d03-99e4-46e1-8230-28870cbd9545",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 416,
            "speed": 100
        },
        {
            "id": "8a3234ec-6a12-41dd-b410-563c12eda608",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 608,
            "speed": 100
        },
        {
            "id": "1599caf0-0f80-439b-ab3a-6a6241147740",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 608,
            "speed": 100
        },
        {
            "id": "00dc6ce8-6822-40f5-b649-894fab293060",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 480,
            "speed": 100
        },
        {
            "id": "17c5bd0b-f65c-4dc8-a203-ffd0886659a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 480,
            "speed": 100
        },
        {
            "id": "1dd8e18e-10b9-420a-bb3b-99ba86714042",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 992,
            "y": 288,
            "speed": 100
        },
        {
            "id": "d91aa6c2-f08a-4c05-8487-a196b800b7ed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 288,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}