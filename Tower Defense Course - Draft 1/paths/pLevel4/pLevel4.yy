{
    "id": "6e05d76a-12a9-4419-8f6a-8da1d867ca34",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel4",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "a7f62b2a-0246-40df-9e99-df0b14c048e8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 0,
            "speed": 100
        },
        {
            "id": "1d0b6ec3-267b-41f7-be96-ee1eb90fbcb3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 672,
            "speed": 100
        },
        {
            "id": "3ed74bab-0916-45ad-88f3-42945fe62be6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 672,
            "speed": 100
        },
        {
            "id": "796b923d-9f82-4e5c-8ab5-3a705cc6c2e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 160,
            "speed": 100
        },
        {
            "id": "3ef5d63b-f327-4158-b938-9031c1724d39",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 160,
            "speed": 100
        },
        {
            "id": "c54985e8-6487-46a3-9a69-5a6ebd9e61cd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 544,
            "speed": 100
        },
        {
            "id": "ca738e5b-dfbe-4b00-9e8b-3f0be4aae269",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 544,
            "speed": 100
        },
        {
            "id": "5e58a188-2dc0-43d1-98bd-4c88e0d2b09a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 672,
            "speed": 100
        },
        {
            "id": "a8f4a4a0-f994-42eb-8b8e-af1ea5c67262",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 672,
            "speed": 100
        },
        {
            "id": "b982a302-0ecd-4bac-a0a0-0a171140b341",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1120,
            "y": 352,
            "speed": 100
        },
        {
            "id": "75c74c78-539d-4ec0-bdd4-3975307c2928",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 352,
            "speed": 100
        },
        {
            "id": "af8dac86-782f-48e3-977c-13e21aa30044",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 160,
            "speed": 100
        },
        {
            "id": "a5f8aeb7-38dc-4a89-a7a3-640732304fe0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 160,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}