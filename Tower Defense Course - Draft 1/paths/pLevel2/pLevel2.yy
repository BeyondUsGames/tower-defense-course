{
    "id": "067f8188-201e-4f84-bb5a-50b6d53f0b2b",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel2",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "70c12ffe-3687-4235-9a34-04bee59faced",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48,
            "speed": 100
        },
        {
            "id": "738d1f2d-a557-4526-a8c6-285adcd9ce3e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 48,
            "speed": 100
        },
        {
            "id": "f519d5da-a524-43e1-9810-b24cba470a83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 336,
            "speed": 100
        },
        {
            "id": "17f10255-e86f-41de-889c-cc494e1b5fef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 336,
            "speed": 100
        },
        {
            "id": "cb2e3136-3877-4216-ae6a-78f214942ba5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 176,
            "speed": 100
        },
        {
            "id": "df66c755-e366-47db-832c-e2cbb78acb0a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1104,
            "y": 176,
            "speed": 100
        },
        {
            "id": "715e43a4-b972-4bfe-acc9-ec8c9471b735",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1104,
            "y": 448,
            "speed": 100
        },
        {
            "id": "b00200d8-bd3f-4340-9ba6-4a8460f6eba4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 448,
            "speed": 100
        },
        {
            "id": "8067b208-cdaa-463b-8d8a-38f654871d0d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 576,
            "speed": 100
        },
        {
            "id": "16f3e133-6080-46c1-8a66-3489423b141a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 576,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}