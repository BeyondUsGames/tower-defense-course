{
    "id": "b57ea875-8f3f-4bf8-8a0b-2d52b8921a7c",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pLevel5",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "33770394-27ff-4687-92f7-5bf5fdf01dd9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 672,
            "speed": 100
        },
        {
            "id": "3ab19dac-ef99-4578-b408-2856e94e9aae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 672,
            "speed": 100
        },
        {
            "id": "179ba17a-6cea-4648-9cdc-2a55404cc2ac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 224,
            "speed": 100
        },
        {
            "id": "d6a03532-a688-47ab-b6d0-fb8d786045d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 224,
            "speed": 100
        },
        {
            "id": "67588d6f-a43a-4a8e-8f76-cfc0b3c1bca4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 416,
            "speed": 100
        },
        {
            "id": "aaa6862c-5bc8-41bd-87ea-72c59ef0b727",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 416,
            "speed": 100
        },
        {
            "id": "40de2b9c-5825-4ed4-a3f7-f7833e9bdd03",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 32,
            "speed": 100
        },
        {
            "id": "fd5a2523-dce0-4879-9c05-4456eb0c853f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 32,
            "speed": 100
        },
        {
            "id": "aba41da2-cfc1-4776-9f7a-e986e1174fb6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1248,
            "y": 288,
            "speed": 100
        },
        {
            "id": "4fb2e5ef-ef95-4587-8f00-538e5763950d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1056,
            "y": 288,
            "speed": 100
        },
        {
            "id": "ded24032-89de-41e9-898f-2e2c4923ed85",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1056,
            "y": 480,
            "speed": 100
        },
        {
            "id": "768ec533-e15f-4c94-a57c-2dbe22a95ad6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1632,
            "y": 480,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}