
{
    "name": "rmOverworld",
    "id": "4befe086-b413-4fb5-bf30-dc036fcb2d22",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "b1f63c45-e331-4a0f-a8e4-bffccfcfd29c",
        "c5f82e67-5f25-486e-ac03-5b4bd416d302",
        "250cd380-a4fd-4cf3-b0da-7f1df53a3d67",
        "06d170e9-f63d-43fb-a28b-40693701f022",
        "3bc7af65-2bae-4d79-b5dc-226092ea1fe3",
        "cd0eeb8f-145f-4736-b579-dd449792c541",
        "88cc645f-5acc-4fd0-bcea-0f2b6df714d9",
        "23f33f3b-b694-4dad-b0bf-6761ff62e05a"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "87c89ca2-867f-4a5d-9af2-62c92c27f063",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_6E1455DA","id": "b1f63c45-e331-4a0f-a8e4-bffccfcfd29c","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_6E1455DA.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6E1455DA","objId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","properties": [{"id": "d6a48616-f17c-4aeb-8bcf-7d67b3461b1f","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "c0d29140-b748-40d1-88de-f9f3abffa23d","mvc": "1.0","value": "sprLevelTraining"},{"id": "e379cafd-75d2-4354-a324-ed85f7439176","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "813ced45-12e2-41a4-9510-77d19e9d301f","mvc": "1.0","value": "rmTraining"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 96},
{"name": "inst_104D3A60","id": "c5f82e67-5f25-486e-ac03-5b4bd416d302","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_104D3A60","objId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","properties": [{"id": "6e8f3f11-c170-4e25-aa0f-f268306792f3","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "813ced45-12e2-41a4-9510-77d19e9d301f","mvc": "1.0","value": "rmLevel1"},{"id": "96494ba0-ea31-4b64-ac7f-16e47af9363b","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "c0d29140-b748-40d1-88de-f9f3abffa23d","mvc": "1.0","value": "sprLevel1"},{"id": "3fb309f9-74a3-4a69-a2ff-58e710508c54","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "7de47640-1a37-4f34-ba43-f17b5c632b3b","mvc": "1.0","value": "1"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 96},
{"name": "inst_239E961E","id": "250cd380-a4fd-4cf3-b0da-7f1df53a3d67","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_239E961E","objId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","properties": [{"id": "e2e11816-a3be-4bb1-9e42-02b1cfb9713f","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "813ced45-12e2-41a4-9510-77d19e9d301f","mvc": "1.0","value": "rmLevel2"},{"id": "de703faf-7e07-4c6a-b2d2-85aa3faad939","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "7de47640-1a37-4f34-ba43-f17b5c632b3b","mvc": "1.0","value": "2"},{"id": "3fa2eb15-c5ed-4840-aa91-c8d0fac28a92","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "c0d29140-b748-40d1-88de-f9f3abffa23d","mvc": "1.0","value": "sprLevel2"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 320,"y": 96},
{"name": "inst_43D5EF58","id": "06d170e9-f63d-43fb-a28b-40693701f022","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_43D5EF58","objId": "9ea9e2c1-b543-45d6-b182-e222730255a8","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 672,"y": 448},
{"name": "inst_27085CD1","id": "3bc7af65-2bae-4d79-b5dc-226092ea1fe3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_27085CD1","objId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","properties": [{"id": "b8f1172c-3457-4f0c-b403-4ffa3c16fc76","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "813ced45-12e2-41a4-9510-77d19e9d301f","mvc": "1.0","value": "rmLevel3"},{"id": "3a26305d-8889-4569-9c52-600768886a33","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "c0d29140-b748-40d1-88de-f9f3abffa23d","mvc": "1.0","value": "sprLevel3"},{"id": "782c5da7-91ba-4364-83c4-5379c967e773","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "7de47640-1a37-4f34-ba43-f17b5c632b3b","mvc": "1.0","value": "3"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 448,"y": 96},
{"name": "inst_11DB483","id": "cd0eeb8f-145f-4736-b579-dd449792c541","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_11DB483","objId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","properties": [{"id": "3598518b-05a9-4b6d-91e8-fbfff382a636","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "813ced45-12e2-41a4-9510-77d19e9d301f","mvc": "1.0","value": "rmLevel4"},{"id": "387864c6-5d7e-4ab9-adb6-ab95fde37cd7","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "c0d29140-b748-40d1-88de-f9f3abffa23d","mvc": "1.0","value": "sprLevel4"},{"id": "a2558bb5-7091-41ac-af85-d14c529a6a72","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "7de47640-1a37-4f34-ba43-f17b5c632b3b","mvc": "1.0","value": "4"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 576,"y": 96},
{"name": "inst_6C0DE30F","id": "88cc645f-5acc-4fd0-bcea-0f2b6df714d9","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6C0DE30F","objId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","properties": [{"id": "b5722a04-96e5-4537-ba30-6ec584f46947","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "813ced45-12e2-41a4-9510-77d19e9d301f","mvc": "1.0","value": "rmLevel5"},{"id": "a1b98426-743f-43da-9b39-896d768192c2","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "c0d29140-b748-40d1-88de-f9f3abffa23d","mvc": "1.0","value": "sprLevel5"},{"id": "9c65d40b-ea0c-4182-b671-13d52bb07b74","modelName": "GMOverriddenProperty","objectId": "ff1a8e93-ccd8-42d6-b199-13c1f7d16897","propertyId": "7de47640-1a37-4f34-ba43-f17b5c632b3b","mvc": "1.0","value": "5"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 704,"y": 96},
{"name": "inst_222603F9","id": "23f33f3b-b694-4dad-b0bf-6761ff62e05a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_222603F9","objId": "44d1e3a5-84a2-466d-a901-55fb36aa0cff","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "fa1bb586-9d94-48da-98fb-70795cbec255",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": -1,
            "htiled": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "460be54f-6b90-42f1-987f-23aec7b898eb",
            "stretch": true,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "0dcd5e2c-c819-4b00-bfcc-20051f8649ec",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "11e74d99-ad2e-493f-bda4-c7f5a593ab1a",
        "Height": 480,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 800
    },
    "mvc": "1.0",
    "views": [
{"id": "bd039ffb-0987-4485-ba8b-f8dc4b91c171","hborder": 32,"hport": 960,"hspeed": -1,"hview": 480,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1600,"wview": 800,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6060bbca-76e9-4ff9-b7f4-4218bcdcdd1a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "bd7616f0-fc94-421e-8a7a-d29ca06a5c1b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b836c8ac-b958-4a6f-923d-c899bdebc577","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5ccb062e-f616-4278-816f-282bd7aa168c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4735ee05-1f24-431a-a1be-3bd565802aa8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f2650b32-9bf7-4e42-b160-40213b11220c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "753ebe3c-4b0b-457c-92b1-0c24a45134b3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "230d3ae0-1667-469f-92ea-e56c44bb0a0e",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}